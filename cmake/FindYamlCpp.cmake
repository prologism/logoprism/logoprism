set(YAML_SRCROOT ${LOGOPRISM_LIBDIR}/yaml-cpp)

if(NOT EXISTS ${YAML_SRCROOT})
  find_package(Git 1.8.3 REQUIRED)
  message(STATUS "Initiliazing git submodule ${YAML_SRCROOT}...")
  execute_process(
    COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive -- "${YAML_SRCROOT}"
  )
endif()

option(YAML_BUILD_EXAMPLES "Build the Yaml-Cpp example programs" OFF)
option(YAML_BUILD_TESTS "Build the Yaml-Cpp test programs" OFF)
add_subdirectory(${YAML_SRCROOT} EXCLUDE_FROM_ALL)

set(YAML_INCLUDE_DIRS ${YAML_SRCROOT}/include)
set(YAML_LIBRARIES yaml-cpp)
