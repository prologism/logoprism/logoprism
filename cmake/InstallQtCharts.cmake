set(QT_CHARTS_ROOT ${LOGOPRISM_LIBDIR}/qtcharts)

if(NOT EXISTS ${QT_CHARTS_ROOT})
  find_package(Git 1.8.3 REQUIRED)
  message(STATUS "Initiliazing git submodule ${QT_CHARTS_ROOT}...")
  execute_process(
    COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive -- "${QT_CHARTS_ROOT}"
  )
endif()

execute_process(
  COMMAND qmake
  WORKING_DIRECTORY ${QT_CHARTS_ROOT}
)

execute_process(
  COMMAND make install 
  WORKING_DIRECTORY ${QT_CHARTS_ROOT}
)

