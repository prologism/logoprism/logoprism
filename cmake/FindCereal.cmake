set(CEREAL_SRCROOT ${LOGOPRISM_LIBDIR}/cereal)

if(NOT EXISTS ${CEREAL_SRCROOT})
  find_package(Git 1.8.3 REQUIRED)
  message(STATUS "Initiliazing git submodule ${CEREAL_SRCROOT}...")
  execute_process(
    COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive -- "${CEREAL_SRCROOT}"
  )
endif()

set(CEREAL_INCLUDE_DIRS ${CEREAL_SRCROOT}/include)
