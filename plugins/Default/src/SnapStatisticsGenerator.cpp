#include "SnapStatisticsGenerator.h"

#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Statistics/SnapStatistics.h"
#include "logoprism/API/Statistics/StatisticsManager.h"

#include <QWidget>
#include <QtCharts>
#include <QBarSet>

namespace LOGOPRISM
{
    static QChartView* createBarChartForResponseTime(std::map<std::string, API::SnapStatistics>& stat_map, std::vector<std::string>& stat_order_vec)
    {

        QBarSet* minimum = new QBarSet("Minimum");
        QBarSet* average = new QBarSet("Average");
        QBarSet* maximum = new QBarSet("Maximum");
        QStringList modules;

        for(auto& element : stat_order_vec)
        {
                auto& stat = stat_map[element];
            modules << QString(element.c_str());

            auto min_time = stat.getMinResponseTime();
            *minimum << (min_time == API::TimeDuration::max() ? 0.0 : stat.getMinResponseTime().count() / (1000.0));

            *average << stat.getAvgResponseTime().count() / (1000.0);

            auto max_time = stat.getMaxResponseTime();
            *maximum << (max_time == API::TimeDuration::min() ? 0.0 : stat.getMaxResponseTime().count() / (1000.0));
        }

        QBarSeries* series = new QBarSeries();
        series->append(minimum);
        series->append(average);
        series->append(maximum);

        QChart* chart = new QChart();
        chart->addSeries(series);
        chart->setTitle("Response Times");
        chart->setAnimationOptions(QChart::SeriesAnimations);

        QBarCategoryAxis* x_axis = new QBarCategoryAxis();
        x_axis->append(modules);
        x_axis->setTitleText("Application");
        chart->addAxis(x_axis, Qt::AlignBottom);
        series->attachAxis(x_axis);

        QValueAxis* y_axis = new QValueAxis();
        y_axis->setTitleText("Response Times (us)");
        chart->addAxis(y_axis, Qt::AlignLeft);
        series->attachAxis(y_axis);
		series->attachAxis(y_axis);
        chart->legend()->setVisible(true);
        chart->legend()->setAlignment(Qt::AlignLeft);

        QChartView* chartView = new QChartView(chart);
        return chartView;
    }

    void generateSnapStatisticsDialog(API::StatisticsManager& stat_manager, QWidget* statistics_box, const API::Timestamp& simulation_time, std::vector<std::shared_ptr<API::EventBase>>)
    {
        auto& snap_stat_map = stat_manager.getSnapStatisticsMap();
        auto& snap_stat_order = stat_manager.getGlobalStatisticsOrderVector();
        QChartView* resp_time_chart = createBarChartForResponseTime(snap_stat_map, snap_stat_order);

        
        QVBoxLayout* statistics_layout_vertical = new QVBoxLayout();
        statistics_layout_vertical->addWidget(resp_time_chart);
        statistics_box->setLayout(statistics_layout_vertical);
    }
}
