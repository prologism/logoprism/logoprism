#ifndef SNAP_STATISTICS_GENERATORS_H
#define SNAP_STATISTICS_GENERATORS_H

#include <memory>

#include "logoprism/API/Processors/Models/EventBase.h"

class QWidget;
namespace LOGOPRISM
{
    namespace API { class StatisticsManager; }
    void generateSnapStatisticsDialog(API::StatisticsManager& stat_manager, QWidget* statistics_box, const API::Timestamp& simulation_time, std::vector<std::shared_ptr<API::EventBase>>);
}

#endif
