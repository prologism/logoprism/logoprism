#ifndef GENERIC_PARSER_H
#define GENERIC_PARSER_H

#include <sstream>
#include <map>
#include <boost/regex.hpp>

#include "logoprism/API/Utils/Config.h"
#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Utils/Timestamp.h"

/**
 * @file GenericParser.h
 * @brief Contains a parser which parses a line in the file provided by the configuration file.
 */


namespace LOGOPRISM
{
	/**
	 *  GenericParser to parse a line.
	 *  It parses a line whose format is known by the regular expression given in the configuration file.
	 */
    class GenericParser
	{
	public:

    	/*
    	 * Constructor.
    	 */
    	GenericParser();

    	/*
    	 * Checks if the parsed event is valid.
    	 * It is the implementation of the converter interface needed by the EventConverter class in the core.
    	 */
    	bool isDataValid(const LOGOPRISM::API::EventBase& event) { return event.isValid(); };

    	/*
    	 * Parses a line.
    	 * It is the implementation of the converter interface needed by the EventConverter class in the core.
    	 * It parses a line based on the provided layout defined by the regular expression.
    	 */
        std::deque<API::EventBase> convert(std::deque<std::string>& lines);
    };
}


#endif
