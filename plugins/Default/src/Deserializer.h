#ifndef DESERIALIZER_H
#define DESERIALIZER_H

/**
 * @file Deserializer.h
 * @brief Deserializer class declaration.
 */

#include <cereal/archives/binary.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Processors/Controllers/SerializeManager.h"

namespace LOGOPRISM
{
	/**
	 *  Deserializer which deserializes the objects.
	 */
	class Deserializer
	{
	public:
		std::ifstream m_File; /**< Stream containing the data */
		cereal::BinaryInputArchive m_DeSerializer;  /**< Deserializer object provided by cereal */
		const std::shared_ptr<API::SerializeManagerBase>& m_SerializeManager; /**< SerializeManager class which did the serialization process */
        long m_Size; /**< Size of the file */
        std::mutex m_Lock; /**< Lock to handle concurrence access to the source */

		/*
		 * Constructor to initialize the Deserializer
		 * @param manager The serialize manager class which did the serialization
		 */
        Deserializer(const std::shared_ptr<API::SerializeManagerBase>& manager);

		/*
		 * Function to read one or more lines in the file.
		 * Its signature is defined by the interface needed by the Logoprism core framework.
		 */
        std::deque<LOGOPRISM::API::EventBase> getData();

        /*
         * Checks if the file source is still okay.
         * Its signature is defined by the interface needed by the Logoprism core framework.
         */
        bool isDataAvailable() { std::lock_guard<std::mutex> lk(m_Lock); return m_File.tellg() < getFileSize() && m_File.good(); }

        /*
         * Modifies file offset.
         * Its signature is defined by the interface needed by the Logoprism core framework.
         */
        void modifyOffset(size_t offset) { std::lock_guard<std::mutex> lk(m_Lock); m_File.seekg(offset); }

        /*
         * Gets the size of the file.
         */
        long getFileSize();

        /*
         * Sets the source.
         * Its signature is defined by the interface needed by the Logoprism core framework.
         */
        void setSource(const std::string& source) { m_File.open(source, std::ios::binary); }
	};

}

#endif
