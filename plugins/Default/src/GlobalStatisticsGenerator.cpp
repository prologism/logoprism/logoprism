#include "GlobalStatisticsGenerator.h"

#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Statistics/GlobalStatistics.h"
#include "logoprism/API/Statistics/StatisticsManager.h"
#include "logoprism/API/Visualisers/Views/ChartView.h"

#include <QWidget>
#include <QtCharts>
#include <QBarSet>
#include <iostream>

namespace LOGOPRISM
{
    static QChartView* createBarChartForResponseTime(std::map<std::string, API::GlobalStatistics>& stat_map, std::vector<std::string>& stat_order_vec)
    {

        QBarSet* minimum = new QBarSet("Minimum");
        QBarSet* average = new QBarSet("Average");
        QBarSet* maximum = new QBarSet("Maximum");
        QStringList modules;
        for(auto& element : stat_order_vec)
        {
                auto& stat = stat_map[element];
            modules << QString(element.c_str());
            *minimum << stat.getMinResponseTime().count() / (1000.0 * 1000.0);
            *average << stat.getAvgResponseTime().count() / (1000.0 * 1000.0);
            *maximum << stat.getMaxResponseTime().count() / (1000.0 * 1000.0);
        }

        QBarSeries* series = new QBarSeries();
        series->append(minimum);
        series->append(average);
        series->append(maximum);

        QChart* chart = new QChart();
        chart->addSeries(series);
        chart->setTitle("Response Times");
        chart->setAnimationOptions(QChart::SeriesAnimations);

        QBarCategoryAxis* x_axis = new QBarCategoryAxis();
        x_axis->append(modules);
        x_axis->setTitleText("Application");
        chart->addAxis(x_axis, Qt::AlignBottom);
        series->attachAxis(x_axis);

        QValueAxis* y_axis = new QValueAxis();
        y_axis->setTitleText("Response Times (ms)");
        chart->addAxis(y_axis, Qt::AlignLeft);
        series->attachAxis(y_axis);
        chart->legend()->setVisible(true);
        chart->legend()->setAlignment(Qt::AlignLeft);

        QChartView* chartView = new QChartView(chart);
        return chartView;
   }

    static QChartView* createBarChartForPercentiles(std::map<std::string, API::GlobalStatistics>& stat_map, std::vector<std::string>& stat_order_vec)
    {
        QBarSet* percentile_50 = new QBarSet("Percentile 50");
        QBarSet* percentile_75 = new QBarSet("Percentile 75");
        QBarSet* percentile_90 = new QBarSet("Percentile 90");
        QBarSet* percentile_99 = new QBarSet("Percentile 99");
        QBarSet* percentile_99_9 = new QBarSet("Percentile 99.9");

        QStringList modules;
        for(auto& element : stat_order_vec)
        {
                auto& stat = stat_map[element];
            modules << QString(element.c_str());
            *percentile_50 << stat.getPercentile50().count() / (1000.0);
            *percentile_75 << stat.getPercentile75().count() / (1000.0);
            *percentile_90 << stat.getPercentile90().count() / (1000.0);
            *percentile_99 << stat.getPercentile99().count() / (1000.0);
            *percentile_99_9 << stat.getPercentile99_9().count() / (1000.0);
        }

        QBarSeries* series = new QBarSeries();
        series->append(percentile_50);
        series->append(percentile_75);
        series->append(percentile_90);
        series->append(percentile_99);
        series->append(percentile_99_9);
       
	    QChart* chart = new QChart();
        chart->addSeries(series);
        chart->setTitle("Percentiles");
        chart->setAnimationOptions(QChart::SeriesAnimations);

        QBarCategoryAxis* x_axis = new QBarCategoryAxis();
        x_axis->setTitleText("Application");
        x_axis->append(modules);
        chart->addAxis(x_axis, Qt::AlignBottom);
        series->attachAxis(x_axis);

        QValueAxis* y_axis = new QValueAxis();
        y_axis->setTitleText("Percentiles (us)");
        chart->addAxis(y_axis, Qt::AlignLeft);
        series->attachAxis(y_axis);
        chart->legend()->setVisible(true);
        chart->legend()->setAlignment(Qt::AlignLeft);

        QChartView* chartView = new QChartView(chart);
        return chartView;
    }

    void generateStatisticsDialog(API::StatisticsManager& stat_manager, QWidget* statistics_box)
    {
        auto& global_stat_map = stat_manager.getGlobalStatisticsMap();
        auto& global_stat_order = stat_manager.getGlobalStatisticsOrderVector();
        QChartView* percentile_chart = createBarChartForPercentiles(global_stat_map, global_stat_order);
        auto& chart_data_map = stat_manager.getChartDataMap();
        QChart* chart = new QChart();

        QDateTimeAxis* x_axis = new QDateTimeAxis;
        x_axis->setFormat("hh:mm:ss.zzz");
        x_axis->setTickCount(10);
        x_axis->setTitleText("Time");
        chart->addAxis(x_axis, Qt::AlignBottom);

        QValueAxis *y_axis = new QValueAxis;
        y_axis->setLabelFormat("%i");
        y_axis->setTitleText("Number of Event");
        chart->addAxis(y_axis, Qt::AlignRight);

        QValueAxis *y_latency_axis = new QValueAxis;
        y_latency_axis->setLabelFormat("%i");
        y_latency_axis->setTitleText("Latency(us)");
        chart->addAxis(y_latency_axis, Qt::AlignLeft);

        {
            uint64_t max_value = std::numeric_limits<uint64_t>::min();
            auto& numbers = chart_data_map["Number Of Events By Time"];
            QLineSeries* series = new QLineSeries();
            for(auto& data : numbers)
            {
                series->append(data.first, data.second);
                if(max_value < data.second)
                    max_value = data.second;
                }
                chart->addSeries(series);
                series->setName(QString("Number Of Events By Time"));
                series->attachAxis(x_axis);
                series->attachAxis(y_axis);
                y_axis->setRange(0, max_value);
        }
        {
            uint64_t max_value = std::numeric_limits<uint64_t>::min();
            auto& numbers = chart_data_map["Latency by Time"];
            QLineSeries* series = new QLineSeries();
            for(auto& data : numbers)
            {
                series->append(data.first, data.second);
                if(max_value < data.second)
                        max_value = data.second;
            }
            chart->addSeries(series);
            series->setName(QString("Latency by Time"));
            series->attachAxis(x_axis);
            series->attachAxis(y_latency_axis);
            y_latency_axis->setRange(0, max_value);
        }

        chart->setTitle("Number of Events By Time / Latency By Time");
        QChartView *chartView = new API::ChartView(chart);

        QVBoxLayout* statistics_layout_vertical = new QVBoxLayout();
        statistics_layout_vertical->addWidget(percentile_chart);
        statistics_layout_vertical->addWidget(chartView);
        statistics_box->setLayout(statistics_layout_vertical);
    }
}
