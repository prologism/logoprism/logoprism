#ifndef DIALOG_GENERATORS_H
#define DIALOG_GENERATORS_H

/**
 * @file DialogGenerators.h
 * @brief Generator functions to generate a QT Widget to show event information.
 */

class QWidget;
namespace LOGOPRISM
{
    namespace API { class EventBase; }
    void generateDefaultDialogBox(API::EventBase* event, QWidget* dialog_box);
}

#endif
