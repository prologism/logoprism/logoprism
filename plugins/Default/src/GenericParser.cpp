#include "GenericParser.h"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/lexical_cast.hpp>
#include "logoprism/API/Utils/BoostStringParseTree.h"
#include "logoprism/API/Processors/Controllers/GenericPool.h"
#include "logoprism/API/Utils/Timestamp.h"
#include <iomanip>

namespace LOGOPRISM
{
    GenericParser::GenericParser()
    {
    }

    std::deque<API::EventBase> GenericParser::convert(std::deque<std::string>& lines)
    {
        std::deque<API::EventBase> events;

        for(auto& line : lines)
        {
            // Split the line where the fields are separated by a "|"
            std::vector<std::string> splitted_line;
            boost::split(splitted_line, line, [](char c){return c == '|' ;});

            API::EventBase event;

            if(splitted_line.size() < 4)
                continue;
            try
            {
                uint64_t nanoseconds_start_time = 1000 *  boost::lexical_cast<uint64_t>(splitted_line[0]); // Start Time nanoseconds since epoch
		event.setStartTime(API::Timestamp(API::TimeDuration(nanoseconds_start_time))); 
                
		uint64_t nanoseconds_duration = 1000 * boost::lexical_cast<uint64_t>(splitted_line[1]); // Duration in nanoseconds
                event.setDuration(API::TimeDuration(nanoseconds_duration));
                
            }

            catch(boost::bad_lexical_cast &)
            {
                continue;
            }
			
	    event.setSource(LOGOPRISM::API::Pool<LOGOPRISM::API::Source>::getInstance().tryToGetElementByName(splitted_line[2])); // Source
            event.setTarget(LOGOPRISM::API::Pool<LOGOPRISM::API::Target>::getInstance().tryToGetElementByName(splitted_line[3])); // Destination

            event.setValid(true);
            events.push_back(event);
        }

        return events;
    }
}
