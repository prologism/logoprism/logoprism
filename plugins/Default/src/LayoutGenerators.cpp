#include "LayoutGenerators.h"

#include "logoprism/API/Visualisers/Controllers/ViewManager.h"

#include <QGraphicsLinearLayout>
#include <QGraphicsScene>
#include <QGraphicsAnchorLayout>

namespace LOGOPRISM
{
	void generateDefaultLayout(API::ViewManager& manager)
	{
        auto source_list = new QGraphicsWidget();
        auto source_layout = new QGraphicsLinearLayout(Qt::Vertical);
        source_list->setLayout(source_layout);
        manager.getGraphicsScene()->addItem(source_list);
        manager.tryAddLayoutWidget("Sources", source_list);

        auto worker_list = new QGraphicsWidget();
        auto worker_layout = new QGraphicsLinearLayout(Qt::Vertical);
        worker_list->setLayout(worker_layout);
        manager.getGraphicsScene()->addItem(worker_list);
        manager.tryAddLayoutWidget("Workers", worker_list);

        auto target_list = new QGraphicsWidget();
        auto target_layout = new QGraphicsLinearLayout(Qt::Vertical);
        target_list->setLayout(target_layout);
        manager.getGraphicsScene()->addItem(target_list);
        manager.tryAddLayoutWidget("Targets", target_list);

        auto top_widget = new QGraphicsWidget();
        auto top_widget_layout = new QGraphicsAnchorLayout();
        top_widget_layout->setHorizontalSpacing(300);
        top_widget_layout->setVerticalSpacing(500);

        top_widget_layout->addCornerAnchors(top_widget_layout, Qt::TopLeftCorner, source_list, Qt::TopLeftCorner);
        top_widget_layout->addAnchor(source_list, Qt::AnchorVerticalCenter, worker_list, Qt::AnchorVerticalCenter);
        top_widget_layout->addAnchor(source_list, Qt::AnchorRight, worker_list, Qt::AnchorLeft);
        top_widget_layout->addAnchor(worker_list, Qt::AnchorVerticalCenter, target_list, Qt::AnchorVerticalCenter);
        top_widget_layout->addAnchor(worker_list, Qt::AnchorRight, target_list, Qt::AnchorLeft);

        top_widget->setLayout(top_widget_layout);
        top_widget->setPos(0,0);
        manager.getGraphicsScene()->addItem(top_widget);
	}
}
