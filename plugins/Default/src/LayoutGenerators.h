#ifndef LAYOUT_GENERATORS_H
#define LAYOUT_GENERATORS_H

/**
 * @file LayoutGenerators.h
 * @brief Generator functions to generate a QT layout on the QGraphicsScene.
 * A ViewManager is passed to define the layout.
 */

namespace LOGOPRISM
{
	namespace API { class ViewManager; }
    void generateDefaultLayout(API::ViewManager& manager);
}

#endif
