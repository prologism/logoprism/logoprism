#include "DialogGenerators.h"

#include "logoprism/API/Processors/Models/EventBase.h"

#include <QWidget>
#include <QLabel>
#include <QGridLayout>

namespace LOGOPRISM
{
    void generateDefaultDialogBox(API::EventBase* event, QWidget* dialog_box)
    {
        dialog_box->setWindowTitle("Event Information");
        QGridLayout* dialog_layout = new QGridLayout();
        dialog_box->setLayout(dialog_layout);

        QLabel* source_header_label = new QLabel(QString("Source: "), dialog_box);
        dialog_layout->addWidget(source_header_label, 0, 0);
        QLabel* source_value_label = new QLabel(event->getSource()->getName().c_str(), dialog_box);
        dialog_layout->addWidget(source_value_label, 0, 1);

        QLabel* worker_header_label = new QLabel(QString("Worker: "));
        dialog_layout->addWidget(worker_header_label, 1, 0);
        QLabel* worker_value_label = new QLabel(event->getWorker()->getName().c_str(), dialog_box);
        dialog_layout->addWidget(worker_value_label, 1, 1);

        QLabel* target_header_label = new QLabel(QString("Target: "));
        dialog_layout->addWidget(target_header_label, 2, 0);
        QLabel* target_value_label = new QLabel(event->getTarget()->getName().c_str(), dialog_box);
        dialog_layout->addWidget(target_value_label, 2, 1);

        QLabel* message_ts_header_label = new QLabel(QString("Message Timestamp: "));
        dialog_layout->addWidget(message_ts_header_label, 3, 0);
        QLabel* message_ts_value_label = new QLabel(QString((API::getStringFromTimeStamp(event->getStartTime()) + " (" + std::to_string(event->getStartTime().time_since_epoch().count()) + ")").c_str()), dialog_box);
        dialog_layout->addWidget(message_ts_value_label, 3, 1);

        QLabel* message_duration_header_label = new QLabel(QString("Message Duration: "));
        dialog_layout->addWidget(message_duration_header_label, 4, 0);
        QLabel* message_duration_value_label = new QLabel(QString(std::to_string(event->getDuration().count()).c_str()), dialog_box);
        dialog_layout->addWidget(message_duration_value_label, 4, 1);
    }
}
