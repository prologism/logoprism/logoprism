#ifndef VIEW_GENERATORS_H
#define VIEW_GENERATORS_H

/**
 * @file ViewGenerators.h
 * @brief View generators for layouts which generates views based on the received vector
 * This file contains the declaration of view generators. View generators receives a list of events to display
 * and it is up to the generator function how the views are generated based on the received events.
 * The ViewManager parameter is there to retrieve layout elements defined in the specified layout.
 * For example, if your layout contains a list and you want to organize your views in a list then you must retrieve
 * your list by the ViewManager and push your views into it.
 */


#include <vector>
#include <memory>
#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Visualisers/Controllers/ViewManager.h"

namespace LOGOPRISM
{
	void generateDefaultView(std::vector<std::shared_ptr<API::EventBase>>& events, API::ViewManager& manager);
}

#endif
