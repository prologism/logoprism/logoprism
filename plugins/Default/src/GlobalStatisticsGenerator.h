#ifndef GLOBAL_STATISTICS_GENERATORS_H
#define GLOBAL_STATISTICS_GENERATORS_H

class QWidget;
namespace LOGOPRISM
{
    namespace API { class StatisticsManager; }
    void generateStatisticsDialog(API::StatisticsManager& stat_manager, QWidget* statistics_box);
}

#endif
