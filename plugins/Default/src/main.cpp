/**
 * @file main.cpp
 * @brief Entry point to the Logoprism core.
 * In the main function the developer must define its provider chain in order
 * to do the serialization and deserialization process.
 * Also a statistics manager must be instantiated. To display the events layouts
 * must be defined as well with their generator function.
 */

#include <string>
#include <vector>
#include <memory>

#include "FileReader.h"
#include "Deserializer.h"
#include "GenericParser.h"
#include "ViewGenerators.h"
#include "LayoutGenerators.h"
#include "GlobalStatisticsGenerator.h"
#include "SnapStatisticsGenerator.h"

#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Processors/Controllers/RawProvider.h"
#include "logoprism/API/Processors/Controllers/IEventManager.h"
#include "logoprism/API/Processors/Controllers/EventConverter.h"
#include "logoprism/API/Processors/Controllers/IThreadManager.h"
#include "logoprism/API/Processors/Controllers/EventManager.h"
#include "logoprism/API/Processors/Controllers/SerializeManager.h"
#include "logoprism/API/Visualisers/Controllers/WorkerSimulator.h"
#include "logoprism/API/Utils/Config.h"
#include "logoprism/API/Logoprism.h"


//Using declaration for worker simulation
using WorkerSimulator = LOGOPRISM::API::WorkerSimulator;

// Using declaration for Serialization
using RawProvider = LOGOPRISM::API::RawProvider<std::string, LOGOPRISM::FileReader>;
using EventConverter = LOGOPRISM::API::EventConverter<std::string, LOGOPRISM::API::EventBase, LOGOPRISM::GenericParser>;
using WorkerConverter = LOGOPRISM::API::EventConverter<LOGOPRISM::API::EventBase, LOGOPRISM::API::EventBase, WorkerSimulator>;
using SerializeManager = LOGOPRISM::API::SerializeManager<LOGOPRISM::API::EventBase>;
using SerializeManagerBase = LOGOPRISM::API::SerializeManagerBase;

// Using declaration for Deserialization
using DeserializedProvider = LOGOPRISM::API::RawProvider<LOGOPRISM::API::EventBase, LOGOPRISM::Deserializer>;
using EventManager = LOGOPRISM::API::EventManager<LOGOPRISM::API::EventBase>;
using EventManagerBase = LOGOPRISM::API::IEventManagerBase;

//Using declaration for statistic calculation
using StatisticsManager = LOGOPRISM::API::StatisticsManager;
using GlobalStatistics = LOGOPRISM::API::GlobalStatistics;
using SnapStatistics = LOGOPRISM::API::SnapStatistics;

// Using declaration for view generation and layouts
using ViewGenerator = std::function<void(std::vector<std::shared_ptr<LOGOPRISM::API::EventBase>>&, LOGOPRISM::API::ViewManager&)>;

// Using declaration for statistics view generation
using GlobalStatisticsViewGenerator = std::function<void(StatisticsManager&, QWidget*)>;
using SnapStatisticsViewGenerator = std::function<void(StatisticsManager&, QWidget*, const LOGOPRISM::API::Timestamp& simulation_time, std::vector<std::shared_ptr<LOGOPRISM::API::EventBase>>& events)>;

extern "C" int main(int argc, char* argv[])
{
	// Get command line arguments and pass them to the core
	std::string program = argv[0];
	std::vector< std::string > arguments;

	for (int i = 1; i < argc; ++i)
	{
		arguments.push_back(argv[i]);
	}

	LOGOPRISM::API::Configuration config(program, arguments);
    LOGOPRISM::API::Logoprism application(config, argc, argv);

	// Statistics
	std::shared_ptr<StatisticsManager> stat_manager = std::make_shared<StatisticsManager>();

	// Serialization Process
    std::shared_ptr<LOGOPRISM::FileReader> reader = std::make_shared<LOGOPRISM::FileReader>();
	std::shared_ptr<LOGOPRISM::GenericParser> parser = std::make_shared<LOGOPRISM::GenericParser>();
	std::shared_ptr<WorkerSimulator> worker_simulator = std::make_shared<WorkerSimulator>(50);

	auto serialize_manager = std::make_shared<SerializeManager>();
	std::shared_ptr<SerializeManagerBase> serialize_manager_base(serialize_manager);

	std::shared_ptr<WorkerConverter> computed_event_converter = std::make_shared<WorkerConverter>(serialize_manager, worker_simulator);
	std::shared_ptr<EventConverter> event_converter = std::make_shared<EventConverter>(computed_event_converter, parser);
	std::shared_ptr<RawProvider> raw_provider = std::make_shared<RawProvider>(event_converter, reader);
	serialize_manager->setProviderDriver(raw_provider.get());

	// Deserialization Process
    std::shared_ptr<LOGOPRISM::Deserializer> deserializer = std::make_shared<LOGOPRISM::Deserializer>(serialize_manager_base);

	auto event_manager = std::make_shared<EventManager>();
	std::shared_ptr<EventManagerBase> event_manager_base = event_manager;

    std::shared_ptr<DeserializedProvider> deserialized_provider = std::make_shared<DeserializedProvider>(event_manager, deserializer);
	event_manager->setProviderDriver(deserialized_provider.get());

	//Creation of provider chain vectors
	std::vector<std::shared_ptr<LOGOPRISM::API::IThreadManager>> read_providers { raw_provider, event_converter , computed_event_converter};
    std::vector<std::shared_ptr<LOGOPRISM::API::IThreadManager>> serialize_providers { deserialized_provider };

	// View Settings
    application.getViewManager().setViewGenerator(ViewGenerator(LOGOPRISM::generateDefaultView));
    LOGOPRISM::generateDefaultLayout(application.getViewManager());

    // Statistics View Generator
    application.getViewManager().setGlobalStatisticsGenerator(GlobalStatisticsViewGenerator(LOGOPRISM::generateStatisticsDialog));
    application.getViewManager().setSnapStatisticsGenerator(SnapStatisticsViewGenerator(LOGOPRISM::generateSnapStatisticsDialog));

	// Set Managers
	application.setEventManager(event_manager_base);
	application.setSerializeManager(serialize_manager_base);
	application.setReadProviders(read_providers);
	application.setSerializeProviders(serialize_providers);
	application.setStatisticsManager(stat_manager);
	application.init();
	application.run();
}
