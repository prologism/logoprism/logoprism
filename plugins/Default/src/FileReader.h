#ifndef FILE_READER_H
#define FILE_READER_H

/**
 * @file FileReader.h
 * @brief Contains a reader which reads the file provided in the configuration file.
 */

#include <iostream>
#include <string>
#include <fstream>
#include <deque>

namespace LOGOPRISM
{

	/**
	 *  FileReader to read a line.
	 * 	It reads a line. The separator is the new line character.
	 */
	class FileReader
	{
	private:
		std::ifstream m_File; /**< Stream object to represent a file */

	public:
        FileReader();

		/*
		 * Function to read one or more lines in the file.
		 * Its signature is defined by the interface needed by the Logoprism core framework.
		 */
        std::deque<std::string> getData();

		/*
		 * Checks if the file source is still okay.
		 * Its signature is defined by the interface needed by the Logoprism core framework.
		 */
		bool isDataAvailable() { return m_File.good(); };

        /*
         * Modifies file offset.
         * Its signature is defined by the interface needed by the Logoprism core framework.
         */
        void modifyOffset(size_t offset) { m_File.seekg(offset); }

        /*
         * Sets the source.
         * Its signature is defined by the interface needed by the Logoprism core framework.
         */
        void setSource(const std::string& source) { m_File.open(source); }
	};
}

#endif
