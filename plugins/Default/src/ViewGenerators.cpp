#include "ViewGenerators.h"
#include "DialogGenerators.h"

#include "logoprism/API/Visualisers/Views/GraphicsLayoutTextItem.h"
#include "logoprism/API/Visualisers/Views/GraphicsCircleItem.h"

#include <QString>
#include <QGraphicsWidget>
#include <QGraphicsLinearLayout>
#include <QGraphicsScene>

namespace LOGOPRISM
{
	void generateDefaultView(std::vector<std::shared_ptr<API::EventBase>>& events, API::ViewManager& manager)
	{
		// Retrieve Layout Element
        QGraphicsWidget* sources = manager.tryGetLayoutWidgetById("Sources");
        QGraphicsLinearLayout* source_layout = dynamic_cast<QGraphicsLinearLayout*>(sources->layout());

        QGraphicsWidget* workers = manager.tryGetLayoutWidgetById("Workers");
        QGraphicsLinearLayout* worker_layout = dynamic_cast<QGraphicsLinearLayout*>(workers->layout());

        QGraphicsWidget* targets = manager.tryGetLayoutWidgetById("Targets");
        QGraphicsLinearLayout* target_layout = dynamic_cast<QGraphicsLinearLayout*>(targets->layout());

        API::GraphicsCircleItem::setDialogGenerator(std::function<void(API::EventBase*, QWidget*)>(generateDefaultDialogBox));

		// Generate Views For All Events
		for(auto& element : events)
		{
			// Generate Static View Elements - Source list on the left
            std::string source_id = element->getSource()->getId();
            auto source_base = manager.tryGetStaticViewById(source_id);
            if(source_base == nullptr)
            {
                auto source = new API::GraphicsLayoutTextItem(QString(element->getSource()->getName().c_str()));
                source->setFont(QFont("Times", 12, QFont::Normal));
                source_base = source;
                manager.tryAddStaticView(source_id, source_base);
                source_layout->addItem(source);
            }

			// Generate Static View Elements - Worker list on the middle
            std::string worker_id = element->getWorker()->getId();
            auto worker_base = manager.tryGetStaticViewById(worker_id);
            if(worker_base == nullptr)
            {
                auto worker = new API::GraphicsLayoutTextItem(QString(element->getWorker()->getName().c_str()));
                worker->setFont(QFont("Times", 12, QFont::Normal));
                worker_base = worker;
                worker->setColor(source_base->getColor());
                manager.tryAddStaticView(worker_id, worker_base);
                worker_layout->addItem(worker);
            }

			// Generate Static View Elements - Target list on the right
            std::string target_id = element->getTarget()->getId();
            auto target_base = manager.tryGetStaticViewById(target_id);
            if(target_base == nullptr)
            {
                auto target = new API::GraphicsLayoutTextItem(QString(element->getTarget()->getName().c_str()));
                target->setFont(QFont("Times", 12, QFont::Normal));
                target_base = target;
                target->setColor(source_base->getColor());
                manager.tryAddStaticView(target_id, target_base);
                target_layout->addItem(target);
            }

			//Generate Dynamic View Elements
            {
                API::GraphicsCircleItem* item = new API::GraphicsCircleItem(source_base, worker_base, element->getStartTime() - element->getDuration(), element->getDuration(), source_base->getColor());
                item->setEvent(element.get());
                manager.getGraphicsScene()->addItem(item);
                manager.addDynamicView(item);
            }

			//Generate Dynamic View Elements
            {
                API::GraphicsCircleItem* item = new API::GraphicsCircleItem(worker_base, target_base, element->getStartTime(), element->getDuration(), source_base->getColor());
                item->setEvent(element.get());
                manager.getGraphicsScene()->addItem(item);
                manager.addDynamicView(item);
            }
		}
	}
}
