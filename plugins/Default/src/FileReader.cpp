#include "FileReader.h"

namespace LOGOPRISM
{

    FileReader::FileReader() : m_File() { }
    std::deque<std::string> FileReader::getData()
	{
        std::deque<std::string> out;
		if(m_File.good())
		{
			std::string str;
			std::getline(m_File, str, '\n');
			out.push_back(str);
		}
		return out;
	}
}
