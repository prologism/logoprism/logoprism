#include "Deserializer.h"

#include <iostream>

namespace LOGOPRISM
{
    Deserializer::Deserializer(const std::shared_ptr<API::SerializeManagerBase>& manager)
    : m_File()
	, m_DeSerializer(m_File)
	, m_SerializeManager(manager)
    , m_Size(0)
	{}

    long Deserializer::getFileSize()
    {
        if(m_Size == 0)
        {
            std::streampos begin, current, end;
            current = m_File.tellg();

            m_File.seekg (0, std::ios::beg);
            begin = m_File.tellg();

            m_File.seekg (0, std::ios::end);
            end = m_File.tellg();

            m_File.seekg (current);
            m_Size = end - begin;
        }
        return m_Size;
    }

    std::deque<LOGOPRISM::API::EventBase> Deserializer::getData()
	{
        std::deque<LOGOPRISM::API::EventBase> out;
        std::lock_guard<std::mutex> lk(m_Lock);
        if( m_File.tellg() < getFileSize() && m_File.good())
		{
			LOGOPRISM::API::EventBase event;
			try
			{
				m_DeSerializer(event);
				out.push_back(event);
			}
			catch(cereal::Exception& e)
			{
				std::clog << "Exception happened: " << e.what() << std::endl;
			}
		}
		return out;
	}
}
