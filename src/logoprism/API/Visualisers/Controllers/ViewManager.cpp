#include "logoprism/API/Statistics/StatisticsManager.h"
#include "logoprism/API/Visualisers/Controllers/ViewManager.h"
#include "logoprism/API/Visualisers/Views/LogoprismWidget.h"
#include "logoprism/API/Visualisers/Views/GraphicsCircleItem.h"
#include "logoprism/API/Visualisers/Views/GraphicsTextItem.h"

namespace LOGOPRISM
{
	namespace API
	{
		ViewManager::ViewManager(TimeInfo& time_info)
        : m_TimeInfo(time_info)
        , m_StatisticsManager(nullptr)
        , m_GenerateSnapStatistics(false)
		{
			m_MainWindow.show();
		}

		IBaseGraphicsItem* ViewManager::tryGetStaticViewById(const std::string& id)
		{
			auto view = m_StaticViewContainer.find(id);
			if(view == m_StaticViewContainer.end()) return nullptr;
			else return view->second;
		}

		bool ViewManager::isStaticViewExistWithId(const std::string& id) const
		{
			auto view = m_StaticViewContainer.find(id);
			if(view != m_StaticViewContainer.end()) return true;
			else return false;
		}

        std::pair<bool, IBaseGraphicsItem*> ViewManager::tryAddStaticView(const std::string& id, IBaseGraphicsItem* view)
		{
			auto view_it = m_StaticViewContainer.find(id);
			if(view_it != m_StaticViewContainer.end())
			{
                return std::make_pair(false, view_it->second);
			}
			else
			{
				m_StaticViewContainer.insert(std::make_pair(id, view));
                return std::make_pair(true, view);
			}
		}

		bool ViewManager::tryRemoveStaticView(const std::string& id)
		{
			auto view = m_StaticViewContainer.find(id);
			if(view != m_StaticViewContainer.end())
			{
				m_StaticViewContainer.erase(view);
				return true;
			}
			return false;
		}

		QGraphicsWidget* ViewManager::tryGetLayoutWidgetById(const std::string& id)
		{
			auto widget = m_LayoutWidgetsById.find(id);
			if(widget == m_LayoutWidgetsById.end()) return nullptr;
			else return widget->second;
		}

        std::pair<bool, QGraphicsWidget*> ViewManager::tryAddLayoutWidget(const std::string& id, QGraphicsWidget* widget)
        {
			auto widget_it = m_LayoutWidgetsById.find(id);
			if(widget_it != m_LayoutWidgetsById.end())
			{
                return std::make_pair(false, widget_it->second);
			}
			else
			{
				m_LayoutWidgetsById.insert(std::make_pair(id, widget));
                return std::make_pair(true, widget);
			}
        }

		bool ViewManager::isLayoutWidgetExistWithId(const std::string& id) const
		{
			auto widget = m_LayoutWidgetsById.find(id);
			if(widget != m_LayoutWidgetsById.end()) return true;
			else return false;
		}

		bool ViewManager::tryRemoveLayoutWidget(const std::string& id)
		{
			auto widget = m_LayoutWidgetsById.find(id);
			if(widget != m_LayoutWidgetsById.end())
			{
				m_LayoutWidgetsById.erase(widget);
				return true;
			}
			return false;
		}

		void ViewManager::addDynamicView(IBaseGraphicsItem* item)
		{
			m_DynamicViewContainer.push_back(item);
		}

		void ViewManager::generateViews(std::vector<std::shared_ptr<EventBase>>& events)
		{
            m_ViewGenerator(events, *this);
		}

		const TimeInfo& ViewManager::getTimeInfo() const
		{
			return m_TimeInfo;
		}

		void ViewManager::setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager)
		{
			m_StatisticsManager = manager;
		}

		std::shared_ptr<StatisticsManager> ViewManager::getStatisticsManager()
		{
			return m_StatisticsManager;
		}

		void ViewManager::drawViews(std::vector<std::shared_ptr<EventBase>>& events)
		{
            getControlPanel()->setSimulationTime(m_TimeInfo.getSimulationTime());

            if(m_TimeInfo.isKeyFrame())
			{
                for(auto& element : m_DynamicViewContainer)
				{
                    delete element;
                }
                m_DynamicViewContainer.clear();
				generateViews(events);
			}

			if(!m_DynamicViewContainer.empty())
			{
				for(auto& element : m_DynamicViewContainer)
				{
                    element->calculateInternalState(m_TimeInfo);
				}
			}

            if(m_GenerateSnapStatistics)
            {
               m_GenerateSnapStatistics = false;
               m_StatisticsManager->resetSnapStatistics();

               auto it = std::remove_if(events.begin(), events.end(), [&](std::shared_ptr<API::EventBase> ptr)
               {
                   return !(ptr->getStartTime() < m_TimeInfo.getSimulationTime() && ptr->getStartTime() + ptr->getDuration() > m_TimeInfo.getSimulationTime());
               });
               events.erase(it, events.end());
               m_StatisticsManager->calculateSnapStatistics(events);
               generateSnapStatistics(events);
            }
		}

		QGraphicsScene* ViewManager::getGraphicsScene()
		{
			return m_MainWindow.getLogoprismWidget()->getScene();
		}

        ControlPanel::ControlEntries& ViewManager::getControlEntries()
        {
            return m_MainWindow.getLogoprismWidget()->getControlPanel()->getControlEntries();
        }

        ControlPanel* ViewManager::getControlPanel()
        {
            return m_MainWindow.getLogoprismWidget()->getControlPanel();
        }

		void ViewManager::setViewGenerator(std::function<void(std::vector<std::shared_ptr<EventBase>>&, ViewManager&)> generator)
		{
			m_ViewGenerator = generator;
		}

        void ViewManager::setGlobalStatisticsGenerator(std::function<void(StatisticsManager&, QWidget*)> generator)
        {
            m_GlobalStatisticsGenerator = generator;
        }

        void ViewManager::setSnapStatisticsGenerator(std::function<void(StatisticsManager&, QWidget*, const Timestamp& simulation_time, std::vector<std::shared_ptr<EventBase>>& events)> generator)
        {
            m_SnapStatisticsGenerator = generator;
        }

        void ViewManager::generateGlobalStatistics()
        {
            QWidget* dialog_box = new QWidget();
            dialog_box->setAttribute(Qt::WA_DeleteOnClose);
            dialog_box->setMinimumSize(800, 600);
            dialog_box->setWindowTitle("Global Statistics");
            m_GlobalStatisticsGenerator(*m_StatisticsManager, dialog_box);
            dialog_box->show();
        }

        void ViewManager::showSnapStatistics()
        {
            m_GenerateSnapStatistics = true;
        }

        void ViewManager::generateSnapStatistics(std::vector<std::shared_ptr<EventBase>>& events)
        {
            QWidget* dialog_box = new QWidget();
            dialog_box->setAttribute(Qt::WA_DeleteOnClose);
            dialog_box->setMinimumSize(800, 600);
            std::string dialog_title = std::string("Snap Statistics at: ") + getStringFromTimeStamp(m_TimeInfo.getSimulationTime());
            dialog_box->setWindowTitle(dialog_title.c_str());
            m_SnapStatisticsGenerator(*m_StatisticsManager, dialog_box, m_TimeInfo.getSimulationTime(), events);
            dialog_box->show();
        }
	}
}
