#ifndef VIEW_MANAGER_H
#define VIEW_MANAGER_H

#include <vector>
#include <map>
#include <memory>
#include <QGraphicsWidget>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"
#include "logoprism/API/Visualisers/Controllers/TimeInfo.h"
#include "logoprism/API/Statistics/StatisticsManager.h"
#include "logoprism/API/Visualisers/Views/MainWindow.h"

namespace LOGOPRISM
{
	namespace API
	{
		class ViewManager
		{
		private:
			std::map<std::string, QGraphicsWidget*> m_LayoutWidgetsById;
			std::map<std::string, IBaseGraphicsItem*> m_StaticViewContainer;
			std::vector<IBaseGraphicsItem*> m_DynamicViewContainer;

			TimeInfo& m_TimeInfo;
			MainWindow m_MainWindow;
			std::shared_ptr<StatisticsManager> m_StatisticsManager;
			std::function<void(std::vector<std::shared_ptr<EventBase>>&, ViewManager&)> m_ViewGenerator;
            std::function<void(StatisticsManager&, QWidget*)> m_GlobalStatisticsGenerator;
            std::function<void(StatisticsManager&, QWidget*, const Timestamp& simulation_time, std::vector<std::shared_ptr<EventBase>>& events)> m_SnapStatisticsGenerator;
            bool m_GenerateSnapStatistics;

		public:
			ViewManager(TimeInfo& time_info);
			ViewManager(const ViewManager& other) = default;
			ViewManager& operator=(const ViewManager& other) = default;
			ViewManager(ViewManager&& other) = default;
			ViewManager& operator=(ViewManager&& other) = default;
			~ViewManager() {};

			void generateViews(std::vector<std::shared_ptr<EventBase>>& events);

			IBaseGraphicsItem* tryGetStaticViewById(const std::string& id);
            std::pair<bool, IBaseGraphicsItem*> tryAddStaticView(const std::string& id, IBaseGraphicsItem* view);
			bool isStaticViewExistWithId(const std::string& id) const;
			bool tryRemoveStaticView(const std::string& id);
			void addDynamicView(IBaseGraphicsItem* item);
			void drawViews(std::vector<std::shared_ptr<EventBase>>& events);

			QGraphicsWidget* tryGetLayoutWidgetById(const std::string& id);
            std::pair<bool, QGraphicsWidget*> tryAddLayoutWidget(const std::string& id, QGraphicsWidget* widget);
			bool isLayoutWidgetExistWithId(const std::string& id) const;
			bool tryRemoveLayoutWidget(const std::string& id);

			const TimeInfo& getTimeInfo() const;
			QGraphicsScene* getGraphicsScene();
            ControlPanel::ControlEntries& getControlEntries();
            ControlPanel* getControlPanel();
			std::shared_ptr<StatisticsManager> getStatisticsManager();

			void setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager);
			void setViewGenerator(std::function<void(std::vector<std::shared_ptr<EventBase>>&, ViewManager&)>);
            void setGlobalStatisticsGenerator(std::function<void(StatisticsManager&, QWidget*)> generator);
            void generateGlobalStatistics();
            void setSnapStatisticsGenerator(std::function<void(StatisticsManager&, QWidget*, const Timestamp& simulation_time, std::vector<std::shared_ptr<EventBase>>& events)> generator);
            void generateSnapStatistics(std::vector<std::shared_ptr<EventBase>>& events);
            void showSnapStatistics();
		};
	}
}

#endif
