#include "WorkerSimulator.h"
#include "logoprism/API/Processors/Controllers/GenericPool.h"

#include <cmath>
#include "logoprism/API/Utils/Timestamp.h"

namespace LOGOPRISM
{
	namespace API
	{
		WorkerSimulator::WorkerSimulator(uint64_t worker_count, TimeDuration time_resolution)
		: m_WorkerCount(worker_count)
		, m_LastWorkerIndex(0)
		, m_TimeResolution(time_resolution)
		{
			for (uint64_t worker_name = 0; worker_name < worker_count; ++worker_name)
			{
				this->m_WorkerEndTimeByWorkerName[std::to_string(worker_name)] = INVALID_TIMESTAMP;
			}
		}

        std::deque<WorkerSimulator::Event> WorkerSimulator::convert(std::deque<WorkerSimulator::Event>& events)
		{
            std::deque<WorkerSimulator::Event> ret;
            for(auto& event : events)
            {
                if (event.getWorker() == nullptr)
                {
                    for (auto& worker_pair : m_WorkerEndTimeByWorkerName)
                    {
                        if (worker_pair.second == INVALID_TIMESTAMP)
                            worker_pair.second = event.getStartTime();

                        if (worker_pair.second < event.getStartTime() + m_TimeResolution)
                        {
                            worker_pair.second = std::max(worker_pair.second, event.getStartTime());
                            worker_pair.second += event.getDuration() + std::chrono::duration_cast<TimeDuration>(std::chrono::microseconds(1000));
                            event.setStartTime(worker_pair.second);

                            event.setWorker(LOGOPRISM::API::Pool<LOGOPRISM::API::Worker>::getInstance().tryToGetElementByName(worker_pair.first));
                            break;
                        }
                    }

                    if (event.getWorker() == nullptr)
                    {
                        m_WorkerEndTimeByWorkerName[std::to_string(m_WorkerCount)] = event.getStartTime() + event.getDuration();
                        event.setWorker(LOGOPRISM::API::Pool<LOGOPRISM::API::Worker>::getInstance().tryToGetElementByName(std::to_string(m_WorkerCount)));
                        m_WorkerCount++;
                    }
                }
                else
                {
                    if (m_WorkerEndTimeByWorkerName.find(event.getWorker()->getName()) == m_WorkerEndTimeByWorkerName.end())
                    {
                        m_WorkerEndTimeByWorkerName[event.getWorker()->getName()] = event.getStartTime() + event.getDuration();
                        m_WorkerCount++;
                    }
                    else
                    {
                        if(m_WorkerEndTimeByWorkerName[event.getWorker()->getName()] == INVALID_TIMESTAMP)
                        {
                            m_WorkerEndTimeByWorkerName[event.getWorker()->getName()] = event.getStartTime() + event.getDuration();
                        }
                        else
                        {
                            if(m_WorkerEndTimeByWorkerName[event.getWorker()->getName()] > event.getStartTime())
                            {
                                event.setStartTime(m_WorkerEndTimeByWorkerName[event.getWorker()->getName()]);
                            }
                            m_WorkerEndTimeByWorkerName[event.getWorker()->getName()] = event.getStartTime() + event.getDuration();
                        }
                    }
                }

                ret.push_back(event);
            }

            return ret;
		}
	}
}
