#include "TimeInfo.h"

namespace LOGOPRISM
{
	namespace API
	{
        TimeInfo::TimeInfo()
		:	m_Time(INVALID_TIMESTAMP)
		,	m_Timelapse(TimeDuration(0))
		,   m_SimulationReferenceTime(INVALID_TIMESTAMP)
		,	m_SimulationTime(INVALID_TIMESTAMP)
		,	m_SimulationTimelapse(TimeDuration(0))
        ,	m_SimulationSpeed(1.0)
        ,   m_SimulationPaused(true)
		,	m_KeyFrameTime(INVALID_TIMESTAMP)
		,	m_KeyFrame(false)
		,	m_SkipTimelapse(TimeDuration(0))
        ,   m_KeyFrameDuration(std::chrono::milliseconds(1))
		,   m_TimelapseDuration(TimeDuration(0))
		{}

		double TimeInfo::floatingSeconds(const TimeDuration& duration)
    	{
    		return duration.count() / TimeInfo::NANOSECOND_DIVISOR;
    	}

		int64_t TimeInfo::floatingNanoSeconds(const TimeDuration& duration)
    	{
    		return duration.count();
    	}

		const Timestamp& TimeInfo::getTime() const
		{
			return m_Time;
		}

		const Timestamp& TimeInfo::getSimulationReferenceTime() const
		{
			return m_SimulationReferenceTime;
		}

		const Timestamp& TimeInfo::getSimulationTime() const
		{
			return m_SimulationTime;
		}

		const Timestamp& TimeInfo::getKeyFrameTime() const
		{
			return m_KeyFrameTime;
		}

		const TimeDuration& TimeInfo::getTimelapse() const
		{
			return m_Timelapse;
		}

		const TimeDuration& TimeInfo::getSimulationTimelapse() const
		{
			return m_SimulationTimelapse;
		}

		const TimeDuration& TimeInfo::getSkipTimelapse() const
		{
			return m_SkipTimelapse;
		}

		bool TimeInfo::isKeyFrame() const
		{
			return m_KeyFrame;
		}

		bool TimeInfo::isSimulationPaused() const
		{
			return m_SimulationPaused;
		}

		void TimeInfo::togglePause()
		{
			m_SimulationPaused = !m_SimulationPaused;
		}

		double TimeInfo::getSimulationSpeed() const
		{
			return isSimulationPaused() ? 0.0 : m_SimulationSpeed;
		}

		double TimeInfo::getRealSimulationSpeed() const
		{
			return m_SimulationSpeed;
		}


		const TimeDuration& TimeInfo::getTimelapseDuration() const
		{
			return m_TimelapseDuration;
		}

		const TimeDuration& TimeInfo::getKeyFrameDuration() const
		{
			return m_KeyFrameDuration;
		}

		void TimeInfo::setTime(const Timestamp& time)
		{
			m_Time = time;
		}

		void TimeInfo::setSimulationTime(const Timestamp& time)
		{
			m_SimulationTime = time;
		}

		void TimeInfo::setSimulationReferenceTime(const Timestamp& time)
		{
			if(time != INVALID_TIMESTAMP)
				m_SimulationReferenceTime = time;
		}

		void TimeInfo::setKeyFrameTime(const Timestamp& time)
		{
			m_KeyFrameTime = time;
		}

		void TimeInfo::setTimelapse(const TimeDuration& duration)
		{
			m_Timelapse = duration;
		}

		void TimeInfo::setSimulationTimelapse(const TimeDuration& duration)
		{
			m_SimulationTimelapse = duration;
		}

		void TimeInfo::setSkipTimelapse(const TimeDuration& duration)
		{
			m_SkipTimelapse = duration;
		}

		void TimeInfo::setKeyFrame(bool key_frame)
		{
			m_KeyFrame = key_frame;
		}

		void TimeInfo::setSimulationSpeed(double speed)
		{
			m_SimulationSpeed = speed;
		}

		void TimeInfo::setTimelapseDuration(const TimeDuration& duration)
		{
			m_TimelapseDuration = duration;
		}

		void TimeInfo::setKeyFrameDuration(const TimeDuration& duration)
		{
			m_KeyFrameDuration = duration;
		}

	    void TimeInfo::update(bool simulation_in_progress, bool direction_reversed)
	    {
			Timestamp previous_time = m_Time;

			if(m_Time == INVALID_TIMESTAMP)
				m_Time = TimeClock::now();

			if (m_KeyFrameTime == INVALID_TIMESTAMP)
				m_KeyFrameTime = TimeClock::now();

			if(m_SimulationTime == INVALID_TIMESTAMP)
				m_SimulationTime = m_SimulationReferenceTime;

			if (m_TimelapseDuration != TimeDuration(0))
				m_Time += m_TimelapseDuration;
			else
				m_Time = TimeClock::now();

			if (previous_time != INVALID_TIMESTAMP)
				m_Timelapse = m_Time - previous_time + m_SkipTimelapse;
			else
				m_Timelapse = TimeDuration(0);

			if(simulation_in_progress)
			{
				if(m_SkipTimelapse == TimeDuration(0))
				{
					m_SimulationTimelapse = TimeDuration(static_cast<int64_t>(m_Timelapse.count() * getSimulationSpeed()));
				}
				else
				{
					m_SimulationTimelapse = TimeDuration(static_cast<int64_t>(m_Timelapse.count()));
				}
				m_SkipTimelapse = TimeDuration(0);

				if(!direction_reversed) m_SimulationTime += m_SimulationTimelapse;
				else m_SimulationTime -= m_SimulationTimelapse;

			}

			m_KeyFrame  = (m_Time - m_KeyFrameTime) >= (m_KeyFrameDuration / std::max(1.0, getSimulationSpeed()));
			m_KeyFrame |= previous_time >= m_Time;

			if (m_KeyFrame)
				m_KeyFrameTime = m_Time;

	    }
	}
}
