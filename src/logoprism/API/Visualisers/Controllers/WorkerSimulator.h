#ifndef WORKER_SIMULATOR_H
#define WORKER_SIMULATOR_H

#include "logoprism/API/Processors/Models/EventBase.h"
#include <map>
#include <string>

namespace LOGOPRISM
{
	namespace API
	{
		class WorkerSimulator
		{
			using Event = LOGOPRISM::API::EventBase;
		public:
			WorkerSimulator(uint64_t worker_count = 0, TimeDuration time_resolution = TimeDuration(std::chrono::duration_cast<TimeDuration>(std::chrono::microseconds(10000))));

            std::deque<Event> convert(std::deque<Event>& event);
			bool isDataValid(const Event& event) { return event.isValid(); }

		protected:
			uint64_t m_WorkerCount;
			uint64_t m_LastWorkerIndex;
			TimeDuration m_TimeResolution;
			std::map<std::string, Timestamp > m_WorkerEndTimeByWorkerName;
		};
	}
}

#endif
