#ifndef TIME_INFO_H
#define TIME_INFO_H

#include <ostream>
#include "logoprism/API/Utils/Timestamp.h"
#include <boost/date_time.hpp>

namespace LOGOPRISM
{
	namespace API
	{
      	class TimeInfo
		{
		public:
      		static double floatingSeconds(const TimeDuration& duration);
      		static int64_t floatingNanoSeconds(const TimeDuration& duration);
      		static constexpr double NANOSECOND_DIVISOR = 1000000000.0;

            TimeInfo();
    		TimeInfo(const TimeInfo& other) = default;
    		TimeInfo& operator=(const TimeInfo& other) = default;
    		TimeInfo(TimeInfo&& other) = default;
    		TimeInfo& operator=(TimeInfo&& other) = default;
			~TimeInfo() {};

		private:
    		Timestamp  m_Time;
    		TimeDuration m_Timelapse;

    		Timestamp m_SimulationReferenceTime;
    		Timestamp  m_SimulationTime;
    		TimeDuration m_SimulationTimelapse;
    		double m_SimulationSpeed;
	        bool   m_SimulationPaused;

    		Timestamp m_KeyFrameTime;
    		bool m_KeyFrame;

    		TimeDuration m_SkipTimelapse;

    		TimeDuration m_KeyFrameDuration;
    		TimeDuration m_TimelapseDuration;

		public:
    		const Timestamp& getTime() const;
    		const Timestamp& getSimulationReferenceTime() const;
    		const Timestamp& getSimulationTime() const;
    		const Timestamp& getKeyFrameTime() const;
    		const TimeDuration& getSkipTimelapse() const;
    		const TimeDuration& getKeyFrameDuration() const;

    		const TimeDuration& getTimelapse() const;
    		const TimeDuration& getSimulationTimelapse() const;
    		const TimeDuration& getTimelapseDuration() const;


    		bool isKeyFrame() const;

    		double getSimulationSpeed() const;
    		double getRealSimulationSpeed() const;
    		bool isSimulationPaused() const;
    		void togglePause();

    		void setTime(const Timestamp&);
    		void setSimulationTime(const Timestamp&);
    		void setSimulationReferenceTime(const Timestamp&);
    		void setKeyFrameTime(const Timestamp&);
    		void setKeyFrameDuration(const TimeDuration&);

    		void setTimelapse(const TimeDuration&);
    		void setSimulationTimelapse(const TimeDuration&);
    		void setSkipTimelapse(const TimeDuration&);
    		void setTimelapseDuration(const TimeDuration&);

    		void setKeyFrame(bool);
    		void setSimulationSpeed(double);

    		void update(bool, bool);

    		friend inline std::ostream& operator<<(std::ostream& stream, const TimeInfo& time_info)
    		{
    			return stream << "TimeInfo {"
    					<< " Time: " << getStringFromTimeStamp(time_info.m_Time) << ","
    		            << " Timelapse: " << time_info.m_Timelapse.count() << ","
    		            << " Simulation Time: " << getStringFromTimeStamp(time_info.m_SimulationTime) << ","
    		            << " Simulation Timelapse: " << time_info.m_SimulationTimelapse.count() << ","
    		            << " Simulation Speed: " << time_info.m_SimulationSpeed << ","
						<< " Simulation Reference Time: " << getStringFromTimeStamp(time_info.m_SimulationReferenceTime) << ","
						<< " Key Frame Time: " << getStringFromTimeStamp(time_info.m_KeyFrameTime) << ","
    		            << " Is Key Frame: " << time_info.m_KeyFrame << ","
    					<< " Skip Timelapse: " << time_info.m_SkipTimelapse.count()
    		            << " }";
    		}
		};
	}
}

#endif
