#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ControlPanel.h"

namespace LOGOPRISM
{
    namespace  API
    {
        class LogoprismWidget;
        class MainWindow : public QMainWindow
        {
            Q_OBJECT

        public:
            MainWindow(QWidget *parent = 0);
            ~MainWindow();

            LogoprismWidget* getLogoprismWidget();

        private:
            LogoprismWidget* m_Logoprism;
        };
    }
}

#endif // MAINWINDOW_H
