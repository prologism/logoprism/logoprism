#include <QVBoxLayout>
#include "LogoprismWidget.h"
#include "ControlPanel.h"

namespace LOGOPRISM
{
    namespace API
    {

        LogoprismWidget::LogoprismWidget(QWidget *parent) : QWidget(parent)
        {
            m_ControlPanel = new ControlPanel(this);

            m_Scene  = new QGraphicsScene(this);
            m_Scene->setItemIndexMethod(QGraphicsScene::NoIndex);
            m_Scene->setBackgroundBrush(Qt::black);

            m_SceneView = new QGraphicsView(this);
            m_SceneView->setScene(m_Scene);

            QVBoxLayout* layout = new QVBoxLayout;
            layout->addWidget(m_SceneView);
            layout->addWidget(m_ControlPanel);
            setLayout(layout);
        }

        QGraphicsScene* LogoprismWidget::getScene()
        {
            return m_Scene;
        }

        ControlPanel* LogoprismWidget::getControlPanel()
        {
            return m_ControlPanel;
        }
    }
}
