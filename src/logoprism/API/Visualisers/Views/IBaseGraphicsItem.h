#ifndef I_BASE_GRAPHICS_ITEM_H
#define I_BASE_GRAPHICS_ITEM_H

namespace LOGOPRISM
{
	namespace API
	{
		class TimeInfo;
		class IBaseGraphicsItem
		{
		public:
            IBaseGraphicsItem(){}
			IBaseGraphicsItem(const IBaseGraphicsItem& other)  = default;
			IBaseGraphicsItem& operator=(const IBaseGraphicsItem& other) = default;
			IBaseGraphicsItem(IBaseGraphicsItem&& other) = default;
			IBaseGraphicsItem& operator=(IBaseGraphicsItem&& other)  = default;
            virtual ~IBaseGraphicsItem() {};

			virtual void calculateInternalState(const TimeInfo& time_info) = 0;
            virtual QPointF getScenePos() const = 0;
            virtual QPointF getPos() const = 0;
            virtual QColor getColor() const = 0;
		};
	}
}
#endif
