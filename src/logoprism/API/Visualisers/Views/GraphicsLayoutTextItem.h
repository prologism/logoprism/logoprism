#ifndef GRAPHICSLAYOUTTEXTITEM_H
#define GRAPHICSLAYOUTTEXTITEM_H

#include <QGraphicsLayoutItem>
#include <QGraphicsTextItem>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"

namespace LOGOPRISM
{
	namespace API
	{

        class GraphicsLayoutTextItem : public QGraphicsLayoutItem, public QGraphicsTextItem, public IBaseGraphicsItem
		{
		public:
            GraphicsLayoutTextItem(const QString& text = QString());

			void calculateInternalState(const TimeInfo& time_info) override;
            QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const override;
            void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
            void setGeometry(const QRectF &r) override;
            QColor getColor() const override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;

            void setColor(const QColor& color);
		};
	}
}

#endif // GRAPHICSLAYOUTTEXTITEM_H
