#include "MainWindow.h"
#include "LogoprismWidget.h"

namespace LOGOPRISM
{
    namespace API
    {

        MainWindow::MainWindow(QWidget *parent)
            : QMainWindow(parent)
        {
            m_Logoprism = new LogoprismWidget(this);
            setWindowTitle("Logoprism");
            setCentralWidget(m_Logoprism);
            setMinimumSize(800, 600);
        }

        MainWindow::~MainWindow()
        {

        }

        LogoprismWidget* MainWindow::getLogoprismWidget()
        {
            return m_Logoprism;
        }
    }
}
