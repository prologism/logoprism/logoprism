#ifndef GRAPHICSLAYOUTRECTITEM_H
#define GRAPHICSLAYOUTRECTITEM_H

#include <QGraphicsLayoutItem>
#include <QGraphicsRectItem>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"

namespace LOGOPRISM
{
	namespace API
	{

        class GraphicsLayoutRectItem : public QGraphicsLayoutItem, public QGraphicsRectItem, public IBaseGraphicsItem
        {
        private:
            QColor m_Color;

        public:
            GraphicsLayoutRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem* parent = nullptr);

            void calculateInternalState(const TimeInfo& time_info) override;
            QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const override;
            void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
            void setGeometry(const QRectF &r) override;
            QColor getColor() const override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;

            void setColor(const QColor& color);
        };
    }
}

#endif // GRAPHICSLAYOUTRECTITEM_H
