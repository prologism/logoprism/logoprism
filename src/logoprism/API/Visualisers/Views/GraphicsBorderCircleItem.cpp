#include <QWidget>
#include <QPainter>

#include "logoprism/API/Visualisers/Views/GraphicsRectItem.h"
#include "logoprism/API/Visualisers/Views/GraphicsBorderCircleItem.h"
#include "logoprism/API/Visualisers/Controllers/TimeInfo.h"

namespace LOGOPRISM
{
	namespace API
	{
        std::function<void(EventBase*, QWidget*)> GraphicsBorderCircleItem::m_DialogGenerator = std::function<void(EventBase*, QWidget*)>();

        GraphicsBorderCircleItem::GraphicsBorderCircleItem(const IBaseGraphicsItem* source, const IBaseGraphicsItem* target, const Timestamp& start_time, const TimeDuration& duration, const QColor& color, const GraphicsRectItem* source_rectangle, const GraphicsRectItem* target_rectangle)
		: QGraphicsEllipseItem()
		, m_Source(source)
		, m_Target(target)
        , m_SourceRectangle(source_rectangle)
        , m_TargetRectangle(target_rectangle)
        , m_SourceAlignment(RectangleAlignment::AlignLeft)
        , m_TargetAlignment(RectangleAlignment::AlignRight)
		, m_StartTime(start_time)
		, m_Duration(duration)
        , m_Size(std::log2(abs(m_Duration.count() / 100.0)))
        , m_Event(nullptr)
		{
			if(m_Duration.count() < 0)
			{
                m_Duration *= -1;
                m_StartTime -= m_Duration;
			}

            setBrush(QBrush(color));
            setPen(QPen(color));
		}

        bool GraphicsBorderCircleItem::isSourcePositionValid() const
		{
            return m_Source->getPos().x() != 0.0 || m_Source->getPos().y() != 0.0;
		}

        bool GraphicsBorderCircleItem::isTargetPositionValid() const
		{
            return m_Target->getPos().x() != 0.0 || m_Target->getPos().y() != 0.0;
		}

        bool GraphicsBorderCircleItem::isSourceRectanglePositionValid() const
        {
            return m_SourceRectangle == nullptr || m_SourceRectangle->getPos().x() != 0.0 || m_SourceRectangle->getPos().y() != 0.0;
        }

        bool GraphicsBorderCircleItem::isTargetRectanglePositionValid() const
        {
            return m_TargetRectangle == nullptr || m_TargetRectangle->getPos().x() != 0.0 || m_TargetRectangle->getPos().y() != 0.0;
        }

        void GraphicsBorderCircleItem::setDialogGenerator(std::function<void(EventBase*, QWidget*)> generator)
        {
            GraphicsBorderCircleItem::m_DialogGenerator = generator;
        }

        void GraphicsBorderCircleItem::setRectangleAlignments(RectangleAlignment source, RectangleAlignment target)
        {
            m_SourceAlignment = source;
            m_TargetAlignment = target;
        }

        void GraphicsBorderCircleItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
		{
			Q_UNUSED(event);
            if(m_DialogGenerator && m_Event)
            {
                QWidget* dialog_box = new QWidget();
                dialog_box->setAttribute(Qt::WA_DeleteOnClose);
                m_DialogGenerator(m_Event, dialog_box);
                dialog_box->show();
            }
        }

        void GraphicsBorderCircleItem::calculateInternalState(const TimeInfo& time_info)
		{
            if(isSourcePositionValid() && isTargetPositionValid() && isSourceRectanglePositionValid() && isTargetRectanglePositionValid())
            {
                QPointF source_position;
                QPointF target_position;

                if(m_SourceRectangle != nullptr)
                {
                    if(m_SourceAlignment == RectangleAlignment::AlignLeft)  source_position = QPointF(m_SourceRectangle->scenePos().x() - m_Size / 2, m_Source->getScenePos().y());
                    else source_position = QPointF(m_SourceRectangle->scenePos().x() + m_SourceRectangle->rect().width() - m_Size / 2, m_Source->getScenePos().y());
                }
                else
                {
                    source_position = m_Source->getScenePos();
                }

                if(m_TargetRectangle != nullptr)
                {
                    if(m_TargetAlignment == RectangleAlignment::AlignLeft) target_position = QPointF(m_TargetRectangle->scenePos().x() - m_Size / 2, m_Target->getScenePos().y());
                    else target_position = QPointF(m_TargetRectangle->scenePos().x() + m_TargetRectangle->rect().width() - m_Size / 2, m_Target->getScenePos().y());
                }
                else
                {
                    target_position = m_Target->getScenePos();
                }

				double total_distance = QLineF(source_position, target_position).length();
				double speed = total_distance / m_Duration.count();

				TimeDuration elapsed_time = time_info.getSimulationTime() - m_StartTime;
				double travelled_distance = speed * elapsed_time.count();

				if(travelled_distance >= 0.0 && travelled_distance <= total_distance)
				{
					double linepos = travelled_distance/total_distance;
					setPos(source_position + ((target_position - source_position) * linepos));
					setRect(0, 0, m_Size, m_Size);
					show();
				}
				else
				{
					hide();
				}
			}
			else
			{
				hide();
			}
		}

        void GraphicsBorderCircleItem::setEvent(EventBase* ev)
        {
            m_Event = ev;
        }

        QPointF GraphicsBorderCircleItem::getScenePos() const
        {
            return QGraphicsEllipseItem::scenePos();
        }

        QPointF GraphicsBorderCircleItem::getPos() const
        {
            return QGraphicsEllipseItem::pos();
        }

        QColor GraphicsBorderCircleItem::getColor() const
        {
            return m_Source->getColor();
        }
	}
}
