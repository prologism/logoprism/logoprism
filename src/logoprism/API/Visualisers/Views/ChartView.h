#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include <QtWidgets/QRubberBand>
#include <QtCharts>

namespace LOGOPRISM
{
    namespace API
    {
        class ChartView : public QChartView
        {
        public:
            ChartView(QChart *chart, QWidget *parent = nullptr);

        protected:
            void mousePressEvent(QMouseEvent *event);
            void mouseMoveEvent(QMouseEvent *event);
            void mouseReleaseEvent(QMouseEvent *event);
            void keyPressEvent(QKeyEvent *event);
        };
    }
}

#endif
