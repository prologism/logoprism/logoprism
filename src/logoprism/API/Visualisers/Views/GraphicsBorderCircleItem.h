#ifndef GRAPHICSBORDERCIRCLEITEM_H
#define GRAPHICSBORDERCIRCLEITEM_H

#include <QGraphicsEllipseItem>

#include <functional>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"
#include "logoprism/API/Utils/Timestamp.h"

namespace LOGOPRISM
{
    namespace API
    {
        class EventBase;
        class GraphicsRectItem;
        class GraphicsBorderCircleItem : public QGraphicsEllipseItem, public IBaseGraphicsItem
        {

        public:
            enum RectangleAlignment
            {
                AlignLeft,
                AlignRight
            };

        private:
            static std::function<void(EventBase*, QWidget*)> m_DialogGenerator;

            const IBaseGraphicsItem* m_Source;
            const IBaseGraphicsItem* m_Target;

            const GraphicsRectItem* m_SourceRectangle;
            const GraphicsRectItem* m_TargetRectangle;

            RectangleAlignment m_SourceAlignment;
            RectangleAlignment m_TargetAlignment;

            Timestamp m_StartTime;
            TimeDuration m_Duration;
            double m_Size;

            EventBase* m_Event;

        public:
            GraphicsBorderCircleItem(const IBaseGraphicsItem* source, const IBaseGraphicsItem* target, const Timestamp& start_time, const TimeDuration& duration, const QColor& color, const GraphicsRectItem* source_rectangle = nullptr, const GraphicsRectItem* target_rectangle = nullptr);
            bool isSourcePositionValid() const;
            bool isTargetPositionValid() const;
            bool isSourceRectanglePositionValid() const;
            bool isTargetRectanglePositionValid() const;
            void setRectangleAlignments(RectangleAlignment source, RectangleAlignment target);
            void setEvent(EventBase* ev);
            static void setDialogGenerator(std::function<void(EventBase*, QWidget*)> generator);

            void calculateInternalState(const TimeInfo& time_info) override;
            void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;
            QColor getColor() const override;
        };
    }
}

#endif // GRAPHICSCIRCLEITEM_H*/
