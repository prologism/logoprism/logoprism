#include <QWidget>
#include <QPainter>

#include "logoprism/API/Visualisers/Views/GraphicsRectItem.h"
#include "logoprism/API/Visualisers/Controllers/TimeInfo.h"

namespace LOGOPRISM
{
	namespace API
	{
      GraphicsRectItem::GraphicsRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent)
        : QGraphicsRectItem(x, y, w, h, parent)
        , IBaseGraphicsItem()
      {}

        void GraphicsRectItem::calculateInternalState(const TimeInfo&)
		{

		}

        QPointF GraphicsRectItem::getScenePos() const
        {
            return QGraphicsRectItem::scenePos();
        }

        QPointF GraphicsRectItem::getPos() const
        {
            return QGraphicsRectItem::pos();
        }

        QColor GraphicsRectItem::getColor() const
        {
            return QGraphicsRectItem::pen().color();
        }
	}
}
