#include "GraphicsLayoutTextItem.h"

#include <QPainter>

namespace LOGOPRISM
{
	namespace API
	{
        GraphicsLayoutTextItem::GraphicsLayoutTextItem(const QString& text)
        : QGraphicsLayoutItem()
        , QGraphicsTextItem(text)
        , IBaseGraphicsItem()
        {
            setDefaultTextColor(QColor(50 + (std::rand() % (255 - 50 + 1)), 50 + (std::rand() % (255 - 50 + 1)), 50 + (std::rand() % (255 - 50 + 1))));
            setFont(QFont("Times", 7, QFont::Normal));
            setGraphicsItem(this);

        }

        void GraphicsLayoutTextItem::calculateInternalState(const TimeInfo&)
		{

		}

        void GraphicsLayoutTextItem::setColor(const QColor& color)
        {
            setDefaultTextColor(color);
        }

        QColor GraphicsLayoutTextItem::getColor() const
        {
            return defaultTextColor();
        }

        QPointF GraphicsLayoutTextItem::getScenePos() const
        {
            return QGraphicsTextItem::scenePos();
        }

        QPointF GraphicsLayoutTextItem::getPos() const
        {
            return QGraphicsTextItem::pos();
        }

        QSizeF GraphicsLayoutTextItem::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
        {
            Q_UNUSED(constraint)
            Q_UNUSED(which)
            return QSizeF(QGraphicsTextItem::boundingRect().width(), QGraphicsTextItem::boundingRect().height() - 15);
        }

        void GraphicsLayoutTextItem::setGeometry(const QRectF& rect)
        {
            prepareGeometryChange();
            QGraphicsLayoutItem::setGeometry(rect);
            setPos(rect.topLeft());
        }

        void GraphicsLayoutTextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
        {
            QGraphicsTextItem::paint(painter, option, widget);
        }
	}
}
