#include "GraphicsLayoutRectItem.h"

#include <QPainter>

namespace LOGOPRISM
{
	namespace API
	{
        GraphicsLayoutRectItem::GraphicsLayoutRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent)
        : QGraphicsLayoutItem()
        , QGraphicsRectItem(x, y, w, h, parent)
        , IBaseGraphicsItem()
        {
            setGraphicsItem(this);
            setBrush(Qt::NoBrush);
            setPen(QPen(Qt::green, 1));
            setColor(Qt::green);
        }

        void GraphicsLayoutRectItem::calculateInternalState(const TimeInfo&)
		{

		}

        void GraphicsLayoutRectItem::setColor(const QColor& color)
        {
            m_Color = color;
        }

        QColor GraphicsLayoutRectItem::getColor() const
        {
            return m_Color;
        }

        QPointF GraphicsLayoutRectItem::getScenePos() const
        {
            return QGraphicsRectItem::scenePos();
        }

        QPointF GraphicsLayoutRectItem::getPos() const
        {
            return QGraphicsRectItem::pos();
        }

        QSizeF GraphicsLayoutRectItem::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
        {
            Q_UNUSED(constraint)
            Q_UNUSED(which)
            return QSizeF(QGraphicsRectItem::boundingRect().width(), QGraphicsRectItem::boundingRect().height());
        }

        void GraphicsLayoutRectItem::setGeometry(const QRectF& rect)
        {
            prepareGeometryChange();
            QGraphicsLayoutItem::setGeometry(rect);
            setPos(rect.topLeft());
        }

        void GraphicsLayoutRectItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
        {
            QGraphicsRectItem::paint(painter, option, widget);
        }
	}
}
