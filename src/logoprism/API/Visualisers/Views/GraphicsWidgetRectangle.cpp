#include "GraphicsWidgetRectangle.h"

#include <QPainter>

namespace LOGOPRISM
{
    namespace API
    {
        GraphicsWidgetRectangle::GraphicsWidgetRectangle(const QColor& color, QGraphicsItem *parent, Qt::WindowFlags wFlags)
            : QGraphicsWidget (parent, wFlags)
            , IBaseGraphicsItem ()
            , m_Color(color)
        {}

        void GraphicsWidgetRectangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
        {
            QGraphicsWidget::paint(painter, option, widget);
            painter->setPen(QPen(m_Color, 1));
            painter->drawRect(QGraphicsWidget::boundingRect());
        }

        QColor GraphicsWidgetRectangle::getColor() const
        {
            return m_Color;
        }

        QPointF GraphicsWidgetRectangle::getScenePos() const
        {
            return QGraphicsWidget::scenePos();
        }

        QPointF GraphicsWidgetRectangle::getPos() const
        {
            return QGraphicsWidget::pos();
        }

        void GraphicsWidgetRectangle::calculateInternalState(const TimeInfo&)
        {

        }
    }
}
