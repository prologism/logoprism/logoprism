#include "ChartView.h"
#include <QtCharts>
#include <QtGui/QMouseEvent>

namespace LOGOPRISM
{
    namespace API
    {
        ChartView::ChartView(QChart *chart, QWidget *parent) : QChartView(chart, parent)
        {
            setRubberBand(QChartView::RectangleRubberBand);
        }

        void ChartView::mousePressEvent(QMouseEvent *event)
        {
            QChartView::mousePressEvent(event);
        }

        void ChartView::mouseMoveEvent(QMouseEvent *event)
        {
            QChartView::mouseMoveEvent(event);
        }

        void ChartView::mouseReleaseEvent(QMouseEvent *event)
        {
            QChartView::mouseReleaseEvent(event);
        }

        void ChartView::keyPressEvent(QKeyEvent *event)
        {
            switch (event->key())
            {
                case Qt::Key_Plus:
                    chart()->zoomIn();
                break;
                case Qt::Key_Minus:
                    chart()->zoomOut();
                break;
                case Qt::Key_Left:
                    chart()->scroll(-10, 0);
                break;
                case Qt::Key_Right:
                    chart()->scroll(10, 0);
                break;
                case Qt::Key_Up:
                    chart()->scroll(0, 10);
                break;
                case Qt::Key_Down:
                    chart()->scroll(0, -10);
                break;
                default:
                    QGraphicsView::keyPressEvent(event);
                break;
            }
        }
    }
}
