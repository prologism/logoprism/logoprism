#ifndef GRAPHICSTEXTITEM_H
#define GRAPHICSTEXTITEM_H

#include <QGraphicsWidget>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"

namespace LOGOPRISM
{
	namespace API
	{
        class GraphicsTextItem : public QGraphicsWidget, public IBaseGraphicsItem
		{
        private:
			QString m_Name;
			QColor m_Color;

		public:
			GraphicsTextItem(QGraphicsItem * parent = nullptr, const QString& name = QString());
			void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
			void calculateInternalState(const TimeInfo& time_info) override;
            QColor getColor() const override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;

            void setColor(const QColor& color);
		};
	}
}

#endif // GRAPHICSTEXTITEM_H
