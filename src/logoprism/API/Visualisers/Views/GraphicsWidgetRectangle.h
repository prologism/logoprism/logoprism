#ifndef GRAPHICSWIDGETRECTANGLE_H
#define GRAPHICSWIDGETRECTANGLE_H

#include <QGraphicsWidget>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"

namespace LOGOPRISM
{
    namespace API
    {
        class GraphicsWidgetRectangle : public QGraphicsWidget, public IBaseGraphicsItem
        {
        private:
            QColor m_Color;

        public:
            GraphicsWidgetRectangle(const QColor& color, QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = Qt::WindowFlags());
            void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
            void calculateInternalState(const TimeInfo& time_info) override;
            QColor getColor() const override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;
        };
    }
}

#endif // GRAPHICSWIDGETRECTANGLE_H
