#ifndef GRAPHICSRECTITEM_H
#define GRAPHICSRECTITEM_H

#include <QGraphicsRectItem>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"

namespace LOGOPRISM
{
    namespace API
    {
        class GraphicsRectItem : public QGraphicsRectItem, public IBaseGraphicsItem
        {
        public:
            GraphicsRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent = nullptr);

            void calculateInternalState(const TimeInfo& time_info) override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;
            QColor getColor() const override;
        };
    }
}

#endif // GRAPHICSRECTITEM_H*/
