#ifndef GRAPHICSCIRCLEITEM_H
#define GRAPHICSCIRCLEITEM_H

#include <QGraphicsEllipseItem>

#include <functional>

#include "logoprism/API/Visualisers/Views/IBaseGraphicsItem.h"
#include "logoprism/API/Utils/Timestamp.h"

namespace LOGOPRISM
{
	namespace API
	{       
        class EventBase;
        class GraphicsCircleItem : public QGraphicsEllipseItem, public IBaseGraphicsItem
		{
        private:
            static std::function<void(EventBase*, QWidget*)> m_DialogGenerator;

            const IBaseGraphicsItem* m_Source;
            const IBaseGraphicsItem* m_Target;

			Timestamp m_StartTime;
			TimeDuration m_Duration;
            double m_Size;

            EventBase* m_Event;

		public:
            GraphicsCircleItem(const IBaseGraphicsItem* source, const IBaseGraphicsItem* target, const Timestamp& start_time, const TimeDuration& duration, const QColor& color);
            bool isSourcePositionValid() const;
			bool isTargetPositionValid() const;
            void setEvent(EventBase* ev);
            static void setDialogGenerator(std::function<void(EventBase*, QWidget*)> generator);

            void calculateInternalState(const TimeInfo& time_info) override;
            void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
            QPointF getScenePos() const override;
            QPointF getPos() const override;
            QColor getColor() const override;
        };
	}
}

#endif // GRAPHICSCIRCLEITEM_H*/
