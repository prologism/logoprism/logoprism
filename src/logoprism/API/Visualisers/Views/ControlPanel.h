#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QAbstractButton>
#include <QMediaPlayer>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QButtonGroup>
#include <QLCDNumber>
#include <QSlider>

#include "logoprism/API/Utils/Timestamp.h"

namespace  LOGOPRISM
{
    namespace API
    {
        class ControlPanel : public QWidget
        {
            Q_OBJECT
        public:
            explicit ControlPanel(QWidget *parent = nullptr);

            QMediaPlayer::State getState() const;
            void setState(QMediaPlayer::State state);

            struct ControlEntries
            {
                std::atomic<bool> m_DataAvailable;
                bool m_PlayButtonPressed;
                bool m_StopButtonPressed;
                bool m_NextButtonPressed;
                bool m_PreviousButtonPressed;
                bool m_GlobalStatisticsButtonPressed;
                bool m_SnapStatisticsButtonPressed;
                bool m_SpeedChanged;
                bool m_KeyFrameDurationChanged;
                bool m_StepModeChanged;
                bool m_DirectionChanged;
                bool m_SliderChanged;
                double m_Speed;
                uint64_t m_KeyFrameDuration;
                int m_SliderValue;

                ControlEntries()
                : m_DataAvailable(false), m_PlayButtonPressed(false), m_StopButtonPressed(false), m_NextButtonPressed(false), m_PreviousButtonPressed(false), m_GlobalStatisticsButtonPressed(false), m_SnapStatisticsButtonPressed(false)
                , m_SpeedChanged(false), m_KeyFrameDurationChanged(false), m_StepModeChanged(false), m_DirectionChanged(false), m_SliderChanged(false), m_Speed(0.0), m_KeyFrameDuration(0), m_SliderValue(0)
                {}

                void clearAll()
                {
                    m_PlayButtonPressed = false;
                    m_StopButtonPressed = false;
                    m_NextButtonPressed = false;
                    m_PreviousButtonPressed = false;
                    m_GlobalStatisticsButtonPressed = false;
                    m_SnapStatisticsButtonPressed = false;
                    m_SpeedChanged = false;
                    m_KeyFrameDurationChanged = false;
                    m_StepModeChanged = false;
                    m_DirectionChanged = false;
                    m_SliderChanged = false;
                    m_DataAvailable.store(false, std::memory_order_relaxed);
                }

                void clear()
                {
                    m_DataAvailable.store(false, std::memory_order_relaxed);
                }
            };

        private:
            QMediaPlayer::State m_PlayerState = QMediaPlayer::StoppedState;
            ControlEntries m_ControlEntries;

            API::Timestamp m_SimulationTimeStart = API::Timestamp();
            API::Timestamp m_SimulationTimeEnd = API::Timestamp();
            QAbstractButton* m_PlayButton = nullptr;
            QAbstractButton* m_StopButton = nullptr;
            QAbstractButton* m_NextButton = nullptr;
            QAbstractButton* m_PreviousButton = nullptr;
            QDoubleSpinBox* m_SpeedBox = nullptr;
            QSpinBox* m_KeyFrameBox  = nullptr;
            QLabel* m_SpeedLabel = nullptr;
            QLabel* m_KeyFrameLabel = nullptr;
            QLabel* m_KeyFrameUnityLabel = nullptr;
            QLabel* m_StepModeLabel = nullptr;
            QPushButton* m_GlobalStatisticsButton = nullptr;
            QPushButton* m_SnapStatisticsButton = nullptr;
            QRadioButton* m_StepModeTime = nullptr;
            QRadioButton* m_StepModeEvent = nullptr;
            QButtonGroup* m_StepModeGroup = nullptr;
            QLabel* m_DirectionLabel = nullptr;
            QRadioButton* m_DirectionForward = nullptr;
            QRadioButton* m_DirectionBackward = nullptr;
            QButtonGroup* m_DirectionGroup = nullptr;
            QLCDNumber* m_SimulationTime = nullptr;
            QSlider* m_TimerSlider = nullptr;
            QLabel*  m_StartTimeLabel = nullptr;
            QLabel*  m_EndTimeLabel = nullptr;

        public:
            ControlEntries& getControlEntries();
            void setSimulationTime(const Timestamp& sim_time);
            void setSimulationTimeRange(const Timestamp& start_time, const Timestamp& end_time);

        public slots:
            void showSnapStatistics();
            void showGlobalStatistics();
            void playOrPause();
            void stop();
            void next();
            void previous();
            void speedChanged(double speed);
            void keyFrameDurationChanged(int duration);
            void stepModeSwitched(bool checked);
            void directionSwitched(bool checked);
            void sliderChanged(int value);
        };
    }
}

#endif // CONTROLPANEL_H
