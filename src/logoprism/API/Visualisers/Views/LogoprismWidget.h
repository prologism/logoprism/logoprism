#ifndef LOGOPRISMWIDGET_H
#define LOGOPRISMWIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>

namespace LOGOPRISM
{
    namespace API
    {
        class ControlPanel;
        class LogoprismWidget : public QWidget
        {
            Q_OBJECT
        public:
            explicit LogoprismWidget(QWidget *parent = nullptr);
            QGraphicsScene* getScene();
            ControlPanel* getControlPanel();

        private:
            ControlPanel* m_ControlPanel;
            QGraphicsScene* m_Scene;
            QGraphicsView* m_SceneView;
        };
    }
}

#endif // LOGOPRISMWIDGET_H
