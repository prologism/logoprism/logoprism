#include "GraphicsTextItem.h"

#include <QPainter>

namespace LOGOPRISM
{
	namespace API
	{
		GraphicsTextItem::GraphicsTextItem(QGraphicsItem* parent, const QString& name)
		: QGraphicsWidget(parent)
		, m_Name(name)
        , m_Color(50 + (std::rand() % (255 - 50 + 1)), 50 + (std::rand() % (255 - 50 + 1)), 50 + (std::rand() % (255 - 50 + 1)))
		{
            setMaximumHeight(12);
            setMinimumHeight(12);
		}

		void GraphicsTextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
		{
			Q_UNUSED(option)
    		Q_UNUSED(widget)

			QFont font("Times", 10);
			painter->setFont(font);
			painter->setPen(m_Color);
			painter->drawText(0, 0, m_Name);
		}

		void GraphicsTextItem::calculateInternalState(const TimeInfo&)
		{

		}

		void GraphicsTextItem::setColor(const QColor& color)
		{
			m_Color = color;
		}

		QColor GraphicsTextItem::getColor() const
		{
			return m_Color;
		}

        QPointF GraphicsTextItem::getScenePos() const
        {
            return QGraphicsWidget::scenePos();
        }

        QPointF GraphicsTextItem::getPos() const
        {
            return QGraphicsWidget::pos();
        }
	}
}
