#include <QWidget>
#include <QPainter>

#include "logoprism/API/Visualisers/Views/GraphicsCircleItem.h"
#include "logoprism/API/Visualisers/Controllers/TimeInfo.h"

namespace LOGOPRISM
{
	namespace API
	{
        std::function<void(EventBase*, QWidget*)> GraphicsCircleItem::m_DialogGenerator = std::function<void(EventBase*, QWidget*)>();

        GraphicsCircleItem::GraphicsCircleItem(const IBaseGraphicsItem* source, const IBaseGraphicsItem* target, const Timestamp& start_time, const TimeDuration& duration, const QColor& color)
		: QGraphicsEllipseItem()
		, m_Source(source)
		, m_Target(target)
		, m_StartTime(start_time)
		, m_Duration(duration)
        , m_Size(std::log2(abs(m_Duration.count() / 100.0)))
        , m_Event(nullptr)
		{
			if(m_Duration.count() < 0)
			{
                m_Duration *= -1;
                m_StartTime -= m_Duration;
			}

            setBrush(QBrush(color));
            setPen(QPen(color));
		}

		bool GraphicsCircleItem::isSourcePositionValid() const
		{
            return m_Source->getPos().x() != 0.0 || m_Source->getPos().y() != 0.0;
		}

		bool GraphicsCircleItem::isTargetPositionValid() const
		{
            return m_Target->getPos().x() != 0.0 || m_Target->getPos().y() != 0.0;
		}

        void GraphicsCircleItem::setDialogGenerator(std::function<void(EventBase*, QWidget*)> generator)
        {
            GraphicsCircleItem::m_DialogGenerator = generator;
        }

		void GraphicsCircleItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
		{
			Q_UNUSED(event);
            if(m_DialogGenerator && m_Event)
            {
                QWidget* dialog_box = new QWidget();
                dialog_box->setAttribute(Qt::WA_DeleteOnClose);
                m_DialogGenerator(m_Event, dialog_box);
                dialog_box->show();
            }
        }

		void GraphicsCircleItem::calculateInternalState(const TimeInfo& time_info)
		{
			if(isSourcePositionValid() && isTargetPositionValid())
			{
                QPointF source_position = m_Source->getScenePos();
                QPointF target_position = m_Target->getScenePos();
				double total_distance = QLineF(source_position, target_position).length();
				double speed = total_distance / m_Duration.count();

				TimeDuration elapsed_time = time_info.getSimulationTime() - m_StartTime;
				double travelled_distance = speed * elapsed_time.count();

				if(travelled_distance >= 0.0 && travelled_distance <= total_distance)
				{
					double linepos = travelled_distance/total_distance;
					setPos(source_position + ((target_position - source_position) * linepos));
					setRect(0, 0, m_Size, m_Size);
					show();
				}
				else
				{
					hide();
				}
			}
			else
			{
				hide();
			}
		}

        void GraphicsCircleItem::setEvent(EventBase* ev)
        {
            m_Event = ev;
        }

        QPointF GraphicsCircleItem::getScenePos() const
        {
            return QGraphicsEllipseItem::scenePos();
        }

        QPointF GraphicsCircleItem::getPos() const
        {
            return QGraphicsEllipseItem::pos();
        }

        QColor GraphicsCircleItem::getColor() const
        {
            return m_Source->getColor();
        }
	}
}
