#include <QToolButton>
#include <QHBoxLayout>
#include <QStyle>

#include <iostream>
#include "logoprism/API/Visualisers/Views/ControlPanel.h"

namespace LOGOPRISM
{
    namespace API
    {
        ControlPanel::ControlPanel(QWidget *parent) : QWidget(parent)
        {
            m_PlayButton = new QToolButton(this);
            m_PlayButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
            connect(m_PlayButton, &QAbstractButton::clicked, this, &ControlPanel::playOrPause);

            m_StopButton = new QToolButton(this);
            m_StopButton->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
            m_StopButton->setEnabled(false);
            connect(m_StopButton, &QAbstractButton::clicked, this, &ControlPanel::stop);

            m_NextButton = new QToolButton(this);
            m_NextButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
            connect(m_NextButton, &QAbstractButton::clicked, this, &ControlPanel::next);

            m_PreviousButton = new QToolButton(this);
            m_PreviousButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
            connect(m_PreviousButton, &QAbstractButton::clicked, this, &ControlPanel::previous);

            m_SpeedLabel = new QLabel(tr("Simulation Speed(x):"), this);
            m_SpeedBox = new QDoubleSpinBox(this);
            m_SpeedBox->setRange(0.0, 10.0);
            m_SpeedBox->setDecimals(6);
            m_SpeedBox->setAccelerated(true);
            m_SpeedBox->setValue(1.0);
            m_SpeedBox->setSingleStep(0.001);
            connect(m_SpeedBox, SIGNAL(valueChanged(double)), this, SLOT(speedChanged(double)));

            m_KeyFrameLabel = new QLabel(tr("Key Frame Duration:"), this);
            m_KeyFrameUnityLabel = new QLabel(tr("ms"), this);
            m_KeyFrameBox = new QSpinBox(this);
            m_KeyFrameBox->setRange(1, 5000);
            m_KeyFrameBox->setAccelerated(true);
            m_KeyFrameBox->setValue(1);
            connect(m_KeyFrameBox, SIGNAL(valueChanged(int)), this, SLOT(keyFrameDurationChanged(int)));

            m_StepModeLabel = new QLabel(tr("Step Mode:"), this);
            m_StepModeTime = new QRadioButton(tr("Time"), this);
            m_StepModeTime->setChecked(true);
            m_StepModeTime->setObjectName(tr("Step Time"));
            connect(m_StepModeTime, SIGNAL(toggled(bool)), this, SLOT(stepModeSwitched(bool)));

            m_StepModeEvent = new QRadioButton(tr("Event"), this);
            m_StepModeEvent->setObjectName(tr("Step Event"));
            connect(m_StepModeEvent, SIGNAL(toggled(bool)), this, SLOT(stepModeSwitched(bool)));

            m_StepModeGroup = new QButtonGroup(this);
            m_StepModeGroup->addButton(m_StepModeTime);
            m_StepModeGroup->addButton(m_StepModeEvent);

            m_DirectionLabel = new QLabel(tr("Direction:"), this);
            m_DirectionForward = new QRadioButton(tr("Forward"), this);
            m_DirectionForward->setChecked(true);
            connect(m_DirectionForward, SIGNAL(toggled(bool)), this, SLOT(directionSwitched(bool)));

            m_DirectionBackward = new QRadioButton(tr("Backward"), this);
            connect(m_DirectionBackward, SIGNAL(toggled(bool)), this, SLOT(directionSwitched(bool)));

            m_DirectionGroup = new QButtonGroup(this);
            m_DirectionGroup->addButton(m_DirectionForward);
            m_DirectionGroup->addButton(m_DirectionBackward);

            m_SimulationTime = new QLCDNumber(this);
            m_SimulationTime->setSegmentStyle(QLCDNumber::SegmentStyle::Filled);
            m_SimulationTime->setPalette(Qt::black);
            m_SimulationTime->setDigitCount(26);

            m_GlobalStatisticsButton = new QPushButton(tr("Global Statistics"), this);
            connect(m_GlobalStatisticsButton, &QAbstractButton::clicked, this, &ControlPanel::showGlobalStatistics);

            m_SnapStatisticsButton = new QPushButton(tr("Snap Statistics"), this);
            connect(m_SnapStatisticsButton, &QAbstractButton::clicked, this, &ControlPanel::showSnapStatistics);

            QBoxLayout* layout_horizontal = new QHBoxLayout;
            layout_horizontal->addWidget(m_StopButton);
            layout_horizontal->addWidget(m_PreviousButton);
            layout_horizontal->addWidget(m_PlayButton);
            layout_horizontal->addWidget(m_NextButton);
            layout_horizontal->addStretch(1);
            layout_horizontal->addWidget(m_SpeedLabel);
            layout_horizontal->addWidget(m_SpeedBox);
            layout_horizontal->addStretch(1);
            layout_horizontal->addWidget(m_KeyFrameLabel);
            layout_horizontal->addWidget(m_KeyFrameBox);
            layout_horizontal->addWidget(m_KeyFrameUnityLabel);
            layout_horizontal->addStretch(1);
            layout_horizontal->addWidget(m_DirectionLabel);
            layout_horizontal->addWidget(m_DirectionForward);
            layout_horizontal->addWidget(m_DirectionBackward);
            layout_horizontal->addStretch(1);
            layout_horizontal->addWidget(m_StepModeLabel);
            layout_horizontal->addWidget(m_StepModeTime);
            layout_horizontal->addWidget(m_StepModeEvent);
            layout_horizontal->addStretch(1);
            layout_horizontal->addWidget(m_GlobalStatisticsButton);
            layout_horizontal->addWidget(m_SnapStatisticsButton);
            layout_horizontal->addWidget(m_SimulationTime);

            m_TimerSlider = new QSlider(Qt::Horizontal);
            m_TimerSlider->setRange(0, std::numeric_limits<int>::max());
            connect(m_TimerSlider, &QSlider::valueChanged, this,  &ControlPanel::sliderChanged);

            m_StartTimeLabel = new QLabel(tr(""), this);
            m_EndTimeLabel = new QLabel(tr(""), this);

            QHBoxLayout* slider_layout = new QHBoxLayout;
            slider_layout->addWidget(m_StartTimeLabel);
            slider_layout->addWidget(m_TimerSlider);
            slider_layout->addWidget(m_EndTimeLabel);

            QVBoxLayout* layout_vertical = new QVBoxLayout();
            layout_vertical->addLayout(slider_layout);
            layout_vertical->addLayout(layout_horizontal);
            setLayout(layout_vertical);
        }

        void ControlPanel::showGlobalStatistics()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_GlobalStatisticsButtonPressed = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::showSnapStatistics()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_SnapStatisticsButtonPressed = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::playOrPause()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                switch (m_PlayerState)
                {
                    case QMediaPlayer::StoppedState:
                    case QMediaPlayer::PausedState:
                        setState(QMediaPlayer::PlayingState);
                        m_SnapStatisticsButton->setDisabled(true);
                        m_ControlEntries.m_PlayButtonPressed = true;
                        m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
                    break;
                    case QMediaPlayer::PlayingState:
                        setState(QMediaPlayer::PausedState);
                        m_SnapStatisticsButton->setDisabled(false);
                        m_ControlEntries.m_PlayButtonPressed = true;
                        m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
                    break;
                }
            }
        }

        void ControlPanel::stop()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_StopButtonPressed = true;

                setState(QMediaPlayer::PausedState);
                m_SnapStatisticsButton->setDisabled(false);
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::next()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_NextButtonPressed = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::previous()
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_PreviousButtonPressed = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::speedChanged(double speed)
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_SpeedChanged = true;
                m_ControlEntries.m_Speed = speed;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::keyFrameDurationChanged(int duration)
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_KeyFrameDurationChanged = true;
                m_ControlEntries.m_KeyFrameDuration = static_cast<uint64_t>(duration);
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::stepModeSwitched(bool)
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_StepModeChanged = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::directionSwitched(bool)
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_DirectionChanged = true;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::sliderChanged(int value)
        {
            if(false == m_ControlEntries.m_DataAvailable.load(std::memory_order_relaxed))
            {
                m_ControlEntries.clearAll();
                m_ControlEntries.m_SliderChanged = true;
                m_ControlEntries.m_SliderValue = value;
                m_ControlEntries.m_DataAvailable.store(true, std::memory_order_release);
            }
        }

        void ControlPanel::setState(QMediaPlayer::State state)
        {
            if (state != m_PlayerState)
            {
                m_PlayerState = state;

                switch (state)
                {
                    case QMediaPlayer::StoppedState:
                        m_StopButton->setEnabled(false);
                        m_PlayButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
                    break;
                    case QMediaPlayer::PlayingState:
                        m_StopButton->setEnabled(true);
                        m_PlayButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
                    break;
                    case QMediaPlayer::PausedState:
                        m_StopButton->setEnabled(true);
                        m_PlayButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
                    break;
                }
            }
        }

        ControlPanel::ControlEntries& ControlPanel::getControlEntries()
        {
            return m_ControlEntries;
        }

        void ControlPanel::setSimulationTime(const Timestamp& sim_time)
        {
            m_SimulationTime->display(getStringFromTimeStamp(sim_time).c_str());
            if(!m_TimerSlider->isSliderDown())
            {
                m_TimerSlider->blockSignals(true);

                uint64_t offset_ns = sim_time.time_since_epoch().count() - m_SimulationTimeStart.time_since_epoch().count();
                uint64_t difference_ns = m_SimulationTimeEnd.time_since_epoch().count() - m_SimulationTimeStart.time_since_epoch().count();
                double multiplication_factor = static_cast<double>(offset_ns) / difference_ns;
                m_TimerSlider->setValue(multiplication_factor * std::numeric_limits<int>::max());

                m_TimerSlider->blockSignals(false);
            }
        }

        void ControlPanel::setSimulationTimeRange(const Timestamp& start_time, const Timestamp& end_time)
        {
        	static constexpr uint64_t DAY_IN_NS = 1000UL * 1000UL * 1000UL * 60UL * 60UL * 24UL;

        	m_SimulationTimeStart = start_time;
            m_SimulationTimeEnd = end_time;

            if(static_cast<uint64_t>(m_SimulationTimeEnd.time_since_epoch().count() - m_SimulationTimeStart.time_since_epoch().count()) < DAY_IN_NS)
            {
            	m_StartTimeLabel->setText(getStringFromTimeStamp(m_SimulationTimeStart, "%H:%M:%S").c_str());
            	m_EndTimeLabel->setText(getStringFromTimeStamp(m_SimulationTimeEnd, "%H:%M:%S").c_str());
            }
            else
            {
            	m_StartTimeLabel->setText(getStringFromTimeStamp(m_SimulationTimeStart).c_str());
            	m_EndTimeLabel->setText(getStringFromTimeStamp(m_SimulationTimeEnd).c_str());
            }
        }
    }
}
