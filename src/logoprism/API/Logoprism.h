#ifndef LOGOPRISM_H
#define LOGOPRISM_H

#include "logoprism/API/Utils/Config.h"
#include "logoprism/API/Processors/Controllers/EventManager.h"
#include "logoprism/API/Visualisers/Controllers/TimeInfo.h"
#include "logoprism/API/Visualisers/Controllers/ViewManager.h"
#include "logoprism/API/Statistics/StatisticsManager.h"
#include "logoprism/API/Processors/Controllers/IThreadManager.h"
#include "logoprism/API/Processors/Controllers/IEventManager.h"
#include "logoprism/API/Processors/Controllers/SerializeManager.h"

#include <QApplication>
#include <QTimer>

namespace LOGOPRISM
{
	namespace API
	{

		class Logoprism
		{
		public:
			enum StepMode
			{
				STEP_TIME = 0,
				STEP_EVENT = 1
			};

		public:
            Logoprism(Configuration const& config, int argc, char *argv[]);
			~Logoprism();
			void tick();
			void run();
			void init();
			void toggleStepMode();
            void onMouseEvent(ControlPanel::ControlEntries& entry);
			void draw();
            std::string getRawFileName() const;
            void setEventManager(const std::shared_ptr<IEventManagerBase>& manager);
            void setReadProviders(std::vector<std::shared_ptr<IThreadManager>>& providers);
            void setSerializeProviders(std::vector<std::shared_ptr<IThreadManager>>& providers);
            void setSerializeManager(const std::shared_ptr<SerializeManagerBase>& manager);
            void setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager);
            void setEventMaxTimeout(const TimeDuration& timeout);
			ViewManager& getViewManager();

		private:
            const Configuration& m_Configuration;
			bool m_SimulationInProgress;
			bool m_DirectionReversed;
			StepMode m_StepMode;
            TimeDuration m_EventMaxTimeout;

		public:
			TimeInfo m_TimeInfo;
			QApplication m_Application;
			QTimer m_Timer;
			ViewManager m_ViewManager;
            std::vector<std::shared_ptr<IThreadManager>> m_ReadProviders;
            std::vector<std::shared_ptr<IThreadManager>> m_SerializeProviders;
            std::shared_ptr<IEventManagerBase> m_EventManager;
            std::shared_ptr<SerializeManagerBase> m_SerializeManager;
            std::shared_ptr<StatisticsManager> m_StatisticsManager;
		};
	}
}

#endif
