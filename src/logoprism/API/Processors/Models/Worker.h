#ifndef WORKER_H
#define WORKER_H

#include <string>
#include <iostream>
#include <cereal/types/chrono.hpp>
#include "logoprism/API/Utils/Timestamp.h"

namespace LOGOPRISM
{
	namespace API
	{
		class Worker
		{
		private:
			std::string m_Id;
			std::string m_Name;
			Timestamp m_BusyTillTime;

		public:
			Worker();
			Worker(std::string name);
			Worker(const Worker& other) = default;
			Worker& operator=(const Worker& other) = default;
			Worker(Worker&& other) = default;
			Worker& operator=(Worker&& other) = default;
			~Worker() {};

			const std::string& getName() const;
			const std::string& getId() const;
			bool isBusy(Timestamp current_time) const;
			void setBusyTillTime(const Timestamp& time);
			const Timestamp& getBusyTillTime() const;
			void delayBusyTillTime(const TimeDuration& duration);
		};

	}
}

#endif
