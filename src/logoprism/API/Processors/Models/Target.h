#ifndef TARGET_H
#define TARGET_H

#include <string>
#include <iostream>

namespace LOGOPRISM
{
	namespace API
	{
		class Target
		{
		private:
			std::string m_Id;
			std::string m_Name;

		public:
			Target();
			Target(std::string name);
			Target(const Target& other) = default;
			Target& operator=(const Target& other) = default;
			Target(Target&& other) = default;
			Target& operator=(Target&& other) = default;
			~Target() {};

			const std::string& getName() const;
			const std::string& getId() const;
		};

	}
}

#endif
