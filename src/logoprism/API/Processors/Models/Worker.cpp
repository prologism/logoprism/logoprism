#include "Worker.h"

namespace LOGOPRISM
{
	namespace API
	{
		Worker::Worker() {}

		Worker::Worker(std::string name) : m_Id("W" + name), m_Name(name) {}

		bool Worker::isBusy(Timestamp current_time) const
		{
			return current_time < m_BusyTillTime;
		}

		void Worker::setBusyTillTime(const Timestamp& time)
		{
			m_BusyTillTime = time;
		}

		const Timestamp& Worker::getBusyTillTime() const
		{
			return m_BusyTillTime;
		}

		void Worker::delayBusyTillTime(const TimeDuration& duration)
		{
			m_BusyTillTime += duration;
		}

		const std::string& Worker::getName() const
		{
			return m_Name;
		}

		const std::string& Worker::getId() const
		{
			return m_Id;
		}
	}
}
