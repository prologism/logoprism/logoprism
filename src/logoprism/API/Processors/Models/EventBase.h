#ifndef EVENT_BASE_H
#define EVENT_BASE_H

/**
 * @file EventBase.h
 * @brief Base class for Events.
 */

#include "Worker.h"
#include "Source.h"
#include "Target.h"
#include "logoprism/API/Statistics/EventStatistics.h"
#include "logoprism/API/Processors/Controllers/GenericPool.h"

#include <memory>
#include <cereal/types/chrono.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>

namespace LOGOPRISM
{
    namespace API
    {
        class StatisticsManager;
        class GlobalStatistics;

        /**
         *  EventBase class for events.
         *  It is the model of the LOGOPRISM. Client must inherits from this class and extend it.
         */
        class EventBase
        {
        private:
            static uint64_t m_EventNumber;
            static StatisticsManager* m_StatisticsManager;

            Timestamp m_StartTime;
            TimeDuration m_Duration;
            std::shared_ptr<Worker> m_Worker;
            std::shared_ptr<Source> m_Source;
            std::shared_ptr<Target> m_Target;
            bool m_Valid;
            std::string m_Id;

        public:
            EventBase() : m_Valid(false), m_Id("E" + std::to_string(m_EventNumber))
            {
                m_EventNumber++;
            }

            EventBase(const EventBase& other) = default;
            EventBase& operator=(const EventBase& other) = default;
            EventBase(EventBase&& other) = default;
            EventBase& operator=(EventBase&& other) = default;
            virtual ~EventBase() {};

            static void setStatisticsManager(StatisticsManager* stat_manager);

            void setWorker(std::shared_ptr<Worker>&& worker);
            void setSource(std::shared_ptr<Source>&& source);
            void setTarget(std::shared_ptr<Target>&& target);
            void setStartTime(const Timestamp& start_time);
            void setDuration(const TimeDuration& duration);
            void setValid(bool valid);

            const std::shared_ptr<Worker>& getWorker() const;
            const std::shared_ptr<Source>& getSource() const;
            const std::shared_ptr<Target>& getTarget() const;
            const Timestamp& getStartTime()  const;
            const TimeDuration& getDuration()  const;
            bool isValid() const noexcept;
            const std::string& getId() const;
            EventStatistics calculateStatistics(const std::string& global_stat_id) const;

            template<class Archive> void load(Archive & archive)
            {
                archive(m_Id, m_Valid);

                std::string target, source, worker;
                archive(target, source, worker);
                setSource(Pool<API::Source>::getInstance().tryToGetElementByName(source));
                setWorker(Pool<API::Worker>::getInstance().tryToGetElementByName(worker));
                setTarget(Pool<Target>::getInstance().tryToGetElementByName(target));

                archive(m_Duration, m_StartTime);
            }

            template<class Archive> void save(Archive & archive) const
            {
                archive(m_Id, m_Valid, m_Target->getName(), m_Source->getName(), m_Worker->getName(), m_Duration, m_StartTime);
            }
        };
    }
}

#endif
