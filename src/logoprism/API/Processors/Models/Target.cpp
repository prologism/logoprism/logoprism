#include "Target.h"

namespace LOGOPRISM
{
	namespace API
	{
		Target::Target() {}
		Target::Target(std::string name) : m_Id("T" + name), m_Name(name) {}

		const std::string& Target::getName() const
		{
			return m_Name;
		}

		const std::string& Target::getId() const
		{
			return m_Id;
		}
	}
}
