#ifndef SOURCE_H
#define SOURCE_H

#include <string>
#include <iostream>

namespace LOGOPRISM
{
	namespace API
	{
		class Source
		{
		private:
			std::string m_Id;
			std::string m_Name;

		public:
			Source();
			Source(std::string name);
			Source(const Source& other) = default;
			Source& operator=(const Source& other) = default;
			Source(Source&& other) = default;
			Source& operator=(Source&& other) = default;
			~Source() {};

			const std::string& getName() const;
			const std::string& getId() const;
		};

	}
}

#endif
