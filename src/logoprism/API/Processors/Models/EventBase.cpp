#include "EventBase.h"
#include "logoprism/API/Statistics/StatisticsManager.h"

namespace LOGOPRISM
{
	namespace API
	{
		uint64_t EventBase::m_EventNumber = 0;
        StatisticsManager* EventBase::m_StatisticsManager = nullptr;
        void EventBase::setStatisticsManager(StatisticsManager* stat_manager) {m_StatisticsManager = stat_manager; }
    	void EventBase::setWorker(std::shared_ptr<Worker>&& worker) { m_Worker = std::move(worker); }
    	void EventBase::setSource(std::shared_ptr<Source>&& source) { m_Source = std::move(source); }
    	void EventBase::setTarget(std::shared_ptr<Target>&& target) { m_Target = std::move(target); }
    	void EventBase::setStartTime(const Timestamp& start_time) { m_StartTime = start_time; }
    	void EventBase::setDuration(const TimeDuration& duration) { m_Duration = duration; }
    	void EventBase::setValid(bool valid) { m_Valid = valid; }

    	const std::shared_ptr<Worker>& EventBase::getWorker() const { return m_Worker; }
    	const std::shared_ptr<Source>& EventBase::getSource() const { return m_Source; }
    	const std::shared_ptr<Target>& EventBase::getTarget() const { return m_Target; }
    	const Timestamp& EventBase::getStartTime()  const { return m_StartTime; }
    	const TimeDuration& EventBase::getDuration()  const { return m_Duration; }
	    bool EventBase::isValid() const noexcept { return m_Valid; }
	    const std::string& EventBase::getId() const { return m_Id; }

        EventStatistics EventBase::calculateStatistics(const std::string& global_stat_id) const
	    {
            auto glob_stat = m_StatisticsManager->getGlobalStatisticsMap()[global_stat_id];

            EventStatistics event_stat;
            event_stat.calculateDeviationFromAvg(m_Duration, glob_stat.getAvgResponseTime());
            event_stat.calculateDeviationFromMax(m_Duration, glob_stat.getMaxResponseTime());
            event_stat.calculateDeviationFromMin(m_Duration, glob_stat.getMinResponseTime());

            if (m_Duration < glob_stat.getPercentile99_9()) event_stat.setPercentileGroup(EventStatistics::PERCENTILE_99_9);
            else if (m_Duration < glob_stat.getPercentile99()) event_stat.setPercentileGroup(EventStatistics::PERCENTILE_99);
            else if (m_Duration < glob_stat.getPercentile90()) event_stat.setPercentileGroup(EventStatistics::PERCENTILE_90);

            return event_stat;
	    }
	}

}
