#include "Source.h"

namespace LOGOPRISM
{
	namespace API
	{
		Source::Source() {}
		Source::Source(std::string name) : m_Id("S" + name), m_Name(name) {}

		const std::string& Source::getName() const
		{
			return m_Name;
		}

		const std::string& Source::getId() const
		{
			return m_Id;
		}
	}
}
