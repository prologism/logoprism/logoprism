#ifndef I_EVENT_MANAGER_H
#define I_EVENT_MANAGER_H

/**
 * @file IEventManager.h
 * @brief Interface definition for IEventManager.
 */

#include <map>
#include <memory>
#include <deque>

#include "IConsumer.h"
#include "IProviderDriver.h"
#include "logoprism/API/Utils/Timestamp.h"
#include "logoprism/API/Processors/Models/EventBase.h"

namespace LOGOPRISM
{
	namespace API
	{

	/**
		 * Manages events.
		 * It is a base interface for IEventMnager.
		 */
		class IEventManagerBase
		{
		public:
			/**
			 *  Constructor.
			 */
			IEventManagerBase() {};
			IEventManagerBase(const IEventManagerBase& other) = default;
			IEventManagerBase& operator=(const IEventManagerBase& other) = default;
			IEventManagerBase(IEventManagerBase&& other) = default;
			IEventManagerBase& operator=(IEventManagerBase& other) = default;
			virtual ~IEventManagerBase(){}

			/**
			 * Sets ProviderDriver member
			 * It sets the ProviderDriver member of the class.
			 * @param provider_driver A ProviderDriver pointer which is stored in the interface.
			 */
			virtual void setProviderDriver(IProviderDriver* provider_driver) = 0;


            /**
             * Initializes the ProviderDriver member
             * It gets a source streann with which it initializes the dirver.
             * @param A source string which is passed to initialize the provider driver.
             */
             virtual void initializeProviderDriver(const std::string& source) = 0;

			/**
			 *  It sets the read margin.
			 *  It provides the opportunity to the visualization system to set the read margin.
			 *  Read margin will be used to read data in advance.
			 *  @param duration A duration from the current simulation time.
			 */
			virtual void setReadMargin(const TimeDuration& duration) = 0;

            /**
             *  It sets the eviction margin.
             *  It provides the opportunity to the visualization system to set the eviction margin.
             *  Evict margin will be used to evict data.
             *  @param duration A duration from the current simulation time.
             */
            virtual void setEvictMargin(const TimeDuration& duration) = 0;

			/**
			 *  Get the closest next element.
			 *  It gets the closest next element based on the simulation time and the direction.
			 *  @param simulation_time The current simulation time provided by the visualization system.
			 *  @param direction_reversed A Boolean indicating whether the direction is reversed or not.
			 *  @return Timestamp of the next element.
			 */
			virtual Timestamp getNextEventInTime(const Timestamp& simulation_time, bool direction_reversed) = 0;

			/**
			 *  Returns events.
			 *  It returns all the events whose start time is inside the time slice given by the parameter.
			 *  @param simulation_time The current simulation time provided by the visualization system.
			 *  @param interval The interval showed by the visualization system.
			 *  @param direction_reversed A Boolean indicating whether the direction is reversed or not.
			 *  @param output The results returned after computation.
			 */
            virtual bool getComputedEventsFor(const Timestamp& simulation_time, const TimeDuration& interval, bool direction_reversed, std::vector<std::shared_ptr<EventBase>>& output) = 0;

			/**
			 *  Provides the simulation reference time.
			 *  It checks the start time of its first events and forward it to the visualization part.
			 *  It should be called only once by the visualization system at the startup.
			 *  @return A datetime, reference time.
			 */
			virtual Timestamp getSimulationReferenceTime() = 0;

			/**
			 * Supervises its provider driver.
			 * It supervises its provider driver and drives it, if data is needed or not.
             * @param simulation_time The current simulation time provided by the visualization system.
			 * @return A Boolean indicating whether the source is depleted or not.
			 */
            virtual bool superviseProviderDriver(Timestamp simulation_time) = 0;

            /**
             * Modifies the provider driver's offset.
             * It sets the provider driver to the new offset from which it can read the data.
             * @param size_t offset the new offset.
             */
            virtual void setProviderDriverOffset(size_t offset) = 0;

            /**
             * Indicates if the simulation time is changed by the user
             */
            virtual void simulationTimeChanged() = 0;

			/**
			 * Checks if the provider chain is depleted.
			 * Under the EventManager plenty of provider/consumer pairs can be found.
			 * This function retrieves the last state whether there are more elements to come or not.
			 * @return A Boolean indicating whether the chain is depleted or not.
			 */
			virtual bool isProviderChainDepleted() = 0;
		};

		/**
		 * Manages events.
		 * It is a template interface which manages a list of its template parameter TEvents.
		 * It also provides interfacing with the Logoprism visualization system.
		 */
		template <typename TEvent>
		class IEventManager : public IEventManagerBase, public IConsumer<TEvent>
		{
		protected:          
            std::map<Timestamp, std::shared_ptr<TEvent>> m_EventContainer;  /**< An event container to store share pointers of TEvents */
            std::map<Timestamp, Timestamp> m_StartTimesByEndTime;  /**< A map to store start times by end time */
			IProviderDriver* m_ProviderDriver; /**< It contains a IProvider to drive the chain through the IProviderDriver. */

		public:
			/**
			 *  Constructor.
			 */
			IEventManager() : m_ProviderDriver(nullptr) {};
			IEventManager(const IEventManager& other) = default;
			IEventManager& operator=(const IEventManager& other) = default;
			IEventManager(IEventManager&& other) = default;
			IEventManager& operator=(IEventManager& other) = default;
			~IEventManager(){}

			void setProviderDriver(IProviderDriver* provider_driver) override { m_ProviderDriver = provider_driver; }
            virtual bool superviseProviderDriver(Timestamp simulation_time) override = 0;
			virtual Timestamp getSimulationReferenceTime() override = 0;
            virtual bool getComputedEventsFor(const Timestamp& simulation_time, const TimeDuration& interval, bool direction_reversed, std::vector<std::shared_ptr<EventBase>>& output) override = 0;
			virtual void setReadMargin(const TimeDuration& duration) override = 0;
			virtual Timestamp getNextEventInTime(const Timestamp& simulation_time, bool direction_reversed) override = 0;
			virtual bool isProviderChainDepleted() override = 0;
            virtual void setEvictMargin(const TimeDuration& duration) override = 0;
            virtual void setProviderDriverOffset(size_t offset) override = 0;
            virtual void simulationTimeChanged() override = 0;
            virtual void initializeProviderDriver(const std::string& source) override = 0;
		};
	}
}

#endif
