#ifndef I_CONSUMER_H
#define I_CONSUMER_H

/**
 * @file IConsumer.h
 * @brief Interface definition for IConsumer.
 */

#include <deque>
#include <mutex>
#include <condition_variable>
#include <atomic>

namespace LOGOPRISM
{
	namespace API
	{

	/**
	 * Consumer interface to consume data.
	 * It is a template interface to consume any type of data received from a provider.
	 */
	template <typename TDataType>
		class IConsumer
		{
		public:
            IConsumer() : m_SourceStateGood(true), m_QueueSize(1000000) {}
			IConsumer(const IConsumer& other) = delete;
			IConsumer& operator=(const IConsumer& other) = delete;
			IConsumer(IConsumer&& other) = default;
			IConsumer& operator=(IConsumer&& other) = default;
			~IConsumer() {}

		private:
            std::deque<TDataType> m_Queue; /**< Queue to store the received data */
			std::mutex m_Lock; /**< Lock to handle concurrent access to the queue */
			std::condition_variable m_CondVar; /**< Condition variable to notify consumer thread */
			std::atomic<bool> m_SourceStateGood; /**< State variable containing the source state */
            uint64_t m_QueueSize; /**< Queue size to limit the memory usage */

			/**
			 * 	Get the lock member.
			 *  Upon concurrent access it gets the lock member.
			 *  @return The lock member.
			 */
			std::mutex& getLock() { return m_Lock; }

			/**
			 * 	Get the condition variable member.
			 *  Its return value is used for notification upon data arrival.
			 *  @return The condition variable member.
			 */
			std::condition_variable& getCondvar() { return m_CondVar; }

		protected:

			/**
			 * 	Swaps queue contents.
			 *  It swaps the content of the data queue with a local buffer in order to minimize the critical section.
			 *  The function does a timed wait measured in microseconds. If the timer is expired no swap happens.
			 *  @param time_out a timeout measured in microseconds.
             *  @param output a deque containing the swapped elements.
			 */
            void swapQueueContentsTimed(std::chrono::microseconds time_out, std::deque<TDataType>& output)
			{
				std::unique_lock<std::mutex> lock(getLock());
				if(getCondvar().wait_for(lock, time_out, [&]() { return !m_Queue.empty(); }))
				{
 					output.swap(m_Queue);
                    m_Queue.shrink_to_fit();
				}
			}

		public:

			/**
			 * 	Swaps queue contents.
			 *  It swaps the content of the data queue with a local buffer in order to minimize the critical section.
			 *  The function does not block it returns immediately.
             *  @param output a deque containing the swapped elements.
			 *  @return A Boolean indication whether there was something to swap or not.
			 */
            bool swapQueueContentsImmediate(std::deque<TDataType>& output)
			{
				std::unique_lock<std::mutex> lock(getLock());
				if(!m_Queue.empty())
				{
					output.swap(m_Queue);
                    m_Queue.shrink_to_fit();
					return true;
				}

				return false;
			}

			/**
			 * 	Set source state callback.
			 *  It is called  by the provider in order to get information about the source state.
			 *  @param state_good Boolean to indicate if the source state is good or not.
			 */
			void setSourceStateGood(bool state_good)
			{
				m_SourceStateGood.store(state_good);
			}

            /**
             * 	Set queue size.
             *  It is used to set the queue size if one wants to modify the default size.
             *  @param queue_size Integer with the new queue size.
             */
            void setQueueSize(uint64_t queue_size)
            {
                m_QueueSize = queue_size;
            }

            /**
             * 	Checks if the queue is full.
             *  @return A Boolean indicating the queue state.
             */
            bool isQueueFull() const
            {
                return m_QueueSize == m_Queue.size();
            }

			/**
			 * 	Retrieve source state information.
			 *  It returns whether the source state is okay or not.
			 *  @return A Boolean describing the provider state.
			 */
			bool isSourceStateGood() const
			{
				return m_SourceStateGood.load();
			}

			/**
			 * 	Notification callback.
			 *  It is called  by the provider upon data arrival.
             *  @param data a deque containing the retrieved elements.
			 */
            uint64_t onDataAvailable(std::deque<TDataType>& data)
			{
                uint64_t consumed = 0;

				std::unique_lock<std::mutex> lock(getLock());
                for(auto& element : data)
                {
                    if(isQueueFull())
                        break;

                    m_Queue.emplace_back(element);
                    consumed++;
                }

				getCondvar().notify_one();
                return consumed;
			}
		};
	}
}

#endif
