#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

/**
 * @file EventManager.h
 * @brief Class definition for EventManager.
 */


#include "IEventManager.h"
#include <algorithm>

namespace LOGOPRISM
{
	namespace API
	{

		/**
		 * Manages events.
		 * It is a template class which manages a list of its template parameter TEvents.
		 * It also provides interfacing with the Logoprism visualization system.
		 */
		template <typename TEvent>
		class EventManager : public IEventManager<TEvent>
		{
		private:
            TimeDuration m_ReadMargin; /**< ReadMargin for advanced reading */
            TimeDuration m_EvictionMargin; /**< EvictionMargin for evincing expired data */
            bool m_SimulationTimeChanged; /**< Flag to indicate if the simulation time has been changed by the user */

		public:
			/**
			 *  Constructor.
			 */
            EventManager() : IEventManager<TEvent>(), m_ReadMargin(0), m_EvictionMargin(0), m_SimulationTimeChanged(false) {}
            EventManager(const EventManager& other) = default;
            EventManager& operator=(const EventManager& other) = default;
            EventManager(EventManager&& other) = default;
            EventManager& operator=(EventManager& other) = default;
            ~EventManager(){}

            /**
            * Supervises its provider driver.
            * It supervises its provider driver and drives it if data needed or not.
            * @param simulation_time The current simulation time provided by the visualization system.
            * @return A Boolean indicating whether the source is depleted or not.
            */
            bool superviseProviderDriver(Timestamp simulation_time) override;


            /**
             * Modifies the provider driver's offset.
             * It sets the provider driver to the new offset from which it can read the data.
             * @param size_t offset the new offset.
             */
            void setProviderDriverOffset(size_t offset) override;

			/**
			 *  Provides the simulation reference time.
			 *  It checks the start time of its first events and forward it to the visualization part.
			 *  @return A datetime, reference time.
			 */
			Timestamp getSimulationReferenceTime() override;

			/**
			 *  Returns events.
			 *  It returns all the events whose start time is inside the time slice given by the parameter.
			 *  @param simulation_time The current simulation time provided by the visualization system.
			 *  @param interval The interval showed by the visualization system.
			 *  @param direction_reversed A Boolean indicating whether the direction is reversed or not.
			 *  @param output The results returned after computation.
			 */
            virtual bool getComputedEventsFor(const Timestamp& simulation_time, const TimeDuration& interval, bool direction_reversed, std::vector<std::shared_ptr<EventBase>>& output) override;

			/**
			 *  It sets the read margin.
			 *  It provides the opportunity to the visualization system to set the read margin.
			 *  Read margin will be used to read data in advance.
			 *  @param duration A duration from the current simulation time.
			 */
			void setReadMargin(const TimeDuration& duration) override;

            /**
             *  It sets the eviction margin.
             *  It provides the opportunity to the visualization system to set the eviction margin.
             *  Evict margin will be used to evict data.
             *  @param duration A duration from the current simulation time.
             */
            void setEvictMargin(const TimeDuration& duration) override;

			/**
			 *  Get the closest next element.
			 *  It gets the closest next element based on the simulation time and the direction.
			 *  @param simulation_time The current simulation time provided by the visualization system.
			 *  @param direction_reversed A Boolean indicating whether the direction is reversed or not.
			 *  @return Timestamp of the next element
			 */
			virtual Timestamp getNextEventInTime(const Timestamp& simulation_time, bool direction_reversed) override;

			/**
			 * Checks if the provider chain is depleted.
			 * Under the EventManager plenty of provider/consumer pairs can be found.
			 * This function retrieves the last state whether there are more elements to come or not.
			 * @return A Boolean indicating whether the chain is depleted or not.
			 */
            bool isProviderChainDepleted() override;

            /**
             * Evict elements.
             * Evicts elements which are expired based on their ending time.
             * @param time reference timestamp
             * @return the number of evicted element
             */
            uint64_t evictExpiredEvents(const Timestamp& time);

            /**
             * Evict elements.
             * Evicts elements based on their start time.
             * @param time reference timestamp
             * @return the number of evicted element
             */
            uint64_t evictEventsTill(const Timestamp& time);

            /**
             * Indicates if the simulation time is changed by the user
             */
            void simulationTimeChanged() override;

            /**
             * Initializes the ProviderDriver member
             * It gets a source streann with which it initializes the dirver.
             * @param A source string which is passed to initialize the provider driver.
             */
             void initializeProviderDriver(const std::string& source) override;

        };
	}
}

#include "EventManager.hpp"

#endif
