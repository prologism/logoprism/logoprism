#ifndef I_PROVIDER_H
#define I_PROVIDER_H

/**
 * @file IProvider.h
 * @brief Interface definition of IProvider.
 */

#include <vector>
#include "IConsumer.h"
#include <iostream>

namespace LOGOPRISM
{

	namespace API
	{
		/**
		 *  IProvider to represent a provider interface.
		 *  It provides data and forwards it to its consumer.
		 *  The provided data type can be set by the template parameter.
		 */
		template <typename TDataType>
		class IProvider
		{
		private:
			std::shared_ptr<IConsumer<TDataType>> m_Consumer; /**< Consumes the provided data */
            std::deque<TDataType> m_Data; /**< Storage to store the provided data and passed it to the consumer */

		public:

			/**
			 *  Constructor.
			 *  It waits for a consumer shared pointer object to which it will forwards the data.
			 */
			IProvider(std::shared_ptr<IConsumer<TDataType>> consumer) : m_Consumer(consumer) {}
			IProvider(const IProvider& other) = default;
			IProvider& operator=(const IProvider& other) = default;
			IProvider(IProvider&& other) = default;
			IProvider& operator=(IProvider& other) = default;
			~IProvider(){}

		protected:

			/**
			 * 	It provides data.
			 *  It calls the implementation function and forward data to the consumer.
			 */
			void provideData()
			{
                if(m_Data.empty())
                {
                    m_Data = provideDataImp();
                }

                if(!m_Data.empty())
				{
                    auto consumed = notifyConsumer(m_Data);
                    m_Data.erase(m_Data.begin(), m_Data.begin() + consumed);
				}
				else
				{
					notifyConsumer(isDataAvailable());
				}
			}

		private:

			/**
			 * 	Notifies consumer if data is available.
			 *  Each time the data is available it is passed to the Consumer by this function.
             *  @param data A deque containing the retrieved data.
             *  @return An uint64_t containing the number of consumed element
			 */
            uint64_t notifyConsumer(std::deque<TDataType>& data) { return m_Consumer->onDataAvailable(data); }

			/**
			 * 	Notifies consumer about the provider state.
			 *  @param state The source state whether it is ok or not.
			 */
			void notifyConsumer(bool state) { m_Consumer->setSourceStateGood(state); }

			/**
			 * 	The providData implementation.
			 *  The implementer must implement the function and manage the details of data retrieval.
			 *  The data is returned in a form a vector.
             *  @return A deque containing the retrieved data.
			 */
            virtual std::deque<TDataType> provideDataImp() = 0;

			/**
			 *  Checks if the source is depleted or not.
			 *  return SourceState indicating source depletion.
			 */
			virtual bool isDataAvailable() const = 0;
		};
	}
}

#endif
