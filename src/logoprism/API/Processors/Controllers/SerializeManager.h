#ifndef SERIALIZE_MANAGER_H
#define SERIALIZE_MANAGER_H

#include <future>
#include <memory>
#include <fstream>
#include <iostream>
#include <cereal/archives/binary.hpp>

#include "IConsumer.h"
#include "IProviderDriver.h"
#include "logoprism/API/Statistics/StatisticsManager.h"
#include "logoprism/API/Utils/Timestamp.h"
#include <cereal/types/chrono.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

namespace LOGOPRISM
{
    namespace API
    {
        class SerializeManagerBase
        {
        public:
            virtual bool serialize() = 0;
            virtual void flushAndClose() = 0;
            virtual void setProviderDriver(IProviderDriver* provider_driver) = 0;
            virtual void initializeProviderDriver(const std::string& source) = 0;
            virtual void setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager) = 0;
            virtual uint64_t getSerializedObjectCounter() const = 0;
            virtual void setIndexInterval(const TimeDuration& interval) = 0;
            virtual const std::map<Timestamp, size_t>& getEventIndexMap() const = 0;
            virtual std::map<Timestamp, size_t>& getEventIndexMap() = 0;
            virtual void setSerializerStreams(std::ofstream&& event_data, std::ofstream&& event_index, std::ofstream&& stat_data) = 0;
            virtual ~SerializeManagerBase() {};
        };

        template <typename TEvent>
        class SerializeManager : public SerializeManagerBase, public IConsumer<TEvent>
        {
        private:
            std::map<Timestamp, size_t> m_EventIndex;
            TimeDuration m_IndexInterval;
            uint64_t m_SerializedObjectCounter;

        protected:
            IProviderDriver* m_ProviderDriver;
            std::ofstream m_SerializedEventDataFile;
            std::ofstream m_SerializedEventIndexFile;
            std::ofstream m_SerializedStatisticFile;
            cereal::BinaryOutputArchive m_EventDataSerializer;
            cereal::BinaryOutputArchive m_EventIndexSerializer;
            cereal::BinaryOutputArchive m_StatisticSerializer;
            std::shared_ptr<StatisticsManager> m_StatManager;

        public:
            SerializeManager();
            SerializeManager(const SerializeManager& other) = default;
            SerializeManager& operator=(const SerializeManager& other) = default;
            SerializeManager(SerializeManager&& other) = default;
            SerializeManager& operator=(SerializeManager& other) = default;
            ~SerializeManager(){}

            void setProviderDriver(IProviderDriver* provider_driver) override;
            void flushAndClose() override;
            bool serialize() override;
            void setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager) override;
            uint64_t getSerializedObjectCounter() const override;
            void setIndexInterval(const TimeDuration& interval) override;
            void setSerializerStreams(std::ofstream&& event_data, std::ofstream&& event_index, std::ofstream&& stat_data) override;
            std::map<Timestamp, size_t>& getEventIndexMap() override;
            const std::map<Timestamp, size_t>& getEventIndexMap() const override;
            void initializeProviderDriver(const std::string& source) override;
        };
    }
}

#include "SerializeManager.hpp"

#endif
