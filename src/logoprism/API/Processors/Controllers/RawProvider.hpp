#include "RawProvider.h"

namespace LOGOPRISM
{
    namespace API
    {
        template <typename TDataType, typename SourceHandler>
        RawProvider<TDataType, SourceHandler>::RawProvider(std::shared_ptr<IConsumer<TDataType>> consumer, std::shared_ptr<SourceHandler> source_handler)
        : IProvider<TDataType>(consumer)
        , IProviderDriver()
        , IThreadManager()
        , m_SourceHandler(source_handler)
        , m_DataIsOnGoing(true)
        {}

        template <typename TDataType, typename SourceHandler>
        std::deque<TDataType> RawProvider<TDataType, SourceHandler>::provideDataImp()
        {
            return m_SourceHandler->getData();
        }

        template <typename TDataType, typename SourceHandler>
        void RawProvider<TDataType, SourceHandler>::startDataFlow()
        {
            m_DataIsOnGoing.store(true, std::memory_order_seq_cst);
            m_CondVar.notify_one();
        }

        template <typename TDataType, typename SourceHandler>
        void RawProvider<TDataType, SourceHandler>::stopDataFlow()
        {
            m_DataIsOnGoing.store(false, std::memory_order_seq_cst);
        }

        template <typename TDataType, typename SourceHandler>
        bool RawProvider<TDataType, SourceHandler>::isDataAvailable() const
        {
            return m_SourceHandler->isDataAvailable();
        }

        template <typename TDataType, typename SourceHandler>
        bool RawProvider<TDataType, SourceHandler>::isDataFlows()
        {
            return m_DataIsOnGoing.load(std::memory_order_seq_cst);
        }

        template <typename TDataType, typename SourceHandler>
        void RawProvider<TDataType, SourceHandler>::modifyOffset(size_t offset)
        {
            m_SourceHandler->modifyOffset(offset);
        }

        template <typename TDataType, typename SourceHandler>
        bool RawProvider<TDataType, SourceHandler>::start()
        {
            if(isDataAvailable() && !isRunning())
            {
                m_Running.store(true, std::memory_order_seq_cst);
                m_Thread = std::thread(&RawProvider::threadFunction, this);
                return true;
            }
            return false;
        }

        template <typename TDataType, typename SourceHandler>
        void RawProvider<TDataType, SourceHandler>::setProviderSource(const std::string& source)
        {
            m_SourceHandler->setSource(source);
        }

        template <typename TDataType, typename SourceHandler>
        bool RawProvider<TDataType, SourceHandler>::stop()
        {
            if(isRunning())
            {
                m_Running.store(false, std::memory_order_seq_cst);
            }

            if(m_Thread.joinable())
            {
                m_Thread.join();
            }

            return true;
        }

        template <typename TDataType, typename SourceHandler>
        bool RawProvider<TDataType, SourceHandler>::isRunning()
        {
            return m_Running.load(std::memory_order_seq_cst);
        }

        template <typename TDataType, typename SourceHandler>
        void RawProvider<TDataType, SourceHandler>::threadFunction()
        {
            while(isRunning())
            {
                if(isDataFlows())
                {
                    this->provideData();
                }
                else
                {
                    std::this_thread::yield();
                }
            }
        }
    }
}
