#ifndef RAW_PROVIDER_H
#define RAW_PROVIDER_H

/**
 * @file RawProvider.h
 * @brief Concrete RawProvider class definition.
 */

#include "IProvider.h"
#include "IThreadManager.h"
#include "IProviderDriver.h"

namespace LOGOPRISM
{
    namespace API
    {

        /**
         *  Concrete RawProvider implementing a provider and a driver and an IThreadManager.
         *  The class is move only type with default move operators.
         *  Its template parameter is the data type for the provider and a client class which can read a data.
         */
        template <typename TDataType, typename SourceHandler>
        class RawProvider : public IProvider<TDataType>, public IProviderDriver, public IThreadManager
        {
        private:
            std::shared_ptr<SourceHandler> m_SourceHandler; /**< SourceHandler implemented by the user */
            std::atomic<bool> m_DataIsOnGoing; /**< Indicating whether data is flowing or not */
            std::mutex m_Lock; /**< Lock for handling concurrent access when supervising the ProviderDriver */
            std::condition_variable m_CondVar;  /**< Condition variable to wait for signal */

        public:

            /**
             *  Constructor.
             *  It waits for a consumer pointer and a source handler pointer.
             */
            RawProvider(std::shared_ptr<IConsumer<TDataType>> consumer, std::shared_ptr<SourceHandler> source_handler);
            RawProvider(const RawProvider& other) = delete;
            RawProvider& operator=(const RawProvider& other) = delete;
            RawProvider(RawProvider&& other) = default;
            RawProvider& operator=(RawProvider&& other) = default;
            ~RawProvider() {}

            /**
             *  Starts the data flow.
             *  It is inherited from the IProviderDriver interface.
             *  It starts the data flow and signals is to the provider function.
             */
            void startDataFlow() override;

            /**
             *  Stops the data flow.
             *  It is inherited from the IProviderDriver interface.
             *  It stops the data flow so the reading of the source is stopped.
             */
            void stopDataFlow() override;

            /**
             *  Checks if the Data flows.
             *  return A Boolean indicating if the data flows.
             */
            bool isDataFlows() override;

            /**
             *  Modifies the provider offset.
             *  @param offset the new offset for the provider.
             */
            void modifyOffset(size_t offset) override;

            /**
             *  Starts the thread.
             *  Inherited from IThreadInterface. It starts a thread
             *  return A Boolean indication a successful start
             */
            bool start() override;

            /**
             *  Stops the thread.
             *  Stops the thread loop by sending a stop signal.
             *  Inherited from IThreadInterface. It stops a thread.
             *  return A Boolean indication a successful stop
             */
            bool stop() override;

            /**
             *  Checks if thread runs or not.
             *  Inherited from IThreadInterface.
             *  return A Boolean indicating if the thread runs or not
             */
            bool isRunning() override;

        private:

            /**
             * Thread function to be called in a thread loop.
             * It must manage the thread stop and suspend conditions.
             */
            void threadFunction();

            /**
             *  Inherited function from IProvider.
             *  It calls the client getData function which provides the data record by record.
             *  It is the responsibility of the client to handle the resource
             */
            std::deque<TDataType> provideDataImp() override;

            /**
             *  Checks if the source is depleted or not.
             *  return A Boolean indicating source depletion.
             */
            bool isDataAvailable() const override;

            /**
             *  Sets the provider source.
             *  @param source the source of the provider.
             */
            void setProviderSource(const std::string& source) override;
        };
    }
}

#include "RawProvider.hpp"

#endif
