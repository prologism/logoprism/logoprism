#ifndef I_THREAD_MANAGER_H
#define I_THREAD_MANAGER_H

/**
 * @file IThreadManager.h
 * @brief Interface definition of IThreadManager.
 */

#include <thread>
#include <atomic>

namespace LOGOPRISM
{

	namespace API
	{
		/**
		 *  IThreadManager for wrapping a thread.
		 *  It provides a possibility to a manage a thread.
		 *  The class is move only type with default move operators.
		 */
		class IThreadManager
		{
		protected:
			std::atomic<bool> m_Running; /**< Boolean to check if a thread is still running */
			std::thread m_Thread; /**< Thread to instantiate a thread */

		public:
			IThreadManager() : m_Running(false) {}
			IThreadManager(const IThreadManager& other) = delete;
			IThreadManager& operator=(const IThreadManager& other) = delete;
			IThreadManager(IThreadManager&& other) = default;
			IThreadManager& operator=(IThreadManager&& other) = default;

			/**
			 * Destructor.
			 */
			virtual ~IThreadManager() {}

			/**
			 * Checks if thread is running.
			 * It checks if the thread is running or not.
			 * It is a pure virtual function which must be implemented by implementers.
			 * @return A Boolean indicating whether the thread runs or not.
			 */
			virtual bool isRunning() = 0;

			/**
			 * Start the thread.
			 * It starts the thread.
			 * It is a pure virtual function which must be implemented by implementers.
			 * @return A Boolean indicating whether the thread is started or not.
			 */
			virtual bool start() = 0;

			/**
			 * Stop the thread.
			 * It stops the thread.
			 * It is a pure virtual function which must be implemented by implementers.
			 * @return A Boolean indicating whether the thread is stopped or not.
			 */
			virtual bool stop() = 0;
		};
	}
}

#endif
