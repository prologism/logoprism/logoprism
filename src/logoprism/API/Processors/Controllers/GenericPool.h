#ifndef GENERIC_POOL_H
#define GENERIC_POOL_H

#include <map>
#include <memory>
#include <iostream>

namespace LOGOPRISM
{
	namespace API
	{
		template <typename TPool>
		class Pool
		{
		private:
			std::map<std::string, std::weak_ptr<TPool>> m_PoolElementsById;

		private:
			Pool() {};

		public:
			Pool(const Pool& other) = delete;
			Pool& operator=(const Pool& other) = delete;
			Pool(Pool&& other) = delete;
			Pool& operator=(Pool&& other) = delete;

			static Pool& getInstance()
			{
				static Pool instance;
				return instance;
			}

			std::shared_ptr<TPool> tryToGetElementByName(const std::string& name)
			{
				std::shared_ptr<TPool> ret;

				auto pool_element = m_PoolElementsById.find(name);
				if(pool_element != m_PoolElementsById.end())
				{
					ret = pool_element->second.lock();
				}

				if(ret == nullptr)
				{
					ret = std::make_shared<TPool>(name);
					m_PoolElementsById[name] = ret;
				}

				return ret;
			}
	  };
	}
}

#endif
