namespace LOGOPRISM
{
	namespace API
	{
		template <typename TEvent>
		SerializeManager<TEvent>::SerializeManager()
        : m_IndexInterval(0)
        , m_SerializedObjectCounter(0)
        , m_ProviderDriver(nullptr)
        , m_SerializedEventDataFile()
        , m_SerializedEventIndexFile()
        , m_SerializedStatisticFile()
        , m_EventDataSerializer(m_SerializedEventDataFile)
        , m_EventIndexSerializer(m_SerializedEventIndexFile)
        , m_StatisticSerializer(m_SerializedStatisticFile)
		, m_StatManager(nullptr)
		{}

		template <typename TEvent>
		void SerializeManager<TEvent>::setProviderDriver(IProviderDriver* provider_driver)
		{
			m_ProviderDriver = provider_driver;
		}

        template <typename TEvent>
        void SerializeManager<TEvent>::setIndexInterval(const TimeDuration& interval)
        {
            m_IndexInterval = interval;
        }

		template <typename TEvent>
		void SerializeManager<TEvent>::flushAndClose()
		{
            m_SerializedEventDataFile.flush();
            m_SerializedEventDataFile.close();

            m_SerializedEventIndexFile.flush();
            m_SerializedEventIndexFile.close();

            m_SerializedStatisticFile.flush();
            m_SerializedStatisticFile.close();
		}

        template <typename TEvent>
        uint64_t SerializeManager<TEvent>::getSerializedObjectCounter() const
        {
            return m_SerializedObjectCounter;
        }

		template <typename TEvent>
		bool SerializeManager<TEvent>::serialize()
		{
            std::deque<TEvent> computed_events;
            this->swapQueueContentsTimed(std::chrono::microseconds(1000), computed_events);
			if(!computed_events.empty())
			{
                m_StatManager->setDataStartTime(computed_events.begin()->getStartTime());

                std::future<void> statistics_future = std::async(std::launch::async, &StatisticsManager::calculateGlobalStatisticsAndChartData<TEvent>, m_StatManager.get(), std::ref(computed_events));
                for(auto& element : computed_events)
				{
                    m_StatManager->setDataEndTime(element.getStartTime() + element.getDuration());
                    m_EventDataSerializer(element);
                    if(m_EventIndex.empty() || element.getStartTime() - m_EventIndex.rbegin()->first > m_IndexInterval)
                    {
                        m_SerializedEventDataFile.flush();
                        m_EventIndex.insert(std::make_pair(element.getStartTime(), m_SerializedEventDataFile.tellp()));
                    }
                    m_SerializedObjectCounter++;
				}
                statistics_future.get();
				return true;
			}
			else
			{
                if(!this->isSourceStateGood())
                {
                    m_EventIndexSerializer(m_EventIndex);
                    m_StatManager->calculateGlobalPercentiles();
                    m_StatisticSerializer(m_StatManager->getGlobalStatisticsMap());
                    m_StatisticSerializer(m_StatManager->getChartDataMap());
                    m_StatisticSerializer(m_StatManager->getDataStartTime());
                    m_StatisticSerializer(m_StatManager->getDataEndTime());
                    m_StatisticSerializer(m_StatManager->getGlobalStatisticsOrderVector());
                }

                return this->isSourceStateGood();
			}
		}

		template <typename TEvent>
		void SerializeManager<TEvent>::setStatisticsManager(const std::shared_ptr<StatisticsManager>& manager)
		{
			m_StatManager = manager;
		}

        template <typename TEvent>
        std::map<Timestamp, size_t>& SerializeManager<TEvent>::getEventIndexMap()
        {
            return m_EventIndex;
        }

        template <typename TEvent>
        const std::map<Timestamp, size_t>& SerializeManager<TEvent>::getEventIndexMap() const
        {
            return m_EventIndex;
        }

        template <typename TEvent>
        void SerializeManager<TEvent>::setSerializerStreams(std::ofstream&& event_data, std::ofstream&& event_index, std::ofstream&& stat_data)
        {
            m_SerializedEventDataFile = std::move(event_data);
            m_SerializedEventIndexFile = std::move(event_index);
            m_SerializedStatisticFile = std::move(stat_data);
        }

        template <typename TEvent>
        void SerializeManager<TEvent>::initializeProviderDriver(const std::string& source)
        {
            if(this->m_ProviderDriver)
            {
                this->m_ProviderDriver->setProviderSource(source);
                return;
            }

            throw std::runtime_error("No registered Provider Driver for reading raw data.");
        }
	}
}
