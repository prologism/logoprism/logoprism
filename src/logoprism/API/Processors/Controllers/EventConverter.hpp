namespace LOGOPRISM
{
	namespace API
	{
		template <typename FromData, typename ToData, typename TParser>
        std::deque<ToData> EventConverter<FromData, ToData, TParser>::provideDataImp()
		{
            std::deque<FromData> received_data;
			this->swapQueueContentsTimed(std::chrono::microseconds(100000), received_data);

            std::deque<ToData> converted_data;
            converted_data = m_Converter->convert(received_data);
			return converted_data;
		}

		template <typename FromData, typename ToData, typename TParser>
		bool EventConverter<FromData, ToData, TParser>::start()
		{
			if(!isRunning())
			{
				m_Running.store(true, std::memory_order_seq_cst);
				m_Thread = std::thread(&EventConverter::threadFunction, this);
				return true;
			}
			return false;
		}

		template <typename FromData, typename ToData, typename TParser>
		bool EventConverter<FromData, ToData, TParser>::stop()
		{
			if(isRunning())
			{
				m_Running.store(false, std::memory_order_seq_cst);
			}

			if(m_Thread.joinable())
			{
				m_Thread.join();
			}

			return true;
		}

		template <typename FromData, typename ToData, typename TParser>
		bool EventConverter<FromData, ToData, TParser>::isRunning()
		{
			return m_Running.load(std::memory_order_seq_cst);
		}

		template <typename FromData, typename ToData, typename TParser>
		void EventConverter<FromData, ToData, TParser>::threadFunction()
		{
			while(isRunning())
			{
				this->provideData();
			}
		}

		template <typename FromData, typename ToData, typename TParser>
		bool EventConverter<FromData, ToData, TParser>::isDataAvailable() const
		{
			return this->isSourceStateGood();
		}
	}
}
