namespace LOGOPRISM
{
    namespace API
    {
        template <typename TEvent>
        Timestamp EventManager<TEvent>::getSimulationReferenceTime()
        {
            if(!this->m_EventContainer.empty())
            {
                return this->m_EventContainer.begin()->second->getStartTime();
            }
            else
            {
                return INVALID_TIMESTAMP;
            }
        }

        template <typename TEvent>
        bool EventManager<TEvent>::isProviderChainDepleted()
        {
            return !this->isSourceStateGood();
        }

        template <typename TEvent>
        Timestamp EventManager<TEvent>::getNextEventInTime(const Timestamp& simulation_time, bool direction_reversed)
        {
            if(!direction_reversed)
            {
                auto it = this->m_EventContainer.upper_bound(simulation_time);
                if(it != this->m_EventContainer.end())
                {
                    return it->second->getStartTime();
                }
            }
            else
            {
                auto it = this->m_EventContainer.lower_bound(simulation_time);
                if(it != this->m_EventContainer.end())
                {
                    if(it == this->m_EventContainer.begin()) return it->second->getStartTime();
                    else return (--it)->second->getStartTime();
                }
            }
            return INVALID_TIMESTAMP;
        }

        template <typename TEvent>
        void EventManager<TEvent>::simulationTimeChanged()
        {
            m_SimulationTimeChanged = true;
        }

        template <typename TEvent>
        bool EventManager<TEvent>::getComputedEventsFor(const Timestamp& simulation_time, const TimeDuration& interval, bool direction_reversed, std::vector<std::shared_ptr<API::EventBase>>& output)
        {
            static Timestamp sim_time = simulation_time;

            /* Event Eviction */
            if(m_SimulationTimeChanged)
            {
                m_SimulationTimeChanged = false;
                if (simulation_time < sim_time) evictEventsTill(sim_time);
            }
            sim_time = simulation_time;
            evictExpiredEvents(simulation_time - interval / 2);

            std::deque<TEvent> computed_events;
            this->swapQueueContentsImmediate(computed_events);

            if(!computed_events.empty())
            {
                for(auto&& element : computed_events)
                {
                    auto res = this->m_EventContainer.insert(std::make_pair(element.getStartTime(), std::make_shared<TEvent>(std::forward<TEvent>(element))));
                    if(res.second)
                        this->m_StartTimesByEndTime.insert(std::make_pair(element.getStartTime() + element.getDuration(), element.getStartTime()));
                }
            }

            if(this->m_EventContainer.empty() || simulation_time == INVALID_TIMESTAMP)
                return false;

            auto first_timestamp = this->m_EventContainer.begin()->second->getStartTime();
            auto last_timestamp = this->m_EventContainer.rbegin()->second->getStartTime();

            if( !this->isSourceStateGood() || ((direction_reversed && first_timestamp < simulation_time - interval / 2) || (!direction_reversed && last_timestamp > simulation_time + interval / 2)))
            {
                auto high_bound = this->m_EventContainer.upper_bound(simulation_time + interval / 2);
                auto index_iter = this->m_EventContainer.begin();
                while(index_iter != high_bound && !(index_iter->second->getStartTime() > simulation_time - interval / 2 - index_iter->second->getDuration())) { ++index_iter; }

                for(auto element_it = index_iter; element_it != high_bound && element_it != this->m_EventContainer.end(); element_it++)
                {
                    auto& element = element_it->second;
                    if(element->getStartTime() > simulation_time - interval / 2 - element->getDuration())
                    {
                        output.emplace_back(element);
                    }
                }
                return true;
            }
            return false;
        }

        template <typename TEvent>
        void EventManager<TEvent>::setReadMargin(const TimeDuration& duration)
        {
            m_ReadMargin = duration;
        }

        template <typename TEvent>
        uint64_t EventManager<TEvent>::evictExpiredEvents(const Timestamp& time)
        {
            uint64_t evict_count = 0;
            auto upper_bound = this->m_StartTimesByEndTime.upper_bound(time - m_EvictionMargin);
            for (auto it = this->m_StartTimesByEndTime.begin(); it != upper_bound; it++)
            {
                this->m_EventContainer.erase(it->second);
                evict_count++;
            }

            this->m_StartTimesByEndTime.erase(this->m_StartTimesByEndTime.begin(), upper_bound);
            return evict_count;
        }

        template <typename TEvent>
        uint64_t EventManager<TEvent>::evictEventsTill(const Timestamp& time)
        {
            uint64_t evict_count = 0;
            auto lower_bound = this->m_EventContainer.lower_bound(time + m_EvictionMargin);
            for (auto it = lower_bound; it != this->m_EventContainer.end(); it++)
            {
                this->m_StartTimesByEndTime.erase(it->second->getStartTime() + it->second->getDuration());
                evict_count++;
            }

            this->m_EventContainer.erase(lower_bound, this->m_EventContainer.end());
            return evict_count;
        }

        template <typename TEvent>
        void EventManager<TEvent>::setEvictMargin(const TimeDuration& duration)
        {
            m_EvictionMargin = duration;
        }

        template <typename TEvent>
        bool EventManager<TEvent>::superviseProviderDriver(Timestamp simulation_time)
        {
            if(!this->m_EventContainer.empty())
            {
                auto last_timestamp = this->m_EventContainer.rbegin()->second->getStartTime();
                if(last_timestamp < simulation_time + m_ReadMargin)
                {
                    this->m_ProviderDriver->startDataFlow();
                }
                else
                {
                    this->m_ProviderDriver->stopDataFlow();
                }
            }
            else
            {
                this->m_ProviderDriver->startDataFlow();
            }

            return this->isSourceStateGood();
        }

        template <typename TEvent>
        void EventManager<TEvent>::setProviderDriverOffset(size_t offset)
        {
            this->m_ProviderDriver->modifyOffset(offset);
        }

        template <typename TEvent>
        void EventManager<TEvent>::initializeProviderDriver(const std::string& source)
        {
            if(this->m_ProviderDriver)
            {
                this->m_ProviderDriver->setProviderSource(source);
                return;
            }

            throw std::runtime_error("No registered Provider Driver for reading serialized data.");
        }
    }
}
