#ifndef EVENT_CONVERTER_H
#define EVENT_CONVERTER_H

/**
 * @file EventConverter.h
 * @brief Concrete EventProvider class definition.
 */

#include "IConverter.h"
#include "RawProvider.h"

namespace LOGOPRISM
{

	namespace API
	{

		/**
		 *  Concrete EventConverter implementing the IEventConverter and IThreadManager
		 *  The class is move only type with default move operators.
		 *  It provides data as an event converted by its converter and it consumes the data provided by the RawProvider.
		 */
		template <typename FromData, typename ToData, typename TConverter>
		class EventConverter : public IConverter<FromData, ToData>, public IThreadManager
		{

		private:
			std::shared_ptr<TConverter> m_Converter; /**< Converter implemented by the client */

		public:
			/**
			 *  Constructor.
			 *  It waits for a consumer pointer object to which it will forwards the data and a parser.
			 */
			EventConverter(std::shared_ptr<IConsumer<ToData>> consumer, std::shared_ptr<TConverter> converter) : IConverter<FromData, ToData>(consumer), IThreadManager(), m_Converter(converter) {}
			EventConverter(const EventConverter& other) = delete;
			EventConverter& operator=(const EventConverter& other) = delete;
			EventConverter(EventConverter&& other) = default;
			EventConverter& operator=(EventConverter&& other) = default;
			~EventConverter() {}

			/**
			 *  Starts the thread.
			 *  Inherited from IThreadInterface. It starts a thread
			 *  return A Boolean indication a successful start
			 */
			bool start() override;

			/**
			 *  Stops the thread.
			 *  Stops the thread loop by sending a stop signal.
			 *  Inherited from IThreadInterface. It stops a thread.
			 *  return A Boolean indication a successful stop
			 */
			bool stop() override;

			/**
			 *  Checks if thread runs or not.
			 *  Inherited from IThreadInterface.
			 *  return A Boolean indicating if the thread runs or not
			 */
			bool isRunning() override;

		private:
			/**
			 * Thread function to be called in a thread loop.
			 * It must manage the thread stop and suspend conditions.
			 */
			void threadFunction();

			/**
			 *  Inherited function from IProvider.
			 *  It calls the client getData function which provides the data record by record.
			 *  It is the responsibility of the client to handle the resource
			 */
            std::deque<ToData> provideDataImp() override;

			/**
			 *  Checks if the source is depleted or not.
			 *  return A Boolean indicating source depletion.
			 */
			bool isDataAvailable() const override;
		};
	}
}

#include "EventConverter.hpp"

#endif
