#ifndef I_PROVIDER_DRIVER_H
#define I_PROVIDER_DRIVER_H

/**
 * @file IProviderDriver.h
 * @brief Interface definition for IProviderDriver.
 */
#include <stddef.h>
#include <istream>

namespace LOGOPRISM
{
    namespace API
    {

        /**
         *  IProviderDriver to start and stop data flow.
         *  It provides the ability to start or stop the data flow and check if data is available in the source.
         */
        class IProviderDriver
        {
        public:
            IProviderDriver() {}
            IProviderDriver(const IProviderDriver& other) = default;
            IProviderDriver& operator=(const IProviderDriver& other) = default;
            IProviderDriver(IProviderDriver&& other) = default;
            IProviderDriver& operator=(IProviderDriver&& other) = default;
            ~IProviderDriver() {}

            /**
             *  Starts the data flow.
             *  Sends a signal to start the data flow. The object must have a concrete source.
             */
            virtual void startDataFlow() = 0;

            /**
             *  Stops the data flow.
             *  If no more data is needed the consumer can ask to stop the data flow.
             */
            virtual void stopDataFlow() = 0;

            /**
             *  Checks if the data is flows or not.
             *  @return A Boolean indicating if data flows or not.
             */
            virtual bool isDataFlows() = 0;

            /**
             *  Modifies the provider offset.
             *  @param offset the new offset for the provider.
             */
            virtual void modifyOffset(size_t offset) = 0;

            /**
             *  Sets the provider source.
             *  @param source the source of the provider.
             */
            virtual void setProviderSource(const std::string& source) = 0;
        };
    }
}

#endif
