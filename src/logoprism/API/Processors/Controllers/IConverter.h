#ifndef I_CONVERTER_H
#define I_CONVERTER_H

/**
 * @file IConverter.h
 * @brief Interface definition for IConverter.
 */

#include "IProvider.h"

namespace LOGOPRISM
{

	namespace API
	{

		/**
		 *  IConverter implementing IProvider and IConsumer.
		 *  It consumes the data provided by a provider and received by its consumer.
		 *  Then it transforms its data with the help of its provider.
		 *  Template parameters are the the input data type and output data type.
		 */
		template <typename FromData, typename ToData>
		class IConverter : public IProvider<ToData>, public IConsumer<FromData>
		{
		public:

			/**
			 *  Constructor.
			 *  It waits for a consumer pointer to which it will forwards the data.
			 */
			IConverter(std::shared_ptr<IConsumer<ToData>> consumer) : IProvider<ToData>(consumer), IConsumer<FromData>() {}
			IConverter(const IConverter& other) = default;
			IConverter& operator=(const IConverter& other) = default;
			IConverter(IConverter&& other) = default;
			IConverter& operator=(IConverter&& other) = default;
			~IConverter() {}

		private:

			/**
			 * 	A provideData implementation function.
			 *  Inherited from IProvider interface.
			 *  The implementer must implement the function and manage the details of data retrieval.
			 *  The data is returned in a form a vector.
			 *  @return A vector containing the retrieved data.
			 */
            virtual std::deque<ToData> provideDataImp() override = 0;

			/**
			 *  Checks if the source is depleted or not.
			 *  return A Boolean indicating source depletion.
			 */
			bool isDataAvailable() const override = 0;
		};
	}
}

#endif
