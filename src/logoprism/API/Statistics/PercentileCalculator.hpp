namespace LOGOPRISM
{
	namespace API
	{
		template <typename T> PercentileCalculator<T>::PercentileCalculator(double epsilon)
	            				: m_epsilon(epsilon)
								, m_one_over_2e((int)(1/(2 * epsilon)))
								, m_n(0)
								, m_S{}
	    						{}

	    template <typename T> void PercentileCalculator<T>::insert(T v)
	    {
	    	if (m_n % m_one_over_2e == 0)
	    		compress();

	    	do_insert(v);
	    	++m_n;
	    }

	    template <typename T> T PercentileCalculator<T>::quantile(double phi) const
	    {
	    	double en = m_epsilon * m_n;
	    	int r = (int)ceil(phi * m_n);
	    	double rmin = 0;
	    	for (auto it = m_S.begin(); it != m_S.end(); ++it)
	    	{
	    		rmin += it->g;
	    		double rmax = rmin + it->delta;
	    		if (r - rmin <= en && rmax - r <= en)
	    			return it->v;
	    	}

	    	// Should never reach the below
	    	assert(false);
	    	return T{};
	    }

	    template <typename T> int PercentileCalculator<T>::determine_delta(tuples_iterator it)
	    {
	    	if (m_n <= m_one_over_2e)
	    	{
	    		return 0;
	    	}
	    	else if (it == m_S.begin() || it == m_S.end())
	    	{
	    		return 0;
	    	}
	    	else
	    	{
	    		int delta = (int)floor(2 * m_epsilon * m_n) - 1;
	    		assert(delta >= 0 && delta <= 2 * m_epsilon * m_n - 1);
	    		return delta;
	    	}
	    }

	    template <typename T> typename PercentileCalculator<T>::tuples_iterator PercentileCalculator<T>::find_insertion_index(T v)
	    {
	    	tuples_iterator it = m_S.begin();
	    	while (it != m_S.end() && it->v < v)
	    		++it;

	    	return it;
	    }

	    template <typename T> void PercentileCalculator<T>::do_insert(T v)
	    {
	    	tuples_iterator it = find_insertion_index(v);
	    	tuple t(v, 1, determine_delta(it));
	    	m_S.insert(it, t);
	    }

	    template <typename T> void PercentileCalculator<T>::compress()
	    {
	    	if (m_S.empty())
	    		return;

	    	int two_epsilon_n = (int)(2 * m_epsilon * m_n);
	    	std::vector<int> bands = construct_band_lookup(two_epsilon_n);
	    	tuples_iterator it = prev(prev(m_S.end()));
	    	while (it != m_S.begin())
	    	{
	    		tuples_iterator it2 = next(it);
	    		if (bands[it->delta] <= bands[it2->delta])
	    		{
	    			int g_i_star = it->g;
	    			tuples_iterator start_it = it;
	    			while (start_it != next(m_S.begin()) && bands[prev(start_it)->delta] < bands[it->delta])
	    			{
	    				--start_it;
	    				g_i_star += start_it->g;
	    			}

	    			if ((g_i_star + it2->g + it2->delta) < two_epsilon_n)
	    			{
	    				tuple merged(it2->v, g_i_star + it2->g, it2->delta);
	    				*start_it = merged;
	    				m_S.erase(next(start_it), next(it2));
	    				it = start_it;
	    			}
	    		}
	    		--it;
	    	}
	    }
	}
}
