#include "SnapStatistics.h"

namespace LOGOPRISM
{
	namespace API
	{
		SnapStatistics::SnapStatistics()
			: m_ResponseTimeMax(TimeDuration::min())
			, m_ResponseTimeMin(TimeDuration::max())
			, m_ResponseTimeAvg(TimeDuration::zero())
			, m_EventCount(0)
			{}

		void SnapStatistics::calculateMax(const TimeDuration& val)
		{
			if(val > m_ResponseTimeMax)
			{
				m_ResponseTimeMax = val;
			}
		}

		void SnapStatistics::calculateMin(const TimeDuration& val)
		{
			if(val < m_ResponseTimeMin)
			{
				m_ResponseTimeMin = val;
			}
		}

		void SnapStatistics::calculateAvg(const TimeDuration& val)
		{
			m_ResponseTimeAvg = (m_ResponseTimeAvg * m_EventCount + val) / (m_EventCount + 1);
			m_EventCount++;
		}

		const TimeDuration& SnapStatistics::getMaxResponseTime() const
		{
			return m_ResponseTimeMax;
		}

		const TimeDuration& SnapStatistics::getMinResponseTime() const
		{
			return m_ResponseTimeMin;
		}

		const TimeDuration& SnapStatistics::getAvgResponseTime() const
		{
			return m_ResponseTimeAvg;
		}

		void SnapStatistics::calculateAll(const TimeDuration& val)
		{
			calculateMin(val);
			calculateMax(val);
			calculateAvg(val);
		}

		void SnapStatistics::calculateDeviationFromMax(const TimeDuration& duration, const TimeDuration& max)
		{
			m_DeviationFromMax = duration - max;
		}

		void SnapStatistics::calculateDeviationFromMin(const TimeDuration& duration, const TimeDuration& min)
		{
			m_DeviationFromMin = duration - min;
		}

		void SnapStatistics::calculateDeviationFromAvg(const TimeDuration& duration, const TimeDuration& avg)
		{
			m_DeviationFromAvg = duration - avg;
		}


		const TimeDuration& SnapStatistics::getDeviationFromMax() const
		{
			return m_DeviationFromMax;
		}

		const TimeDuration& SnapStatistics::getDeviationFromMin() const
		{
			return m_DeviationFromMin;
		}

		const TimeDuration& SnapStatistics::getDeviationFromAvg() const
		{
			return m_DeviationFromAvg;
		}

		void SnapStatistics::reset()
		{
			m_ResponseTimeMax = TimeDuration::min();
			m_ResponseTimeMin = TimeDuration::max();
			m_ResponseTimeAvg = TimeDuration::zero();
			m_EventCount = 0;
		}

		uint64_t SnapStatistics::getEventCount() const
		{
			return m_EventCount;
		}

		std::ostream& operator<<(std::ostream& os, const SnapStatistics& stat)
		{
			auto min_response_time = stat.getMinResponseTime() == TimeDuration::max() ? 0.0f : stat.getMinResponseTime().count() / 1000.0f;
			auto max_response_time = stat.getMaxResponseTime() == TimeDuration::min() ? 0.0f : stat.getMaxResponseTime().count() / 1000.0f;
			auto dev_from_min = stat.getDeviationFromMin() == TimeDuration::max() ? 0.0f : stat.getDeviationFromMin().count() / 1000.0f;
			auto dev_from_max = stat.getDeviationFromMax() == TimeDuration::min() ? 0.0f : stat.getDeviationFromMax().count() / 1000.0f;

			os << "Min Response Time: " << min_response_time << " us " << std::endl
			   << "Max Response Time: " << max_response_time / 1000.0 << " us " << std::endl
			   << "Avg Response Time: " << stat.getAvgResponseTime().count() / 1000.0 << " us " << std::endl
			   << "Deviation From Min: " << dev_from_min << " us " << std::endl
			   << "Deviation From Max: " << dev_from_max << " us " << std::endl
			   << "Deviation From Avg: " << stat.getDeviationFromAvg().count() / 1000.0 << " us " << std::endl
			   << "Number of events: " << stat.getEventCount();

			return os;
		}
	}
}
