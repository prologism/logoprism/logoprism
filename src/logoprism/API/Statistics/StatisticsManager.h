#ifndef STATISTICS_MANAGER_H
#define STATISTICS_MANAGER_H

#include <map>
#include <vector>
#include <string>
#include <functional>
#include <memory>

#include "GlobalStatistics.h"
#include "SnapStatistics.h"
#include "logoprism/API/Processors/Models/EventBase.h"

namespace LOGOPRISM
{
    namespace API
    {
        class StatisticsManager
        {
        private:
            std::map<std::string, GlobalStatistics> m_GlobalStatisticsById;
            std::vector<std::string> m_GlobalStatisticsIdOrder;

            std::map<std::string, SnapStatistics> m_SnapStatisticsById;
            std::vector<std::string> m_SnapStatisticsIdOrder;

            std::map<std::string, std::map<int64_t, uint32_t>> m_ChartDataById;

            Timestamp m_DataStartTime;
            Timestamp m_DataEndTime;

        public:
            StatisticsManager() : m_DataStartTime(Timestamp::max()), m_DataEndTime(Timestamp::min())
            {
                addGlobalStatistics("Global", GlobalStatistics(1000));
                addSnapStatistics("Global", SnapStatistics());
                addChartData("Global", std::map<int64_t, uint32_t>());
            }

            StatisticsManager(const StatisticsManager& other) = default;
            StatisticsManager& operator=(const StatisticsManager& other) = default;
            StatisticsManager(StatisticsManager&& other) = default;
            StatisticsManager& operator=(StatisticsManager& other) = default;
            virtual ~StatisticsManager(){}

            bool addGlobalStatistics(const std::string&, const GlobalStatistics&);
            bool addSnapStatistics(const std::string&, const SnapStatistics&);
            bool addChartData(const std::string& id, const std::map<int64_t, uint32_t>& data);
            std::vector<std::string>& getGlobalStatisticsOrderVector();
            std::map<std::string, GlobalStatistics>& getGlobalStatisticsMap();
            std::vector<std::string>& getSnapStatisticsOrderVector();
            std::map<std::string, SnapStatistics>& getSnapStatisticsMap();
            std::map<std::string, std::map<int64_t, uint32_t>>& getChartDataMap();

            virtual void calculateGlobalStatisticsAndChartData(API::EventBase* event);
            virtual void calculateSnapStatistics(std::vector<std::shared_ptr<API::EventBase>>& events);

            void setDataStartTime(const Timestamp& start_time);
            const Timestamp& getDataStartTime() const;

            void setDataEndTime(const Timestamp& end_time);
            const Timestamp& getDataEndTime() const;

            template <typename TEvent>
            void calculateGlobalStatisticsAndChartData(std::deque<TEvent>& events)
            {
                for(auto& event : events)
                {
                    calculateGlobalStatisticsAndChartData(&event);
                }
            }

            void calculateGlobalPercentiles();
            void printGlobalStatistics();
            void resetSnapStatistics();
            TimeDuration getMaximumDuration();
		};
	}
}

#endif
