#ifndef SNAP_STATISTICS_H
#define SNAP_STATISTICS_H

#include "logoprism/API/Utils/Timestamp.h"
#include <iostream>

namespace LOGOPRISM
{
	namespace API
	{
		class SnapStatistics
		{
		public:
			TimeDuration m_ResponseTimeMax;
			TimeDuration m_ResponseTimeMin;
			TimeDuration m_ResponseTimeAvg;
			TimeDuration m_DeviationFromMax;
			TimeDuration m_DeviationFromMin;
			TimeDuration m_DeviationFromAvg;
			uint64_t m_EventCount;

		public:
			SnapStatistics();
			SnapStatistics(const SnapStatistics& other) = default;
			SnapStatistics& operator=(const SnapStatistics& other) = default;
			SnapStatistics(SnapStatistics&& other) = default;
			SnapStatistics& operator=(SnapStatistics&& other) = default;

			void calculateMax(const TimeDuration& val);
			void calculateMin(const TimeDuration& val);
			void calculateAvg(const TimeDuration& val);
			void calculateAll(const TimeDuration& val);

			const TimeDuration& getMaxResponseTime() const;
			const TimeDuration& getMinResponseTime() const;
			const TimeDuration& getAvgResponseTime() const;

			const TimeDuration& getDeviationFromMax() const;
			const TimeDuration& getDeviationFromMin() const;
			const TimeDuration& getDeviationFromAvg() const;

			uint64_t getEventCount() const;

			void calculateDeviationFromMax(const TimeDuration& duration, const TimeDuration& max);
			void calculateDeviationFromMin(const TimeDuration& duration, const TimeDuration& min);
			void calculateDeviationFromAvg(const TimeDuration& duration, const TimeDuration& avg);

			void reset();

			friend std::ostream& operator<<(std::ostream& os, const SnapStatistics& stat);
	  };
	}
}

#endif
