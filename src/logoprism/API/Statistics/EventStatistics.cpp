#include "EventStatistics.h"

namespace LOGOPRISM
{
	namespace API
	{
		void EventStatistics::calculateDeviationFromMax(const TimeDuration& duration, const TimeDuration& max)
		{
			m_DeviationFromMax = duration - max;
		}

		void EventStatistics::calculateDeviationFromMin(const TimeDuration& duration, const TimeDuration& min)
		{
			m_DeviationFromMin = duration - min;
		}

		void EventStatistics::calculateDeviationFromAvg(const TimeDuration& duration, const TimeDuration& avg)
		{
			m_DeviationFromAvg = duration - avg;
		}

		void EventStatistics::setPercentileGroup(EventStatistics::PercentileGroup pgroup)
		{
			m_PercentileGroup = pgroup;
		}

		const TimeDuration& EventStatistics::getDeviationFromMax() const
		{
			return m_DeviationFromMax;
		}

		const TimeDuration& EventStatistics::getDeviationFromMin() const
		{
			return m_DeviationFromMin;
		}

		const TimeDuration& EventStatistics::getDeviationFromAvg() const
		{
			return m_DeviationFromAvg;
		}

		EventStatistics::PercentileGroup EventStatistics::getPercentileGroup() const
		{
			return m_PercentileGroup;
		}
	}
}
