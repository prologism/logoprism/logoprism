#ifndef PERCENTILE_CALCULATOR
#define PERCENTILE_CALCULATOR
#include <cmath>
#include <vector>
#include <assert.h>

// The Greenwald-Khanna algorithm as defined in the paper
// Space-Efficient Online Computation of Quantile Summaries

namespace LOGOPRISM
{
	namespace API
	{

    	template <typename T>
    	class PercentileCalculator
		{
		public:
    		PercentileCalculator(double epsilon);
    		PercentileCalculator(const PercentileCalculator&) = default;
    		PercentileCalculator& operator=(const PercentileCalculator&) = default;
    		PercentileCalculator(PercentileCalculator&&) = default;
    		PercentileCalculator& operator=(PercentileCalculator&&) = default;

    		void insert(T v);
    		T quantile(double phi) const;

		private:
    		struct tuple
			{
    			T v;
    			int g;
    			int delta;

    			tuple(T v, int g, int delta)
    			: v(v), g(g), delta(delta) {}
			};

    		typedef std::vector<tuple> tuples_t;
    		typedef typename tuples_t::iterator tuples_iterator;

    		int determine_delta(tuples_iterator it);
    		tuples_iterator find_insertion_index(T v);
    		void do_insert(T v);
    		void compress();

    		static const int MAX_BAND = 999999;
    		static std::vector<int> construct_band_lookup(int two_epsilon_n)
        	{

    			// TODO: This function is rather slow.  Rewrite to be faster.
    			std::vector<int> bands(two_epsilon_n + 1);
    			bands[0] = MAX_BAND; // delta = 0 is its own band
    			bands[two_epsilon_n] = 0; // delta = two_epsilon_n is band 0 by definition

    			for (int alpha = 1; alpha <= ceil(log2(two_epsilon_n)); ++alpha)
    			{
    				int two_alpha_minus_1 = (1 << (alpha-1));
    				int two_alpha = two_alpha_minus_1 << 1;
    				int lower = two_epsilon_n - two_alpha - (two_epsilon_n % two_alpha);
    				if (lower < 0)
    					lower = 0;

    				int upper = two_epsilon_n - two_alpha_minus_1 - (two_epsilon_n % two_alpha_minus_1);
    				for (int  i = lower + 1; i <= upper; ++i)
    				{
    					bands[i] = alpha;
    				}
    			}

    			return bands;
        	}

    		double m_epsilon;
    		int m_one_over_2e;
    		int m_n;
    		tuples_t m_S;
		};
	}
}

#include "PercentileCalculator.hpp"

#endif
