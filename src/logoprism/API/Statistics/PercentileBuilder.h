#ifndef PERCENTILE_BUILDER
#define PERCENTILE_BUILDER
#include <map>

namespace LOGOPRISM
{
    namespace API
    {

        class PercentileBuilder
        {
        public:
            PercentileBuilder(uint64_t step_size = 1000) : m_StepSize(step_size) , m_ReceivedCounter(0) {}
      	    void addValue(uint64_t value);
	        uint64_t calculatePercentile(double percentile);

        private:
            int m_StepSize;
            std::map<uint64_t, uint64_t> m_Values;
            uint64_t m_ReceivedCounter;
        };
    }
}
#endif
