#ifndef EVENT_STATISTICS_H
#define EVENT_STATISTICS_H

#include <iostream>
#include "logoprism/API/Utils/Timestamp.h"

namespace LOGOPRISM
{
	namespace API
	{
		class EventStatistics
		{
		public:
			enum PercentileGroup
			{
				UNKNOWN         = 0,
				PERCENTILE_50   = 1,
				PERCENTILE_90   = 2,
				PERCENTILE_99   = 3,
				PERCENTILE_99_9 = 4
			};

		public:
			TimeDuration m_DeviationFromMax;
			TimeDuration m_DeviationFromMin;
			TimeDuration m_DeviationFromAvg;
			PercentileGroup m_PercentileGroup;

		public:
			EventStatistics() : m_PercentileGroup(UNKNOWN) {};
			EventStatistics(const EventStatistics& other) = default;
			EventStatistics& operator=(const EventStatistics& other) = default;
			EventStatistics(EventStatistics&& other) = default;
			EventStatistics& operator=(EventStatistics&& other) = default;

			void calculateDeviationFromMax(const TimeDuration& duration, const TimeDuration& max);
			void calculateDeviationFromMin(const TimeDuration& duration, const TimeDuration& min);
			void calculateDeviationFromAvg(const TimeDuration& duration, const TimeDuration& avg);
			void setPercentileGroup(EventStatistics::PercentileGroup pgroup);

			const TimeDuration& getDeviationFromMax() const;
			const TimeDuration& getDeviationFromMin() const;
			const TimeDuration& getDeviationFromAvg() const;
			PercentileGroup getPercentileGroup() const;

			friend std::ostream& operator<<(std::ostream& os, const EventStatistics& stat)
			{
				os << "Deviation From Min: " << stat.getDeviationFromMin().count() << " ns "
				   << "Deviation From Max: " << stat.getDeviationFromMax().count() << " ns "
				   << "Deviation From Avg: " << stat.getDeviationFromAvg().count() << " ns "
				   << "Percentile Group: " << stat.getPercentileGroup();

				return os;
			}
	  };
	}
}

#endif
