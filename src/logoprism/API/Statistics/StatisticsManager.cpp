#include "StatisticsManager.h"

namespace LOGOPRISM
{
    namespace API
    {
        bool StatisticsManager::addGlobalStatistics(const std::string& id, const GlobalStatistics& stat)
        {
            if(m_GlobalStatisticsById.find(id) == m_GlobalStatisticsById.end())
            {
                m_GlobalStatisticsById.insert(std::make_pair(id, stat));
                m_GlobalStatisticsIdOrder.push_back(id);
                return true;
            }
            return false;
        }

        bool StatisticsManager::addChartData(const std::string& id, const std::map<int64_t, uint32_t>& data)
        {
            if(m_ChartDataById.find(id) == m_ChartDataById.end())
            {
                m_ChartDataById.insert(std::make_pair(id, data));
                return true;
            }
            return false;
        }

        bool StatisticsManager::addSnapStatistics(const std::string& id, const SnapStatistics& stat)
        {
            if(m_SnapStatisticsById.find(id) == m_SnapStatisticsById.end())
            {
                m_SnapStatisticsById.insert(std::make_pair(id, stat));
                m_SnapStatisticsIdOrder.push_back(id);
                return true;
            }
            return false;
        }

        std::vector<std::string>& StatisticsManager::getGlobalStatisticsOrderVector()
        {
            return m_GlobalStatisticsIdOrder;
        }

        std::map<std::string, GlobalStatistics>& StatisticsManager::getGlobalStatisticsMap()
        {
            return m_GlobalStatisticsById;
        }

        std::vector<std::string>& StatisticsManager::getSnapStatisticsOrderVector()
        {
            return m_SnapStatisticsIdOrder;
        }

        std::map<std::string, SnapStatistics>& StatisticsManager::getSnapStatisticsMap()
        {
            return m_SnapStatisticsById;
        }

        std::map<std::string, std::map<int64_t, uint32_t>>& StatisticsManager::getChartDataMap()
        {
            return m_ChartDataById;
        }

        void StatisticsManager::calculateGlobalStatisticsAndChartData(API::EventBase* event)
        {
            auto& global_stat_map = getGlobalStatisticsMap();
            global_stat_map["Global"].calculateAll(event->getDuration());


            auto& chart_data_map = getChartDataMap();
            {
                auto millisec_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(event->getStartTime().time_since_epoch()).count();
                chart_data_map["Number Of Events By Time"][millisec_since_epoch]++;
            }
            {
                auto millisec_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(event->getStartTime().time_since_epoch()).count();
                if(chart_data_map["Latency by Time"][millisec_since_epoch] < event->getDuration().count() / 1000.0)
                {
                    chart_data_map["Latency by Time"][millisec_since_epoch] = event->getDuration().count() / 1000.0;
                }
            }

        }

        void StatisticsManager::calculateSnapStatistics(std::vector<std::shared_ptr<API::EventBase>>& events)
        {
            for(auto& element : events)
            {
                auto& snap_stat_map = getSnapStatisticsMap();
                snap_stat_map["Global"].calculateAll(element->getDuration());

                auto& global_stat_map = getGlobalStatisticsMap();
                snap_stat_map["Global"].calculateDeviationFromAvg(snap_stat_map["Global"].getAvgResponseTime(), global_stat_map["Global"].getAvgResponseTime());
                snap_stat_map["Global"].calculateDeviationFromMax(snap_stat_map["Global"].getMaxResponseTime(), global_stat_map["Global"].getMaxResponseTime());
                snap_stat_map["Global"].calculateDeviationFromMin(snap_stat_map["Global"].getMinResponseTime(), global_stat_map["Global"].getMinResponseTime());
            }
        }

		void StatisticsManager::calculateGlobalPercentiles()
		{
            for(auto& element : m_GlobalStatisticsById)
            {
                element.second.calculatePercentiles();
            }
        }

        void StatisticsManager::printGlobalStatistics()
        {
            for(auto& element : m_GlobalStatisticsById)
            {
                std::clog << element.first << "[ " << element.second << std::endl;
            }
        }

        void StatisticsManager::resetSnapStatistics()
        {
            for(auto& element : m_SnapStatisticsById)
            {
                element.second.reset();
            }
        }

        void StatisticsManager::setDataStartTime(const Timestamp& start_time)
        {
            if(start_time < m_DataStartTime)
                m_DataStartTime = start_time;
        }

        const Timestamp& StatisticsManager::getDataStartTime() const
        {
            return m_DataStartTime;
        }

        void StatisticsManager::setDataEndTime(const Timestamp& end_time)
        {
            if(end_time > m_DataEndTime)
                m_DataEndTime = end_time;
        }

        const Timestamp& StatisticsManager::getDataEndTime() const
        {
            return m_DataEndTime;
        }

        TimeDuration StatisticsManager::getMaximumDuration()
        {
            return m_GlobalStatisticsById["Global"].getMaxResponseTime();
        }
	}
}
