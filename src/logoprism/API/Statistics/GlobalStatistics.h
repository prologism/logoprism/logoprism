#ifndef GlOBAL_STATISTICS_H
#define GlOBAL_STATISTICS_H

#include "logoprism/API/Utils/Timestamp.h"
#include "PercentileBuilder.h"

#include <cereal/types/chrono.hpp>
#include <iostream>

namespace LOGOPRISM
{
	namespace API
	{
		class GlobalStatistics
		{
		private:
			TimeDuration m_ResponseTimeMax;
			TimeDuration m_ResponseTimeMin;
			TimeDuration m_ResponseTimeAvg;
			TimeDuration m_Percentil50;
			TimeDuration m_Percentil75;
			TimeDuration m_Percentil90;
			TimeDuration m_Percentil99;
			TimeDuration m_Percentil99_9;

			uint64_t m_EventCount;

			uint64_t m_PercetilePrecision;
			PercentileBuilder m_PercentileBuilder;
		public:
			GlobalStatistics(uint64_t precision = 1000);
			GlobalStatistics(const GlobalStatistics& other) = default;
			GlobalStatistics& operator=(const GlobalStatistics& other) = default;
			GlobalStatistics(GlobalStatistics&& other) = default;
			GlobalStatistics& operator=(GlobalStatistics&& other) = default;

			void calculateMax(const TimeDuration& val);
			void calculateMin(const TimeDuration& val);
			void calculateAvg(const TimeDuration& val);
			void calculateAll(const TimeDuration& val);

			void pushForPercentileCalculator(const TimeDuration& val);
			void calculatePercentiles();

			const TimeDuration& getMaxResponseTime() const;
			const TimeDuration& getMinResponseTime() const;
			const TimeDuration& getAvgResponseTime() const;
			const TimeDuration& getPercentile50() const;
			const TimeDuration& getPercentile75() const;
			const TimeDuration& getPercentile90() const;
			const TimeDuration& getPercentile99() const;
			const TimeDuration& getPercentile99_9() const;
			uint64_t getEventCount() const;

            template<class Archive> void load(Archive & archive)
            {
                archive(m_ResponseTimeMax, m_ResponseTimeMin, m_ResponseTimeAvg, m_Percentil50, m_Percentil75, m_Percentil90, m_Percentil99, m_Percentil99_9, m_EventCount, m_PercetilePrecision);
            }

            template<class Archive> void save(Archive & archive) const
            {
                archive(m_ResponseTimeMax, m_ResponseTimeMin, m_ResponseTimeAvg, m_Percentil50, m_Percentil75, m_Percentil90, m_Percentil99, m_Percentil99_9, m_EventCount, m_PercetilePrecision);
            }

			friend std::ostream& operator<<(std::ostream& os, const GlobalStatistics& stat);
	  };
	}
}

#endif
