#include "PercentileBuilder.h"

namespace LOGOPRISM
{
    namespace API
    {

        void PercentileBuilder::addValue(uint64_t value)
        {
            uint32_t steps = value / m_StepSize;
            m_Values[steps]++;
            m_ReceivedCounter++;
        }

	    uint64_t PercentileBuilder::calculatePercentile(double percentile)
        {
            uint64_t count = 0;
            uint64_t per_val = m_ReceivedCounter * percentile / 100.0;
            uint64_t per_val_time = m_StepSize * m_Values.rbegin()->first;

            for (auto& value : m_Values)
            {
                count += value.second;
                if (count >= per_val)
                {
                    per_val_time = value.first * m_StepSize;
                    break;
                }
	        }
            return per_val_time;
        }
    }
}
