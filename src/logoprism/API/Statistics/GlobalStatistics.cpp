#include "GlobalStatistics.h"

namespace LOGOPRISM
{
	namespace API
	{
		GlobalStatistics::GlobalStatistics(uint64_t precision)
			: m_ResponseTimeMax(TimeDuration::min())
			, m_ResponseTimeMin(TimeDuration::max())
			, m_ResponseTimeAvg(TimeDuration::zero())
		    , m_Percentil50(TimeDuration::zero())
		    , m_Percentil75(TimeDuration::zero())
			, m_Percentil90(TimeDuration::zero())
			, m_Percentil99(TimeDuration::zero())
			, m_Percentil99_9(TimeDuration::zero())
			, m_EventCount(0)
			, m_PercetilePrecision(precision)
			, m_PercentileBuilder(m_PercetilePrecision)
			{}

		void GlobalStatistics::calculateMax(const TimeDuration& val)
		{
			if(val > m_ResponseTimeMax)
			{
				m_ResponseTimeMax = val;
			}
		}

		void GlobalStatistics::calculateMin(const TimeDuration& val)
		{
			if(val < m_ResponseTimeMin)
			{
				m_ResponseTimeMin = val;
			}
		}

		void GlobalStatistics::calculateAvg(const TimeDuration& val)
		{
			m_ResponseTimeAvg = (m_ResponseTimeAvg * m_EventCount + val) / (m_EventCount + 1);
			m_EventCount++;
		}

		const TimeDuration& GlobalStatistics::getMaxResponseTime() const
		{
			return m_ResponseTimeMax;
		}

		const TimeDuration& GlobalStatistics::getMinResponseTime() const
		{
			return m_ResponseTimeMin;
		}

		const TimeDuration& GlobalStatistics::getAvgResponseTime() const
		{
			return m_ResponseTimeAvg;
		}

		const TimeDuration& GlobalStatistics::getPercentile50() const
		{
			return m_Percentil50;
		}

		const TimeDuration& GlobalStatistics::getPercentile75() const
		{
			return m_Percentil75;
		}

		const TimeDuration& GlobalStatistics::getPercentile90() const
		{
			return m_Percentil90;
		}

		const TimeDuration& GlobalStatistics::getPercentile99() const
		{
			return m_Percentil99;
		}

		const TimeDuration& GlobalStatistics::getPercentile99_9() const
		{
			return m_Percentil99_9;
		}

		void GlobalStatistics::pushForPercentileCalculator(const TimeDuration& val)
		{
			m_PercentileBuilder.addValue(val.count());
		}

		void GlobalStatistics::calculateAll(const TimeDuration& val)
		{
			calculateMin(val);
			calculateMax(val);
			calculateAvg(val);
			pushForPercentileCalculator(val);
		}

		void GlobalStatistics::calculatePercentiles()
		{
			m_Percentil50 = TimeDuration(m_PercentileBuilder.calculatePercentile(50));
			m_Percentil75 = TimeDuration(m_PercentileBuilder.calculatePercentile(75));
			m_Percentil90 = TimeDuration(m_PercentileBuilder.calculatePercentile(90));
			m_Percentil99 = TimeDuration(m_PercentileBuilder.calculatePercentile(99));
			m_Percentil99_9 = TimeDuration(m_PercentileBuilder.calculatePercentile(99.9));
		}

		uint64_t GlobalStatistics::getEventCount() const
		{
			return m_EventCount;
		}

		std::ostream& operator<<(std::ostream& os, const GlobalStatistics& stat)
		{
			os << "Min Response Time: " << stat.getMinResponseTime().count() / 1000.0 << " us " << std::endl
			   << "Max Response Time: " << stat.getMaxResponseTime().count() / 1000.0 << " us " << std::endl
			   << "Avg Response Time: " << stat.getAvgResponseTime().count() / 1000.0 << " us " << std::endl
			   << "Percentile 50: " << stat.getPercentile50().count() / 1000.0 << " us " << std::endl
			   << "Percentile 75: " << stat.getPercentile75().count() / 1000.0 << " us " << std::endl
			   << "Percentile 90: " << stat.getPercentile90().count() / 1000.0 << " us " << std::endl
			   << "Percentile 99: " << stat.getPercentile99().count() / 1000.0 << " us " << std::endl
			   << "Percentile 99_9: " << stat.getPercentile99_9().count() / 1000.0 << " us " << std::endl
			   << "Number of events: " << stat.getEventCount();

			return os;
		}
	}
}
