#include <memory>
#include <vector>
#include <boost/filesystem.hpp>
#include <QTimer>

#include <cereal/archives/binary.hpp>
#include <cereal/types/chrono.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

#include "logoprism/API/Logoprism.h"
#include "logoprism/API/Utils/Signals.h"
#include "logoprism/API/Processors/Models/EventBase.h"
#include "logoprism/API/Processors/Models/Worker.h"
#include "logoprism/API/Processors/Models/Source.h"
#include "logoprism/API/Processors/Models/Target.h"
#include "logoprism/API/Processors/Controllers/GenericPool.h"
#include "logoprism/API/Visualisers/Controllers/ViewManager.h"

namespace LOGOPRISM
{
	namespace API
	{
		Logoprism::Logoprism(const API::Configuration& config, int argc, char *argv[])
        : m_Configuration(config)
		, m_SimulationInProgress(false)
		, m_DirectionReversed(false)
		, m_StepMode(STEP_TIME)
        , m_EventMaxTimeout(0)
		, m_TimeInfo()
		, m_Application(argc, argv)
		, m_Timer()
		, m_ViewManager(m_TimeInfo)
		{
            QObject::connect(&m_Timer, &QTimer::timeout, [=](){ this->tick(); });
        }

		Logoprism::~Logoprism()	{ }

		void Logoprism::init()
		{
			if(nullptr == m_EventManager.get() || m_ReadProviders.empty() || nullptr == m_SerializeManager || m_SerializeProviders.empty() || nullptr == m_StatisticsManager)
			{
				throw std::runtime_error("There is no registered EventManager or SerializeManager  or StatisticsManager or Providers.");
			}
            
            bool serialized_streams_exists = boost::filesystem::exists(m_Configuration.get<std::string>("serializer.event-data-file")) &&
                    boost::filesystem::exists(m_Configuration.get<std::string>("serializer.event-index-file")) &&
                    boost::filesystem::exists(m_Configuration.get<std::string>("serializer.stat-data-file"));

            if(!boost::filesystem::exists(m_Configuration.get<std::string>("input.raw-file")) && !serialized_streams_exists)
            {
                throw std::runtime_error("The raw input file and the serialization files does not exists.");
            }

            m_ViewManager.setStatisticsManager(m_StatisticsManager);
            EventBase::setStatisticsManager(m_StatisticsManager.get());

			m_SerializeManager->setStatisticsManager(m_StatisticsManager);
            m_SerializeManager->setIndexInterval(TimeDuration(m_Configuration.get<uint64_t>("timer.index-interval-ns")));

            m_EventManager->setReadMargin(TimeDuration(m_Configuration.get<uint64_t>("timer.read-margin-ns")));
            m_EventManager->setEvictMargin(TimeDuration(m_Configuration.get<uint64_t>("timer.evict-margin-ns")));

            setEventMaxTimeout(TimeDuration(m_Configuration.get<uint64_t>("timer.max-timeout-ns")));

            if(!serialized_streams_exists)
            {
                m_SerializeManager->setSerializerStreams(std::ofstream(m_Configuration.get<std::string>("serializer.event-data-file"), std::ios::binary), std::ofstream(m_Configuration.get<std::string>("serializer.event-index-file"), std::ios::binary), std::ofstream(m_Configuration.get<std::string>("serializer.stat-data-file"), std::ios::binary));
                m_SerializeManager->initializeProviderDriver(m_Configuration.get<std::string>("input.raw-file"));

                for(auto& provider : m_ReadProviders)
                {
                    provider->start();
                }

                std::clog << "Serializing data, please wait..." << std::endl;
                while(m_SerializeManager->serialize()) {}
                m_SerializeManager->flushAndClose();
                std::clog << "Serializing terminated. The number of serialized objects are: " <<  m_SerializeManager->getSerializedObjectCounter()  << ", Maximum duration: " << m_StatisticsManager->getMaximumDuration().count() << " ns" << std::endl;
                m_ViewManager.getControlPanel()->setSimulationTimeRange(m_StatisticsManager->getDataStartTime(), m_StatisticsManager->getDataEndTime());
                m_EventManager->initializeProviderDriver(m_Configuration.get<std::string>("serializer.event-data-file"));

                for(auto& provider : m_ReadProviders)
                {
                    provider->stop();
                }
            }
            else
            {
                std::clog << "Serialized files exists, loading..." << std::endl;
                m_EventManager->initializeProviderDriver(m_Configuration.get<std::string>("serializer.event-data-file"));

                std::ifstream event_index_file(m_Configuration.get<std::string>("serializer.event-index-file"), std::ios::binary);
                cereal::BinaryInputArchive event_index_deserializer(event_index_file);
                event_index_deserializer(m_SerializeManager->getEventIndexMap());

                std::ifstream stat_data_file(m_Configuration.get<std::string>("serializer.stat-data-file"), std::ios::binary);
                cereal::BinaryInputArchive stat_data_deserializer(stat_data_file);
                stat_data_deserializer(m_StatisticsManager->getGlobalStatisticsMap());
                stat_data_deserializer(m_StatisticsManager->getChartDataMap());

                Timestamp start_date;
                stat_data_deserializer(start_date);
                m_StatisticsManager->setDataStartTime(start_date);

                Timestamp end_date;
                stat_data_deserializer(end_date);
                m_StatisticsManager->setDataEndTime(end_date);

                stat_data_deserializer(m_StatisticsManager->getGlobalStatisticsOrderVector());
                m_ViewManager.getControlPanel()->setSimulationTimeRange(m_StatisticsManager->getDataStartTime(), m_StatisticsManager->getDataEndTime());
            }

			for(auto& provider : m_SerializeProviders)
			{
				provider->start();
			}
		}

        void Logoprism::setEventMaxTimeout(const TimeDuration& timeout)
        {
            m_EventMaxTimeout = timeout;
        }

		ViewManager& Logoprism::getViewManager()
		{
			return m_ViewManager;
		}

		void Logoprism::toggleStepMode()
		{
			if(m_StepMode == STEP_TIME) m_StepMode = STEP_EVENT;
			else m_StepMode = STEP_TIME;
		}

		void Logoprism::run()
		{
			m_Timer.start(10);
		    m_Application.exec();

			for(auto& provider : m_SerializeProviders)
			{
				provider->stop();
			}
		}

        std::string Logoprism::getRawFileName() const
		{
            return m_Configuration.get<std::string>("input.raw-file");
		}

        void Logoprism::onMouseEvent(ControlPanel::ControlEntries& entry)
        {
            if(entry.m_DataAvailable.load(std::memory_order_acquire))
            {
                if(entry.m_NextButtonPressed)
                {
                    if(m_StepMode == STEP_TIME)
                    {
                        m_TimeInfo.setSkipTimelapse(std::chrono::duration_cast<API::TimeDuration>(std::chrono::milliseconds(100)));
                    }
                    else
                    {
                        Timestamp simulation_time = m_EventManager->getNextEventInTime(m_TimeInfo.getSimulationTime(), false);
                        if(simulation_time != INVALID_TIMESTAMP)
                        {
                            m_TimeInfo.setSimulationTime(simulation_time);
                        }
                    }
                    m_EventManager->simulationTimeChanged();
                }
                else if(entry.m_PreviousButtonPressed)
                {
                    if(m_StepMode == STEP_TIME)
                    {
                        m_TimeInfo.setSkipTimelapse(std::chrono::duration_cast<API::TimeDuration>(std::chrono::milliseconds(-100)));
                    }
                    else
                    {
                        Timestamp simulation_time = m_EventManager->getNextEventInTime(m_TimeInfo.getSimulationTime(), true);
                        if(simulation_time != INVALID_TIMESTAMP)
                        {
                            m_TimeInfo.setSimulationTime(simulation_time);
                        }
                    }
                    m_EventManager->simulationTimeChanged();
                }
                else if(entry.m_GlobalStatisticsButtonPressed)
                {
                    m_ViewManager.generateGlobalStatistics();
                }
                else if(entry.m_SnapStatisticsButtonPressed)
                {
                    m_ViewManager.showSnapStatistics();
                }
                else if(entry.m_SliderChanged)
                {
                    uint64_t min_time_ns = m_StatisticsManager->getDataStartTime().time_since_epoch().count();
                    uint64_t diff_ns = m_StatisticsManager->getDataEndTime().time_since_epoch().count() - m_StatisticsManager->getDataStartTime().time_since_epoch().count();
                    double division_res = static_cast<double>(diff_ns) / std::numeric_limits<int>::max();
                    m_TimeInfo.setSimulationTime(Timestamp(API::TimeDuration(uint64_t(min_time_ns + division_res * entry.m_SliderValue) - m_EventMaxTimeout.count())));

                    auto it = m_SerializeManager->getEventIndexMap().lower_bound(m_TimeInfo.getSimulationTime());
                    if(it != m_SerializeManager->getEventIndexMap().begin()) it--;

                    m_EventManager->setProviderDriverOffset(it->second);
                    m_EventManager->simulationTimeChanged();
                }
                else if(entry.m_PlayButtonPressed)
                {
                    m_TimeInfo.togglePause();
                }
                else if(entry.m_DirectionChanged)
                {
                    m_DirectionReversed = !m_DirectionReversed;
                }
                else if(entry.m_KeyFrameDurationChanged)
                {
                    m_TimeInfo.setKeyFrameDuration(TimeDuration(entry.m_KeyFrameDuration));
                }
                else if(entry.m_StepModeChanged)
                {
                    toggleStepMode();
                }
                else if(entry.m_SpeedChanged)
                {
                    m_TimeInfo.setSimulationSpeed(entry.m_Speed);
                }
                else if(entry.m_StopButtonPressed)
                {
                    m_TimeInfo.setSimulationTime(m_StatisticsManager->getDataStartTime());
                    auto it = m_SerializeManager->getEventIndexMap().lower_bound(m_TimeInfo.getSimulationTime());
                    if(it != m_SerializeManager->getEventIndexMap().begin()) it--;

                    m_EventManager->setProviderDriverOffset(it->second);
                    m_EventManager->simulationTimeChanged();

                    if(!m_TimeInfo.isSimulationPaused())
                        m_TimeInfo.togglePause();
                }
                entry.clear();
            }
        }

		void Logoprism::tick()
		{
            m_TimeInfo.setSimulationReferenceTime(m_EventManager->getSimulationReferenceTime());
            m_TimeInfo.update(m_SimulationInProgress, m_DirectionReversed);
            onMouseEvent(m_ViewManager.getControlEntries());
            m_EventManager->superviseProviderDriver(m_TimeInfo.getSimulationTime());
            draw();
		}

		void Logoprism::draw()
		{
			std::vector<std::shared_ptr<API::EventBase>> out;
			if(m_TimeInfo.isKeyFrame())
			{
                m_SimulationInProgress = m_EventManager->getComputedEventsFor(m_TimeInfo.getSimulationTime(), m_TimeInfo.getKeyFrameDuration() * 2, m_DirectionReversed, out);
                if(m_TimeInfo.getSimulationReferenceTime() != INVALID_TIMESTAMP && m_TimeInfo.getSimulationTime() >= m_StatisticsManager->getDataEndTime())
                {
                    m_ViewManager.getControlPanel()->stop();
                }
			}

			m_ViewManager.drawViews(out);
		}

		void Logoprism::setEventManager(const std::shared_ptr<API::IEventManagerBase>& manager)
		{
			m_EventManager = manager;
		}

		void Logoprism::setSerializeManager(const std::shared_ptr<API::SerializeManagerBase>& manager)
		{
			m_SerializeManager = manager;
		}

		void Logoprism::setStatisticsManager(const std::shared_ptr<API::StatisticsManager>& manager)
		{
			m_StatisticsManager = manager;
		}

		void Logoprism::setReadProviders(std::vector<std::shared_ptr<API::IThreadManager>>& providers)
		{
			for(auto& provider : providers)
			{
				m_ReadProviders.push_back(provider);
			}
		}

		void Logoprism::setSerializeProviders(std::vector<std::shared_ptr<API::IThreadManager>>& providers)
		{
			for(auto& provider : providers)
			{
				m_SerializeProviders.push_back(provider);
			}
		}
	}
}
