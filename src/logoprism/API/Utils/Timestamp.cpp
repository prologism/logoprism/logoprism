#include "Timestamp.h"
#include <sstream>
#include <iomanip>

namespace LOGOPRISM
{
    namespace API
    {
        std::string getStringFromTimeStamp(const Timestamp& tp, const std::string& format_str)
        {
            std::stringstream str_date;
            std::time_t time = std::chrono::system_clock::to_time_t(tp);
            std::tm timetm = *std::localtime(&time);
            str_date << std::put_time(&timetm, format_str.c_str()) << "." << std::setfill ('0') << std::setw(6) << std::chrono::duration_cast<std::chrono::microseconds>(tp.time_since_epoch()).count() % 1000000;
            return str_date.str();
        }
    }
}
