#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <vector>
#include <iostream>

#include "Config.h"
#include "YamlParser.h"

namespace LOGOPRISM
{
	namespace API
	{

    static boost::filesystem::path expand_user(std::string path)
    {
    	if (path.empty())
    		return boost::filesystem::path();

    	if (path[0] != '~')
    		return boost::filesystem::path(path);

    	if ((path.size() > 1) && (path[1] != '/'))
    		path.insert(1, "/../");

    	char const* const home        = getenv("HOME");
    	char const* const userprofile = getenv("USERPROFILE");
      	char const* const homedrive   = getenv("HOMEDRIVE");
      	char const* const homepath    = getenv("HOMEPATH");

      	if(home)
      		path.replace(0, 1, std::string(home));

      	else if (userprofile)
      		path.replace(0, 1, std::string(userprofile));

      	else if (homedrive && homepath)
      		path.replace(0, 1, std::string(homedrive) + std::string(homepath));

      	return boost::filesystem::path(path);
    }

    Configuration::Configuration(std::string const& program, std::vector< std::string > const& arguments)
    	: program(program)
    	{
    		namespace bpo = boost::program_options;
    		namespace bfs = boost::filesystem;

    		bfs::path const   application_path = bfs::canonical(bfs::path(program).remove_filename());
    		std::string const application_name = bfs::path(program).stem().string();

    		std::string const config_name   = application_name + ".conf";
    		bfs::path const   system_config = bfs::path("/etc") / config_name;
    		bfs::path const   global_config = expand_user("~") / config_name;
    		bfs::path const   local_config  = application_path / config_name;

    		if (bfs::exists(system_config))
    			logoprism::config::yaml::read_yaml(system_config.string(), static_cast< boost::property_tree::ptree& >(*this));

    		if (bfs::exists(global_config))
    			logoprism::config::yaml::read_yaml(global_config.string(), static_cast< boost::property_tree::ptree& >(*this));

    		if (bfs::exists(local_config))
    			logoprism::config::yaml::read_yaml(local_config.string(), static_cast< boost::property_tree::ptree& >(*this));

    		std::clog << program << " ";
    		for (auto const& argument : arguments)
    		{
    			std::clog << argument << " ";
    		}
    		std::clog << std::endl;

    		if (arguments.empty())
    			return;

    		bpo::options_description generic_options("Generic options");
    		generic_options.add_options()
        		("version,v", "print version string")
				("help", "produce help message")
				("config,c", this->make_option< std::string >("config.file"), "configuration file");

    		bpo::options_description logoprism_options("Logoprism options");
    		logoprism_options.add_options()
        		("input-file,i", this->make_option< std::string >("input.file"), "input files")
				("input-format", this->make_option< std::string >("input.format"), "input format")
				("input-speed", this->make_option< double >("input.speed")->default_value(1.0), "input speed")
				("input-keepalive", this->make_option< double >("input.keepalive")->default_value(5.0), "input keep-alive time")
				("display-width,w", this->make_option< size_t >("display.width")->default_value(1024), "display width")
				("display-height,h", this->make_option< size_t >("display.height")->default_value(560), "display height")
        	("display-fullscreen,f", this->make_option< bool >("display.fullscreen")->default_value(false)->zero_tokens(), "run full screen")
			("display-multisampling", this->make_option< bool >("display.multisampling")->default_value(false)->zero_tokens(), "use multisampling")
			("output-video,o", this->make_option< bool >("output.video")->default_value(false)->zero_tokens(), "encode video")
			("output-framerate", this->make_option< size_t >("output.framerate"), "output frame rate (fps)")
			("output-pipeline", this->make_option< std::string >("output.pipeline"), "output gstreamer pipeline");

    		bpo::options_description command_line_options;
    		command_line_options.add(generic_options).add(logoprism_options);

    		bpo::variables_map  vm;
      	  bpo::parsed_options options = bpo::command_line_parser(arguments).options(command_line_options).allow_unregistered().run();
      	  bpo::store(options, vm);
      	  bpo::notify(vm);

      	  std::string const config_file = this->get("config.file", "");
      	  if (boost::filesystem::extension(config_file) == ".yaml")
      		  logoprism::config::yaml::read_yaml(config_file, static_cast< boost::property_tree::ptree& >(*this));

      	  if (vm.count("help"))
      		  std::cout << command_line_options << std::endl;
    	}

	}
}
