#ifndef SIGNALS_H
#define SIGNALS_H

namespace LOGOPRISM
{
	namespace API
	{
     	 void kill();
     	 bool isKilled();
     	 void install();
    }
}

#endif
