#ifndef STRINGTOINTEGERCONVERTER_H
#define STRINGTOINTEGERCONVERTER_H

namespace LOGOPRISM
{
    namespace API
    {
        template <typename T>
        T stringTo(const char* str)
        {
            T ret = 0;
            bool is_negative = false;
            if (*str == '-') 
            {
                is_negative = true;
                ++str;
            }

            while (*str >= '0' && *str <= '9') 
            {
                ret = (ret * 10) + (*str - '0');
                ++str;
            }

            if (is_negative) 
            {
                ret = -ret;
            }
            return ret;
        }
    }
}

#endif // STRINGTOINTEGERCONVERTER_H
