#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <chrono>
#include <ctime>
#include <string>

namespace LOGOPRISM
{
	namespace API
	{
		using TimeDuration = std::chrono::nanoseconds;
		using Timestamp = std::chrono::system_clock::time_point;
		using TimeClock = std::chrono::system_clock;
		constexpr static Timestamp INVALID_TIMESTAMP(TimeDuration(0));

		std::string getStringFromTimeStamp(const Timestamp& tp, const std::string& format_str = "%Y.%m.%d %H:%M:%S");
    }
}

#endif
