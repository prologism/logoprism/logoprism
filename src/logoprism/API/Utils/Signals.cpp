#include "Signals.h"

#ifndef _WIN32
	#include <unistd.h>
	#include <sys/wait.h>
#endif

#include <signal.h>

namespace LOGOPRISM
{
	namespace API
	{
		static sig_atomic_t volatile killReceived = false;
		static void terminateApplication(int)
		{
			killReceived = true;
		}

		static void terminateWithBacktrace(int signal_number)
		{

#ifndef _WIN32
			// Fork and exec gdb to generate nice backtraces. If gdb is not available, this will fail silently.
			// Fork, execve, waitpid, signal and raise are POSIX 'Asynchronous Signal Safe' functions which can safely be used in a signal handler.
			static char const* const gdb_backtrace_command[] = { "sh", "-c", "ps -p $$ -o ppid= | xargs gdb -batch -ex \"thread apply all bt\" -p 2>/dev/null", NULL };

			if (fork() == 0)
				execve("/bin/sh", (char**) gdb_backtrace_command, NULL);
			else
				waitpid(-1, NULL, 0);
#endif

			// Restore default signal handler and raise it again.
			signal(signal_number, SIG_DFL);
			raise(signal_number);
		}

		void kill()
		{
			raise(SIGINT);
		}

		bool isKilled()
		{
			return killReceived;
		}

		void install()
		{
			signal(SIGINT, &terminateApplication);
			signal(SIGTERM, &terminateApplication);

			signal(SIGSEGV, &terminateWithBacktrace);
			signal(SIGABRT, &terminateWithBacktrace);
			signal(SIGILL, &terminateWithBacktrace);
			signal(SIGFPE, &terminateWithBacktrace);
		}
	}
}
