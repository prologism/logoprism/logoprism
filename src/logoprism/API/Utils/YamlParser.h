#ifndef __LOGOPRISM_CONFIG_YAML_PARSER_HPP__
#define __LOGOPRISM_CONFIG_YAML_PARSER_HPP__

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/detail/file_parser_error.hpp>

#include <yaml-cpp/yaml.h>
#include <fstream>

namespace logoprism {
  namespace config {
    namespace yaml {

      struct yaml_parser_error : boost::property_tree::file_parser_error {
        yaml_parser_error(std::string const& message, std::string const& filename, size_t line) :
          file_parser_error(message, filename, line)
        {}
      };

      template< class Ptree >
      void read_yaml_helper(YAML::Node const& node, std::string const& key, Ptree& ptree, bool root=false) {
	std::cout << "1: " << node.Type() << " => " << key << std::endl;
        if (node.Type() == YAML::NodeType::Map) {
          Ptree* next;
          if (root)
            next = &ptree;
          else if (key != "")
            next = &ptree.put_child(key, ptree.get_child(key, Ptree()));
          else
            next = &ptree.push_back(std::make_pair(key, Ptree()))->second;

          for (YAML::const_iterator child = node.begin(), end = node.end(); child != end; ++child) {
	    std::cout << "2 (Map): " << child->first.as<std::string>()  << std::endl;
            read_yaml_helper(child->second, child->first.as<std::string>(), *next);
          }

        } else if (node.Type() == YAML::NodeType::Sequence) {
          Ptree* next;
	  std::cout << "3 (Sequence) " << std::endl;
          if (root)
            next = &ptree;
          else if (key != "")
            next = &ptree.put_child(key, ptree.get_child(key, Ptree()));
          else
            next = &ptree.push_back(std::make_pair(key, Ptree()))->second;

          for (auto const& child : node) {
            read_yaml_helper(child, "", *next);
          }

        } else {
	  std::cout << "4 (autre) " << std::endl;
          if (key != "") {
	    std::cout << "4 (autre non vide) " << key << " => " << node << std::endl;
            if (!node.IsNull())
            ptree.put(key, node.as< std::string >());
          } else {
	    std::cout << "4 (autre vide) " << node << std::endl;
            ptree.push_back(std::make_pair(key, Ptree(node.as< std::string >())));
          }
	  std::cout << "4 (autre fin) " << std::endl;
        }

      } // read_yaml_helper

      template< class Ptree >
      void read_yaml_internal(std::basic_istream< typename Ptree::key_type::value_type >& stream, Ptree& ptree, std::string const&) {
        // YAML::Parser parser;
        YAML::Node   document = YAML::Load(stream);

        // while (parser.GetNextDocument(document)) {
          read_yaml_helper(document, "", ptree, true);
          std::cout << "Fini ... " << std::endl;
        // }
      }

      template< class Ptree >
      void read_yaml(std::basic_istream< typename Ptree::key_type::value_type >& stream, Ptree& ptree) {
        read_yaml_internal(stream, ptree, std::string());
      }

      template< class Ptree >
      void read_yaml(std::string const& filename, Ptree& ptree, std::locale const& loc=std::locale()) {
        std::basic_ifstream< typename Ptree::key_type::value_type >
        stream(filename.c_str());
        if (!stream)
          BOOST_PROPERTY_TREE_THROW(yaml_parser_error("cannot open file", filename, 0));
        stream.imbue(loc);

        read_yaml_internal(stream, ptree, filename);
      }

      template< class Ptree >
      void write_yaml_helper(YAML::Emitter& stream, Ptree const& ptree) {
        typedef typename Ptree::key_type::value_type    char_type;
        typedef typename std::basic_string< char_type > string_type;

        if (ptree.empty()) {

#define YAML_TRY_GET(type_m)                          \
  do {                                                \
    try {                                             \
      stream << ptree.template get_value< type_m >(); \
      return;                                         \
    } catch (boost::exception const& e) {             \
    }                                                 \
  } while (0)

          // YAML_TRY_GET(int16_t);
          // YAML_TRY_GET(uint16_t);
          // YAML_TRY_GET(int32_t);
          // YAML_TRY_GET(uint32_t);
          // YAML_TRY_GET(int64_t);
          YAML_TRY_GET(uint64_t);

          // YAML_TRY_GET(float);
          YAML_TRY_GET(double);
          YAML_TRY_GET(bool);

#undef YAML_TRY_GET

          stream << YAML::SingleQuoted << ptree.template get_value< string_type >();

        } else if (ptree.count(string_type()) == ptree.size()) {
          if (std::count_if(ptree.begin(), ptree.end(), [] (typename Ptree::value_type const & node) { return node.second.empty(); }) == ptree.size())
            stream << YAML::Flow;

          stream << YAML::BeginSeq;
          for (auto const& node : ptree) {
            write_yaml_helper(stream, node.second);
          }
          stream << YAML::EndSeq;

        } else {
          // if (std::count_if(ptree.begin(), ptree.end(), [] (typename Ptree::value_type const & node) { return node.second.empty(); }) == ptree.size())
          // stream << YAML::Flow;

          stream << YAML::BeginMap;
          for (auto const& node : ptree) {
            stream << YAML::Key << node.first;
            stream << YAML::Value;
            write_yaml_helper(stream, node.second);
          }
          stream << YAML::EndMap;

        }
      } // write_yaml_helper

      template< class Ptree >
      void write_yaml_internal(std::basic_ostream< typename Ptree::key_type::value_type >& stream, Ptree const& ptree, std::string const& filename) {
        // if (!verify_yaml(pt, 0))
        // BOOST_PROPERTY_TREE_THROW(yaml_parser_error("ptree contains data that cannot be represented in JSON format", filename, 0));

        YAML::Emitter out;

        out << YAML::BeginDoc;
        write_yaml_helper(out, ptree);
        out << YAML::EndDoc;
        stream << out.c_str();

        if (!stream.good())
          BOOST_PROPERTY_TREE_THROW(yaml_parser_error("write error", filename, 0));
      }

      template< class Ptree >
      void write_yaml(std::basic_ostream< typename Ptree::key_type::value_type >& stream, Ptree const& ptree) {
        write_yaml_internal(stream, ptree, std::string());
      }

      template< class Ptree >
      void write_yaml(std::string const& filename, Ptree const& ptree, std::locale const& loc=std::locale()) {
        std::basic_ofstream< typename Ptree::key_type::value_type > stream(filename.c_str());
        if (!stream)
          BOOST_PROPERTY_TREE_THROW(yaml_parser_error("cannot open file", filename, 0));
        stream.imbue(loc);

        write_yaml_internal(stream, ptree, filename);
      }

    }
  }
}

#endif // ifndef __LOGOPRISM_CONFIG_YAML_PARSER_HPP__ const
