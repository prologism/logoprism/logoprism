#ifndef CONFIG_H
#define CONFIG_H

#include <cstdlib>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>

namespace LOGOPRISM
{
	namespace API
	{

    	struct Configuration : boost::property_tree::ptree
		{
    		Configuration(std::string const& program, std::vector< std::string > const& arguments);

    		std::string const program;

		private:
    		struct ptree_notifier
			{
    			ptree_notifier(boost::property_tree::ptree& ptree, std::string const& key) :
    				ptree(ptree),
					key(key)
    			{}

    			template< typename T >
    			void operator()(T const& value) { ptree.put(key, value); }

    			template< typename T >
    			void operator()(std::vector< T > const& values)
    			{
    				boost::property_tree::ptree& child = ptree.put_child(key, boost::property_tree::ptree());

    				for (auto const& v : values) {
    					child.push_back(std::make_pair("", boost::property_tree::ptree(v)));
    				}
    			}

    			boost::property_tree::ptree& ptree;
    			std::string const            key;
			};

    		template< typename T >
    		boost::program_options::typed_value< T >* make_option(std::string const& key)
			{
    			return boost::program_options::value< T >()->notifier(ptree_notifier(*this, key));
			}

		};

	}
}

#endif
