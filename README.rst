LogO'Prism - Animate your log stream!
====================================================================================================
.. image:: share/icons/hicolor/256x256/logoprism.png
    :alt: LogO'Prism Logo
    :target: https://gitlab.com/prologism/logoprism/logoprism

LogO'Prism
````````````````````````````````````````````````````````````````````````````````````````````````````
LogO'Prism is a visualisation library developed by `Prologism <https://www.prologism.fr>`_ which helps to visualise your log stream coming from different types of applications. 
It uses the Qt C++ library for displaying and it provides various types of objects to display your log events. 
It works as a framework which transforms your log file to Qt QGraphics objects and it displays them based on log timestamps.

LogO'Prism In Action
````````````````````````````````````````````````````````````````````````````````````````````````````
Some screenshots taken from the default plugin in operation. They are generated based on the data coming with the default plugin. 
Please check the section describing the default plugin for more details.

The picture below shows a screenshot of the main window of LogO'Prism animating the give log stream:

.. image:: share/screenshots/MainWindow.png
    :alt: LogO'Prism In Action
    :target: https://gitlab.com/prologism/logoprism/logoprism

By clicking on a disk, representing an event, one can get more information about the event. It is a customisable window to show 
more information if needed:

.. image:: share/screenshots/EventInfo.png
    :alt: LogO'Prism Global Statistics
    :target: https://gitlab.com/prologism/logoprism/logoprism

Several statistics calculated based on the read data, such as the number of events per milliseconds or the maximum latency in each
millisecond. The statistics module is customisable so that one can calculate the view one's own statistics:

.. image:: share/screenshots/GlobalStats.png
    :alt: LogO'Prism Snap Statistics
    :target: https://gitlab.com/prologism/logoprism/logoprism

Statistics are calculated on each snapshot of the animated stream in pause mode. The snap statistic is calculated based on the events
shown on the main window. This window is also customizable to personalised statistics:

.. image:: share/screenshots/SnapStats.png
    :alt: LogO'Prism Event Information
    :target: https://gitlab.com/prologism/logoprism/logoprism

DESCRIPTION
````````````````````````````````````````````````````````````````````````````````````````````````````
LogO'Prism is a highly extensible shared library which transforms your log stream into visualisable objects in order to help
the analysis of application events. The extension and the adaptation can be made by plugins, so LogO'Prism works as a core 
product which is used by an executable plugin.

LogO'Prism is mainly separated into two extensible subsystems and a mediator which orchestrates these two systems together.

The first subsystem, can be referred as the data processing subsystem, is responsible for the processing of a log file and
the transformation of file blocks into structures representing one or multiple events. This subsystem is also responsible for 
the calculation of different types of statistics based on the transformed data. Events are represented by a base class which 
can be extended by a plugin and it contains the following elements:

- Timestamp
- Duration
- Source
- Target
- Worker 
- Statistics

The second subsystem, can be referred as the visualisation subsystem, is responsible for the visualisation process. It takes 
the events selected by the first subsystem and it displays them.

The mediator provides the connection between these two subsystems and it also provides the time base for the real-time simulation
in order to display the real-time flow of the events of the application. The mediator defines a key frame time which can be 
interpreted as a resfreshment rate or a sample interval. At each sample interval, it asks the data processing system to select 
events which can be displayed based on their timestamps. Hence, it is important to provide all the events between the current
time and the end of the sampling period.

Data Processing System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This subsystem is there to process a log file and transform it into event structures. The heart of this subsystem is the 
templated producer/consumer pairs. A producer class is for producing any type of data. It uses the observer design pattern,
so it has a reference to its consumer to which it transfers its data. Then the data is consumed or transformed by the consumer
and possibly forwarded to other producers or consumers.

From these basic building blocks LogO'Prism builds a producer/consumer chain to parse and transform its data multiple times 
by separate threads as each producer is associated with a separate thread. 

Higher level classes are defined from these basic building blocks. One of such a class is a RawProvider class which has one 
template parameter in addition representing a source handler class. A source can be a file or a stream and a source handler
class must have the following functions:

- a function to retrieve data from its source
- a function to check whether data is available or the source is depleted

Another important class is an EventConverter which regroups the provider/consumer classes together and it mainly used for
transformation. For example, once a RawProvider read a line, it passes its line to an EventConverter which then transforms,
parses the line into an event structure. The EventConverter class does the transformation by the help of its template parameter 
which must have the following functions:

- a function to check whether the received data is valid
- a function for conversion

LogO'Prism uses two provider/consumer chain to achieve the transformation process:

- The first chain parses and transforms each line in a serialisable form and it calculates the global statistics. 
- The second chain just deserialises these objects and put them into an EventManager class.

The calculation of statistics belongs to this subsystem as well. It calculates several types of statistics based on the read files
and events. It calculates a global statistics based on all the events in the file. If the application contains several modules
it can calculate a global statistic for each of them. 

An event structure contains also a statistic element which mainly contains deviation values from a global statistic. A third type 
of statistic is calculated on an image, based on all the elements being executed at the selected time instance.

Visualisation System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The visualisation subsystem uses the Qt C++ library to visualise a real-time event flow parsed by the data processing subsystem.
LogO'Prism defines different types of objects for visualisation such as circles, rectangles, lines, points, etc.

The plugin can define its own layout using Qt and organise its objects in them. A layout is a sort of description of the screen
which helps to organise objects on the screen. For instance, a list is a layout which organises its element in a list on the screen. 

LogO'Prism supports a general layout plus a statistic window which can be reached by the visualisation control panel.

Once a layout is defined, the plugin must provide a generator function to generate objects representing log events. This
function is called by LogO'Prism core at each sample interval to generate the requested objects and put them into the layout.

Mediator System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The mediator system connects the two systems together. The mediator system provides a TimeInfo class which contains some information
about the time such as the current time, simulation time, timelapse. 

The mediator asks, at a defined sampling rate, the data processing system to provide events for displaying and passes them to the 
visualisation system. The visualisation system calls the generator function defined by the plugin.


PLUGINS
````````````````````````````````````````````````````````````````````````````````````````````````````

LogO'Prism uses a plugin system to extend its functionality and also to perform its tasks which consist of parsing and transforming
file block into events and displaying.

For the data processing system LogO'Prism needs two provider chains, for serialising and for deserialising. For the serialisation:

- RawProvider - EventConverter(parser) - EventConverter(worker simulator) - SerializeManager

RawProvider class is a provider having a source handler class as a template paramater. A typical example is when the source handler 
reads a file in which each line corresponds to an event. This is read by the RawProvider via its source handler and it is passed to
its consumer class which is the EventConverter. An EventConverter class is a producer/consumer pair which takes its data from the 
consumer and produces a new data by the help of its third template parameter which is a converter class. For example, it takes the lines
received from the RawProvider and it transforms it into an event structure and it passes these structures to its consumer. 

The next element in the producer/consumer chain can be a terminal state or more EventConverter classes. For instance, in the example
above the next step is still an EventConverter which takes an event structure and it assigns a worker thread to it which results in a 
modification of its timestamp and its potential worker thread. 

Finally, at a terminal state, the computed event structures are passed to the SerializeManager which is a consumer class and it serialises 
the received events into a file and calculates a global statistic.

After having read the whole file and serialized all of the event stuctures, a deserialisation process takes place by a second provider/consumer 
chain which typically:

- RawProvider - EventConverter(statistics calculator) - EventManager

The RawProvider has the same responsibility as before but instead of producing stream of lines from the file, it produces directy event structures
from the serialised data.

The serialised event structures then passed to an EventConverter for event statistic calculation based on the calculated global statistics.

After having calculated the statistics, the event stuctures are passed to the final stage, to the EventManager. The EventManager is a consumer
and it stocks the received event stuctures in a sorted deque from which the events are selected for displaying based on their timestamp.

These two provider chains must be passed to the LogO'Prism framework as two separate vectors of IThreadManager pointers.

Besides these two chains, a StatisticsManagar class must be instatiatied where the plugin developer can overwrite the default implementation of 
the statistic calculalator functions. Also, the developer can add new statistics if the system to be analysed has multiple modules to have a 
statistic per module. Then the manager class must be passed to the LogO'Prism framework.

Having defined all of the elements for the data processing subsystem, the plugin developer must define a layout and its view generator function. 
The view generator function gets a list of event structures to display and displays them as the developer wishes
by using the built-in view objects provided by LogO'Prism.

The final step is to call the initialise function of the LogO'Prism framework which verifies that all the necessary classes are passed correctly. 
Then the application can run which starts displaying the real-time view.

Please for further information refer to the Default plugin located in the repository.

Default Plugin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With LogO'Prism Core a default plugin is provided, which can be found in the repository.

Before Use
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before using the plugin, LogO'Prism Core and the plugin as well must be compiled as described in the compilation step.

Example Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To show an example the plugin comes with a configuration and also with some data. As mentioned, the configuration has a .yaml format 
where one can describe the input files and output files as well. The plugin comes with an example parser which reads lines where 
fields are separate with | character. Of course one can replace it with one’s own parser having knowledge of one’s data format.

Example Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The example data is provided to show an example. Each row is parsed one by one and the parser is executed for each to generate an event. 
There are the following fields in the data file:

- start time in microseconds since epoch
- execution time in microseconds
- the source
- the target

Event Type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As one can see from the data file to represent  an event, the EventBase class provided by LogO'Prism is sufficient. The parser only needs to assign:

- start time of the event (nanoseconds since epoch)
- the duration of the event in nanoseconds
- the source of the event
- the target of the event


As one can see there is no information about the worker thread in the data file. Worker threads can be simulated by LogO'Prism Core by adding
the WorkerSimulator class to the provider-consumer chain. This simulator takes each event and tries to find a worker for it. If there is a free
worker at the moment of the start time of the event, the event is assigned to the worker. If there is no free worker at all, to simulate thread
processing, the event start time must be delayed to the point where there is a free worker.

Provider Chain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As mentioned, there is a provider-consumer chain to process data. The default plugin has a chain of four elements. 

The RawProvider parses the raw data files containing text lines. The EventConverter, creates an event and fills its members with
the information retrieved from a line. As mentioned above, the worker thread information is not present in the data file so, 
one more converter needs to be added to the chain as a worker simulator, provided by  LogO'Prism Core, to assign worker threads. 
Finally the SerializeManager terminates the chain to save each event in a binary format so that next time one launches the 
plugin no more processing is needed.

COMPILING
````````````````````````````````````````````````````````````````````````````````````````````````````

LogO'Prism builds with CMake_ and the following platforms are supported, as long as the dependencies are available. 
As LogO'Prism is based on the Qt C++ library, it must be installed on the chosen platform. 

Besides of the dependencies listed in each section, LogO'Prism library is compiled in parallel with the following libraries:

- yaml-cpp: YAMLP parser library for parsing the LogO'Prism configuration file
- cereal: Serialisation library which is used during the serialisation and deserialisation process
- qtcharts: Qt library for graphs which is used to visualise the statistics

Please note that these libraries are submodules so they must be initialised when you do a checkout.

If you encounter any difficulties with the build process or with the list of packages please take a look at our .gitlab_ci.yml.

Linux - DEB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Linux, required tools and libraries can usually be found in your favorite distribution's package
manager, however the package names can differ from distribution to distribution.

- GCC_ >= 5.0, or Clang_ >= 3.2 are required for C++11 support and for std::put_time
- CMake_ >= 2.8.10
- pkg-config
- libboost-filesystem-dev 
- libboost-program-options-dev 
- libboost-regex-dev 
- qt5-default
- qtmultimedia5-dev

.. code:: bash

  mkdir build && cd build
  cmake ..
  make

Linux - RPM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Linux, required tools and libraries can usually be found in your favorite distribution's package
manager, however the package names can differ from distribution to distribution.

- GCC_ >= 5.0, or Clang_ >= 3.2 are required for C++11 support and for std::put_time
- CMake_ >= 2.8.10
- libXrandr-devel
- boost-filesystem
- boost-program-options
- boost-regex
- boost-devel
- qt5-qtbase
- qt5-qtbase-devel
- qt5-qtmultimedia
- qt5-qtmultimedia-devel

.. code:: bash

  mkdir build && cd build
  cmake ..
  make

Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For Windows, the compilation process is made by using the MinGW64 compiler. 
The easiest way to install MinGW is to install it together with the Qt library. Qt installer can be
downloaded from their official site. Upon installation, select to install MinGW besides the Qt components. 
This will also give a possiblity to open a command line with a preconfigured environment for Qt development.

Besides Qt, the following packages must be installed manually:

- CMake
- pkg-config lite available on: `pkg-config <https://sourceforge.net/projects/pkgconfiglite/files/>`_

When you run CMake precise that your compiler is a MinGW by choosing a generator using the -G option in CMake.
CMake will automatically download and install the following dependencies locally:

- 7zip
- ICU
- Boost

.. code:: bash

  mkdir build && cd build
  cmake -G Kate ..
  make

USAGE
````````````````````````````````````````````````````````````````````````````````````````````````````

Installation & Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you want to run your plugin or if you want to test the default plugin provided by LogO'Prism, you
must add the liblogoprism.so library file to your library path by setting the LD_LIBRARY_PATH variable.

If you want to develop your plugin you need to add the include path of the LogO'Prism headers.
Besides the headers you must also link your plugin with the logoprism.so file. 

When you develop your plugin you can choose between:
- retrieving LogO'Prism Core archive containing the pre-compiled shared library with headers from its repository
- or compiling LogO'Prism Core on your machine 

Retrieving LogO'Prism Core
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In order to develop your plugin you can retrieve the LogO'Prism Core archive from its repository.
This archive contains:

- the necessary header files for development
- the LogO'Prism Core shared library
- the example plugin binary file

If you choose this option pay attention to the ABI which must be the same for your plugin
in order to work well with the shared library. Try to compile with the same compiler as well. 

Compiling LogO'Prism Core
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you choose this option you can clone the repository and compile LogO'Prism Core
as detailed in the compilation section. Once the library is compiled you can start to develop your plugin.

Run-time utilisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use the ``etc/logoprism.yaml`` as a sample configuration file that has to be located in the same
folder as the ``logoprism`` binary.

At run-time LogO'Prism provides a control panel, which looks like a media player, to control
the flow of the generated film. This control panel contains the following user inputs:

- ``Play/Pause``: play/pause
- ``Stop``: stop the application
- ``Forward``: step forward by time or event according to the selected step mode
- ``Backward``: step backward by time or event according to the selected step mode
- ``Speed panel``: speed up/slow down the simulation
- ``Key frame panel``: decrease/increase sampling interval, ZOOM In/Out 
- ``Step selector``: toggle step mode between time/event
- ``Direction selector``: toggle the simulation direction
- ``Statistic button``: shows the statistics

CREDITS
````````````````````````````````````````````````````````````````````````````````````````````````````
This tool have been developed by Prologism <https://www.prologism.fr>. 
Please visit our site page at https://www.prologism.fr.
Don't hesitate to contact us `contact@prologism.fr <mailto:contact@prologism.fr>`.

COPYING INFORMATION
````````````````````````````````````````````````````````````````````````````````````````````````````
Distributed under the GPLv3 License

See accompanying LICENSE file or copy at `GPLv3 <https://www.gnu.org/licenses/gpl.txt>`_

.. _CMake: http://cmake.org
.. _GCC: http://gcc.gnu.org
.. _Clang: http://clang.llvm.org
